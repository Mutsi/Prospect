﻿using LiteNetLib;
using LiteNetLib.Utils;
using NetObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsGame1;
using WindowsGame1.NetPacks;
using WindowsGame1.Network;

namespace DedicatedServer
{

	class Program
	{
		internal class PeerShip : Ship
		{
			public int PeerID { get; set; }
		}
		private static NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();
		private static NetManager server;

		private static Dictionary<Guid, PeerShip> ships = new Dictionary<Guid, PeerShip>();

		static void Main(string[] args)
		{
			EventBasedNetListener listener = new EventBasedNetListener();
			server = new NetManager(listener);
			server.Start(Game1.UDPPort);

			listener.ConnectionRequestEvent += request =>
			{
				Console.WriteLine($"Client connecting {request.RemoteEndPoint}");
				if (server.GetPeersCount(ConnectionState.Connected) < 10 /* max connections */)
					request.AcceptIfKey("prospectclient");
				else
					request.Reject();
			};

			listener.PeerConnectedEvent += peer =>
			{
				foreach (Ship ship in ships.Values)
				{
					SendShip(ship, peer);
				}
				Console.WriteLine($"Client connected {peer.EndPoint}");
			};

			listener.PeerDisconnectedEvent += Listener_PeerDisconnectedEvent;

			//Subscribe to packet receiving
			_netPacketProcessor.SubscribeReusable<ShipData, NetPeer>(OnShipSyncReceived);
			_netPacketProcessor.SubscribeReusable<ShipUpdate, NetPeer>(OnShipUpdateReceived);
			_netPacketProcessor.SubscribeReusable<ShipPartData, NetPeer>(OnShipPartReceived);

			listener.NetworkReceiveEvent += Listener_NetworkReceiveEvent;

			while (!Console.KeyAvailable)
			{
				server.PollEvents();
				Thread.Sleep(15);
			}
			server.Stop();
		}

		private static void Listener_PeerDisconnectedEvent(NetPeer peer, DisconnectInfo disconnectInfo)
		{
			var deleteShips = ships.Where(o => o.Value.PeerID == peer.Id).ToList();
			foreach (var delete in deleteShips)
			{
				ships.Remove(delete.Key);
			}

			Console.WriteLine("Disconnected client");
			Console.WriteLine($"Clients online {server.ConnectedPeersCount} ships synced {ships.Count}");
		}

		private static void Listener_NetworkReceiveEvent(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
		{
			try
			{
				_netPacketProcessor.ReadAllPackets(reader, peer);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}

		private static void OnShipSyncReceived(ShipData arg1, NetPeer arg2)
		{
			ShipSyncPack pack = ShipSyncPack.Unpack(arg1.Data);

			//Upsert
			if (!ships.ContainsKey(pack.ShipGuid))
			{
				PeerShip ship = new PeerShip();
				ship.PeerID = arg2.Id;
				ship.parts = pack.Parts;
				ship.guid = pack.ShipGuid;
				ships.Add(pack.ShipGuid, ship);
			}
			else
			{
				ships[pack.ShipGuid].parts = pack.Parts;
			}

			NetDataWriter writer = new NetDataWriter();
			server.SendToAll(_netPacketProcessor.Write(arg1), DeliveryMethod.ReliableOrdered, arg2);
		}

		private static void OnShipUpdateReceived(ShipUpdate arg1, NetPeer arg2)
		{
			Ship shipUpdate = Ship.Unpack(arg1.Data);

			//Upsert
			if (ships.ContainsKey(shipUpdate.guid))
			{
				//Console.WriteLine($"Ship: {shipUpdate.ToString()}");
			}

			NetDataWriter writer = new NetDataWriter();
			server.SendToAll(_netPacketProcessor.Write(arg1), DeliveryMethod.Unreliable, arg2);
		}

		private static void OnShipPartReceived(ShipPartData arg1, NetPeer arg2)
		{
			ShipPartPack pack = ShipPartPack.Unpack(arg1.Data);

			//Upsert
			if (ships.ContainsKey(pack.ShipGuid))
			{
				if (!arg1.IsRemove)
				{
					if (ships[pack.ShipGuid].parts == null)
					{
						ships[pack.ShipGuid].parts = new List<ShipPart>();
					}
					ships[pack.ShipGuid].parts.Add(pack.ShipPart);
				}
				else
				{
					ships[pack.ShipGuid].parts.RemoveAll(o => o.guid == pack.ShipPart.guid);
				}
			}

			NetDataWriter writer = new NetDataWriter();
			server.SendToAll(_netPacketProcessor.Write(arg1), DeliveryMethod.ReliableOrdered, arg2);
		}

		private static void SendShip(Ship ship, NetPeer peer)
		{
			ShipSyncPack package = new ShipSyncPack();
			package.PartCount = (short)ship.parts.Count();
			package.Parts = ship.parts;
			package.ShipGuid = ship.guid;
			byte[] data = package.Package();

			ShipData sd = new ShipData
			{
				Data = data
			};
			try
			{
				peer.Send(_netPacketProcessor.Write(sd), DeliveryMethod.ReliableOrdered);
			}
			catch (Exception e)
			{

				throw e;
			}
		}
	}
}
