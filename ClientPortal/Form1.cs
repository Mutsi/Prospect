﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientPortal
{
	public partial class MainScreen : Form
	{
		public MainScreen()
		{
			InitializeComponent();
			IsUpdateAvailable();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		public static bool IsUpdateAvailable()
		{
			FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://127.0.0.1:21/");
			request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;

			// This example assumes the FTP site uses anonymous logon.  
			request.Credentials = new NetworkCredential();
			request.KeepAlive = false;
			request.UseBinary = true;
			request.UsePassive = true;

			return true;
		}
	}
}
