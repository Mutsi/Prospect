﻿using NetObj;
using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetObj
{
	public class UDPSender : NetSocket
	{
		private Socket socket;
		private IPAddress send_to_address;
		private IPEndPoint sending_end_point;

		private UDPConnection serverConnection;

		public UDPSender(string serveraddr)
		{
			socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			send_to_address = IPAddress.Parse(serveraddr);
			sending_end_point = new IPEndPoint(send_to_address, 16785);

			serverConnection = new UDPConnection(sending_end_point);
			UDPConnections.Add(sending_end_point.ToString(), serverConnection);
		}

		~UDPSender()
		{
			isActive = false;
		}

		public static bool ConnectedToServer = false;

		public void ListenToServer()
		{
			while (isActive)
			{
				if (socket.Available > 1)
				{
					byte[] data = new byte[1400];
					socket.Receive(data);
					EndPoint endPoint = sending_end_point;
					//socket.BeginReceive(data, 0, 500, SocketFlags.None, ReceiveCallback, data);
					//socket.BeginReceiveFrom(data, 0, 1400, SocketFlags.None, ref endPoint, ReceiveCallback, data);
					if (ConnectedToServer == false)
					{
						lock (serverConnection)
						{
							try
							{
								ClientConnectedArgs arg = new ClientConnectedArgs { connection = null };
								NewConnectionEvent(null, arg);

							}
							catch (Exception e)
							{

								//throw e;
							}
						}
					}
					ConnectedToServer = true;


					ReadData(data, UDPConnections.ElementAt(0).Value);

					ResendReliable();
				}
				else
				{
					if ((DateTime.Now - UDPConnections.First().Value.lastUpdate).TotalSeconds > 10)
					{
						ConnectedToServer = false;
					}
					Thread.Sleep(10);
				}
			}
		}

		private void ReceiveCallback(IAsyncResult ar)
		{
			ReadData((byte[])ar.AsyncState, UDPConnections.First().Value);

		}

		public override void SendPack(NetPack pack)
		{
			pack.connection = UDPConnections.Values.First();
			base.SendPack(pack);

			byte[] send_buffer = pack.SerializePackage();

			//Console.WriteLine("Sending Package of Size : {0}", send_buffer.Length);

			socket.SendTo(send_buffer, sending_end_point);

			if (thread == null)
			{
				thread = new Thread(new ThreadStart(ListenToServer));
				thread.Start();
			}
		}
	}
}
