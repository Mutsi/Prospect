﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetObj
{
	public class ExecutionMap
	{
		public static void Initialize()
		{
			Map.Add(0xff, readAck);
		}

		private static void readAck(NetPack pack)
		{
			Console.WriteLine("Ack Read");
		}

		public static Dictionary<byte, NetPack.Execute> Map = new Dictionary<byte, NetPack.Execute>();
	}
}
