﻿using NetObj;
using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace NetObj
{
	public class UDPListener : NetSocket
	{
		private const int listenPort = 16785;
		public UdpClient listener;
		private static UdpClient getClient;
		private IPEndPoint groupEP;

		private static UDPListener getInstance;


		public UDPListener()
		{
			listener = new UdpClient(listenPort);
			getClient = listener;
			groupEP = new IPEndPoint(IPAddress.Any, listenPort);
			getInstance = this;
		}

		~UDPListener()
		{
			listener.Close();
		}

		// get the ip and port number where the client will be listening on
		public IPEndPoint GetClientInfo()
		{
			return groupEP;
		}


		public void Listen()
		{
			thread = new Thread(new ThreadStart(listen));
			thread.Start();
		}

		public override void Shutdown()
		{
			listener.Close();
			base.Shutdown();
		}

		private void listen()
		{
			while (isActive)
			{
				byte[] receive_byte_array;
				try
				{
					receive_byte_array = listener.Receive(ref groupEP);
					//Check whois client
					IPEndPoint connection = GetClientInfo();
					string addrStr = connection.ToString();

					UDPConnection current = null;
					if (!UDPConnections.ContainsKey(addrStr))
					{
						current = new UDPConnection(connection);
						lock (UDPConnections)
						{
							UDPConnections.Add(addrStr, current);
						}
						NewConnectionEvent(this, new ClientConnectedArgs { connection = current });
					}
					else
					{
						current = UDPConnections[addrStr];
					}
					ReadData(receive_byte_array, current);
				}
				catch (Exception e)
				{
					Console.WriteLine(e.ToString());
				}

				ResendReliable();
				handleTimeouts();
				//Thread.Sleep(50);
			}
			listener.Close();
		}

		public void Broadcast(NetPack pack)
		{
			NetPack repeat = new NetPack();
			repeat.connection = pack.connection;
			repeat.content = pack.content;
			repeat.ExcecuteFunc = pack.ExcecuteFunc;
			repeat.Guid = pack.Guid;
			repeat.isReliable = pack.isReliable;
			repeat.PackID = pack.PackID;
			repeat.SequenceNr = pack.SequenceNr;

			lock (UDPConnections)
			{
				foreach (var item in UDPConnections)
				{
					if (pack.connection != item.Value)
					{
						repeat.connection = item.Value;
						SendPack(repeat);
					}
				}
			}
		}

		public override void SendPack(NetPack pack)
		{
			base.SendPack(pack);

			//Send data back
			byte[] send_buffer = pack.SerializePackage();
			//Console.WriteLine("Sending Package of Size : {0}", send_buffer.Length);
			//IPEndPoint clientInfo = GetClientInfo();
			//listener.Send(send_buffer, send_buffer.Length, pack.connection.endpoint);

			// Begin sending the data to the remote device.
			listener.BeginSend(send_buffer, send_buffer.Length, pack.connection.endpoint, new AsyncCallback(SendCallback), listener);
		}

		private static void SendCallback(IAsyncResult ar)
		{
			try
			{
				// Retrieve the socket from the state object.
				UdpClient handler = (UdpClient)ar.AsyncState;
				// Complete sending the data to the remote device.
				int bytesSent = handler.EndSend(ar);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		}

		public static void ReadCallback(IAsyncResult ar)
		{
			// Retrieve the socket from the state object.
			UDPConnection connection = (UDPConnection)ar.AsyncState;

			// Read data from the client socket. 
			byte[] bytesRead = getClient.EndReceive(ar, ref connection.endpoint);

			if (bytesRead.Length > 0)
			{
				UDPListener.getInstance.ReadData(bytesRead, connection);
			}
		}

		private void handleTimeouts()
		{
			return;
			lock (UDPConnections)
			{
				string dropConnection = string.Empty;
				foreach (var connection in UDPConnections)
				{
					if (DateTime.Now - connection.Value.lastUpdate > new TimeSpan(0, 0, 10))
					{
						dropConnection = connection.Key;
					}
				}

				if (dropConnection != string.Empty)
				{
					UDPConnections.Remove(dropConnection);
				}
			}
		}

	}
}