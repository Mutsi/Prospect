﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace UpdateClient
{
	class Program
	{
		static string address;

		public static object MessageBox { get; private set; }

		public static bool IsFileReady(String sFilename)
		{
			// If the file can be opened for exclusive access it means that the file
			// is no longer locked by another process.
			try
			{
				if (!File.Exists(sFilename))
					return true;

				using (FileStream inputStream = File.Open(sFilename, FileMode.Open, FileAccess.Read, FileShare.None))
				{
					if (inputStream.Length > 0)
					{
						return true;
					}
					else
					{
						return false;
					}

				}
			}
			catch (Exception)
			{
				return false;
			}
		}

		static void Main(string[] args)
		{
			Console.WriteLine("Waiting for game to close");
			string curDir = Environment.CurrentDirectory;

			while (!IsFileReady(curDir + "/WindowsGame1.exe"))
			{
			}

			Console.WriteLine("Checking Internet Connectivity");
			//bool isMutsi = false;
			//string mutsiWeb = "84.104.184.25";
			/*IPAddress[] addresslist = Dns.GetHostAddresses(mutsiWeb);
			string externalip = new WebClient().DownloadString("http://icanhazip.com").Trim();

			foreach (IPAddress theaddress in addresslist)
			{
				if (theaddress.ToString() == externalip)
					isMutsi = true;
			}*/

			//Program.address = isMutsi ? "127.0.0.1" : "52.232.46.126";
			Program.address = "52.232.46.126";

			Console.WriteLine("Connecting to File Server");

			DownloadFtpDirectory("ftp://" + Program.address + ":21/current/", new NetworkCredential("prospectclient", ""), curDir);
			Console.WriteLine("Downloading Update");

			System.Diagnostics.Process.Start(@"WindowsGame1.exe");
		}

		static void DownloadFtpDirectory(string url, NetworkCredential credentials, string localPath)
		{
			FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(url);
			listRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
			listRequest.Credentials = credentials;

			List<string> lines = new List<string>();

			using (FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse())
			using (Stream listStream = listResponse.GetResponseStream())
			using (StreamReader listReader = new StreamReader(listStream))
			{
				while (!listReader.EndOfStream)
				{
					lines.Add(listReader.ReadLine());
				}
			}


			foreach (string line in lines)
			{
				string[] tokens =
					line.Split(new[] { ' ' }, 9, StringSplitOptions.RemoveEmptyEntries);
				string name = tokens[8];
				string permissions = tokens[0];

				string localFilePath = Path.Combine(localPath, name);
				string fileUrl = url + name;

				if (permissions[0] == 'd')
				{
					if (!Directory.Exists(localFilePath))
					{
						Directory.CreateDirectory(localFilePath);
					}

					DownloadFtpDirectory(fileUrl + "/", credentials, localFilePath);
				}
				else
				{
					FtpWebRequest downloadRequest = (FtpWebRequest)WebRequest.Create(fileUrl);
					downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
					downloadRequest.Credentials = credentials;

					DateTime modified = new DateTime();

					//Check versions
					if (File.Exists(localFilePath))
					{
						FileInfo info = new FileInfo(localFilePath);
						modified = info.LastWriteTime;

						DateTime serverModified = getFileCreatedDateTime(fileUrl);
						if (serverModified <= modified)
							continue;
					}

					using (FtpWebResponse downloadResponse =
							  (FtpWebResponse)downloadRequest.GetResponse())
					using (Stream sourceStream = downloadResponse.GetResponseStream())
					using (Stream targetStream = File.Create(localFilePath))
					{
						byte[] buffer = new byte[10240];
						int read;
						while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
						{
							targetStream.Write(buffer, 0, read);
						}
					}
				}
			}
		}

		private static DateTime getFileCreatedDateTime(string address)
		{
			try
			{
				FtpWebRequest ftpRequest = (FtpWebRequest)FtpWebRequest.Create(address);
				ftpRequest.Credentials = new NetworkCredential("prospectclient", "");

				ftpRequest.UseBinary = true;
				ftpRequest.UsePassive = true;
				ftpRequest.KeepAlive = true;

				ftpRequest.Method = WebRequestMethods.Ftp.GetDateTimestamp;
				FtpWebResponse ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();

				return ftpResponse.LastModified;
			}
			catch (Exception ex)
			{
				// Don't like doing this, but I'll leave it here
				// to maintain compatability with the existing code:
				Console.WriteLine(ex.ToString());
			}

			return DateTime.MinValue;
		}
	}
}
