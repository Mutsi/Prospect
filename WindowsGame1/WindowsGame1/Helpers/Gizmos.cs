﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1.Helpers
{
	public class GizmoRay
	{
		public Vector2 A, B;
	}

	public class Gizmos
	{
		public static List<GizmoRay> DebugRays = new List<GizmoRay>();

		public static void DrawRays(SpriteBatch batch, GraphicsDevice device, BasicEffect effect)
		{
			batch.Begin();

			foreach (GizmoRay ray in DebugRays)
			{
				//An array of 2 vertices - a start and end position
				VertexPositionColor[] Vertices = new VertexPositionColor[2];
				int[] Indices = new int[2];

				//Starting position of the ray
				Vertices[0] = new VertexPositionColor()
				{
					Color = Color.Orange,
					Position = new Vector3(ray.A.X, ray.A.Y, 0)
				};


				//End point of the ray
				Vertices[1] = new VertexPositionColor()
				{
					Color = Color.Orange,
					Position = new Vector3(ray.B.X, ray.B.Y, 0)
				};

				Indices[0] = 0;
				Indices[1] = 1;

				foreach (EffectPass pass in effect.CurrentTechnique.Passes)
				{
					pass.Apply();
					device.DrawUserIndexedPrimitives(PrimitiveType.LineStrip, Vertices, 0, 2, Indices, 0, 1, VertexPositionColorTexture.VertexDeclaration);
				}
			}

			batch.End();
		}
	}
}
