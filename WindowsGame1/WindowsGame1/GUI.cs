﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    public class GUIItem
    {
        public Rectangle rec;
        public virtual void Draw(SpriteBatch batch) { }
        public bool IsActive = true;
    }
    public class GUI
    {
        public List<GUIItem> items = new List<GUIItem>();
        private KeyboardState prevks;

        SpriteFont font;
        Texture2D nixel;

        public OldTextBox focus = null;

        public GUI()
        {
            font = Game1.ContentRef.Load<SpriteFont>("PlainText");
            nixel = Game1.ContentRef.Load<Texture2D>("nixel");
        }

        public void Update(KeyboardState ks)
        {
            if (focus != null && focus.IsActive)
            {
                foreach (Keys key in ks.GetPressedKeys())
                {
                    if (!prevks.GetPressedKeys().Contains<Keys>(key))
                        focus.CharEntered(Physics.ConvertKeyToChar(key, ks.IsKeyDown(Keys.LeftShift)));
                }

                if (ks.IsKeyDown(Keys.Back) && !prevks.IsKeyDown(Keys.Back))
                {
                    focus.Backspace();
                }
            }

            prevks = ks;
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (GUIItem item in items)
            {
                if (item.IsActive)
                    item.Draw(batch);
            }
        }
    }

    public class Shapes
    {
        public static void DrawBox(SpriteBatch batch, Texture2D nixel, Rectangle rect, Color color)
        {
            batch.Draw(nixel, rect, color);
        }
    }

    public class OldButton : GUIItem
    {
        //These are set in the constructor:
        SpriteFont font;
        Texture2D nixel;
        public string text;
        bool hover = false;

        MouseState prevMS;

        public OldButton(Rectangle rec, string text)
        {
            font = Game1.ContentRef.Load<SpriteFont>("PlainText");
            nixel = Game1.ContentRef.Load<Texture2D>("nixel");
            this.rec = rec;
            this.text = text;

            this.rec.Width = (int)font.MeasureString(text).X;
            this.rec.Height = (int)font.MeasureString(text).Y;
        }

        public void SetText(string text)
        {
            this.text = text;

            this.rec.Width = (int)font.MeasureString(text).X;
            this.rec.Height = (int)font.MeasureString(text).Y;
        }

        public bool Update(MouseState ms)
        {
            hover = false;

            bool clicked = false;
            if (rec.Contains(ms.X, ms.Y))
            {
                hover = true;
                clicked = (ms.LeftButton == ButtonState.Pressed && prevMS.LeftButton == ButtonState.Released);
            }
            prevMS = ms;
            return clicked;
        }

        public override void Draw(SpriteBatch batch)
        {
            Color col = Color.DarkGray;
            if (hover)
                col = Color.LightGray;

            Shapes.DrawBox(batch, nixel, rec, col);
            batch.DrawString(font, text, new Vector2(rec.X, rec.Y), Color.Black);
        }
    }

    public class OldTextBox : GUIItem
    {
        public string text = ""; //Start with no text
        int cursorPos = 0;

        //These are set in the constructor:
        SpriteFont font;
        Texture2D nixel;

        public OldTextBox(Rectangle rec)
        {
            font = Game1.ContentRef.Load<SpriteFont>("PlainText");
            nixel = Game1.ContentRef.Load<Texture2D>("nixel");
            this.rec = rec;

            int height = (int)font.MeasureString("X").Y;
            this.rec.Height = height;
        }

        public void CharEntered(string c)
        {
            if (c.Length == 0)
                return;
            string newText = text.Insert(cursorPos, c); //Insert the char

            //Check if the text width is shorter than the back rectangle
            if (font.MeasureString(newText).X < rec.Width)
            {
                text = newText; //Set the text
                cursorPos++; //Move the text cursor
            }
        }

        public void Backspace()
        {
            if (cursorPos > 0 && text.Length > 0)
            {
                text = text.Remove(cursorPos - 1, 1);
                cursorPos--; //Move the text cursor
            }
        }

        public override void Draw(SpriteBatch batch)
        {
            Shapes.DrawBox(batch, nixel, rec, Color.DarkGray);
            batch.DrawString(font, text, new Vector2(rec.X, rec.Y), Color.Black);
        }
    }

    public class TextList : GUIItem
    {
        //These are set in the constructor:
        SpriteFont font;
        Texture2D nixel;

        public string[] files = { "" };

        public int selected = 0;

        public TextList(Rectangle rec)
        {
            font = Game1.ContentRef.Load<SpriteFont>("PlainText");
            nixel = Game1.ContentRef.Load<Texture2D>("nixel");
            this.rec = rec;

            int height = (int)font.MeasureString("Y").Y;
            this.rec.Height = height;
        }

        public void Update(MouseState ms)
        {
            int i = 0;
            if (ms.LeftButton == ButtonState.Pressed)
                foreach (string file in files)
                {
                    Rectangle fileRec = new Rectangle(rec.X, rec.Y + i * rec.Height, rec.Width, rec.Height);
                    if (fileRec.Contains(ms.X, ms.Y))
                    {
                        selected = i;
                    }
                    i++;
                }
        }

        public override void Draw(SpriteBatch batch)
        {
            if (files.Length == 0)
                return;
            int i = 0;
            foreach (string file in files)
            {
                Color col = Color.DarkGray;
                if (i == selected)
                    col = Color.LightGray;

                Shapes.DrawBox(batch, nixel, new Rectangle(rec.X, rec.Y + i * rec.Height, rec.Width, rec.Height), col);
                batch.DrawString(font, file, new Vector2(rec.X, rec.Y + i * rec.Height), Color.Black);
                i++;
            }
        }
    }

}
