﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	class TractorBeam : ShipPart
	{
		private const float power = 2.5f;
		[NonSerialized()]
		private Texture2D tractor, tractorGlow, beam;

		protected float laserRot = 0.0f;

		protected Vector2 dir = Vector2.Zero;

		protected float laserAnim = 0.0f;
		protected bool isOn = false;
		protected float laserDist = 0.0f;

		protected float mineSpeed = .5f;
		protected float mineRange = 15.0f * 16.0f;

		public TractorBeam()
		{
			TypeID = 8;

		}
		public TractorBeam(float x, float y)
		{
			TypeID = 8;
			Cost = 100;
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("GunMount");
			tractor = ResourceManager.Load<Texture2D>("TractorBeam");
			tractorGlow = ResourceManager.Load<Texture2D>("TractorGlow");
			beam = ResourceManager.Load<Texture2D>("TraktorBeam");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}


		protected Cargobay cargoBay = null;
		public override void Action(Ship ship, bool isNet = false)
		{
			isOn = ship.fireSystem && (ship.eCharge > 0.5f);
			cargoBay = null;

			if (isOn)
			{
				ship.eCharge -= power;
				laserDist = mineRange;
				Cargobay nearest = null;
				float min = float.MaxValue;

				foreach (Ship otherShip in Game1.GetShips)
				{
					if (otherShip == ship)
						continue;

					for (int i = 0; i < otherShip.Parts.Count; i++)
					{
						if (otherShip.Parts[i] as Cargobay == null)
							continue;
						Cargobay m = (Cargobay)otherShip.Parts[i];
						if (!m.HasCargo || !ship.HasCargoRoom(m.CargoType))
							continue;

						Rectangle partRect = new Rectangle((int)m.WorldPosition().X - 16, (int)m.WorldPosition().Y - 16, 32, 32);

						float dist;
						if (Physics.CollideRayRectangle(partRect, worldPosition, worldPosition + (dir * mineRange), out dist))
						{
							if (dist < min)
							{
								min = dist;
								nearest = m;
								laserDist = dist;
								cargoBay = nearest;
							}
						}
					}

					if (nearest != null)
					{
						Game1.particleMaster.SpawnTractor(1, nearest.WorldPosition(), Vector2.Normalize(worldPosition - nearest.WorldPosition()) * laserDist / 40.0f);
						float taken = cargoBay.TakeCargo(mineSpeed);
						ship.VoidCargo.AddResource(cargoBay.CargoType, taken);
					}
				}


				Vector2 aimAt = ship.MSWorld;
				if (cargoBay != null)
				{
					aimAt = cargoBay.WorldPosition();
				}
				dir = Vector2.Normalize(aimAt - worldPosition);
				laserRot = (float)Math.Atan2(dir.Y, dir.X);

				laserAnim += 1.0f;
			}
		}

		public override void Draw(SpriteBatch batch)
		{
			base.Draw(batch);

			batch.Draw(texture, worldPosition, null, Color.Purple, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);


			int segments = (int)(laserDist / 32.0f) + 1;
			if (isOn)
			{

				batch.Draw(tractorGlow, worldPosition, null, Color.Purple, laserAnim, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
				for (int i = 1; i < segments; i++)
				{
					batch.Draw(beam, worldPosition + (dir * 32.0f * i), new Rectangle((int)+laserAnim, 0, 32, 32), Color.Purple, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
				}
			}
			batch.Draw(tractor, worldPosition, null, Color.Purple, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}
	}
}
