using NetObj;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace WindowsGame1
{
#if WINDOWS || XBOX
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main(string[] args)
		{
#if !DEBUG
			try
			{
#endif
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);

				ClientPortal portal = new ClientPortal();

				if (ClientPortal.RestartRequired)
				{
					Process currentProcess = Process.GetCurrentProcess();
					System.Diagnostics.Process.Start(@"UpdateClient.exe", currentProcess.Id.ToString());
					return;
				}
				else
					Application.Run(portal);


				using (Game1 game = new Game1())
				{
					bool isServer = ClientPortal.isServer;
					foreach (string arg in args)
					{
						if (arg.ToLower() == "server")
						{
							isServer = true;
						}
					}
					game.IsServer = isServer;
					game.Run();
				}
			}
#if !DEBUG

			catch (System.Exception e)
			{
				MessageBox.Show(e.ToString(), "Oops", MessageBoxButtons.OK, MessageBoxIcon.Error);
				throw e;
			}
		}
#endif
	}
#endif
}
