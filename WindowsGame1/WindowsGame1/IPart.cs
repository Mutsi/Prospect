﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
	public interface IPart
	{
		float Mass();
		void SetLocal(int x, int y);
		Vector2 WorldPosition();
		void Update(Ship ship);
		void Action(Ship ship, bool isNet = false);
		void LoadContent();
		void Draw(SpriteBatch batch);
		Vector2 LocalCOMPosition();
		bool IsValid(int x, int y, Ship ship);
	}
}
