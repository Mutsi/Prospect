﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1.Network
{
	public class ShipPartData
	{
		public bool IsRemove { get; set; }
		public byte[] Data { get; set; }
	}
}
