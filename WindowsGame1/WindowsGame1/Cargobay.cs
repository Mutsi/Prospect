﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProtoBuf;
using WindowsGame1.ShipParts;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using WindowsGame1.Models;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	class Cargobay : ShipPart, Activatable
	{

		protected float capactiy = 50.0f;
		protected float cargoDensity = 3.0f;
		[ProtoMember(1)]
		protected float cargo = 0.0f;
		[ProtoMember(2)]
		protected Resource resource = Resource.Void;

		public bool HasRoom { get { return cargo < capactiy; } }
		public bool HasCargo { get { return cargo > 0.0f; } }
		public Resource CargoType { get { return cargo == 0.0f ? Resource.Void : resource; } }
		public float CargoAmount { get { return cargo; } }
		public float SpaceLeft { get { return capactiy - cargo; } }

		[NonSerialized()]
		private Texture2D mineral;
		[NonSerialized()]
		private Texture2D container;

		public Cargobay()
		{
			TypeID = 10;
		}
		public Cargobay(float x, float y)
		{
			TypeID = 10;
			mass = 10.0f;
			Cost = 50;
		}

		public override float Mass()
		{
			return mass + cargo * cargoDensity;
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("Cargobay");
			mineral = ResourceManager.Load<Texture2D>("CargoMineral");
			container = ResourceManager.Load<Texture2D>("CargoContainer");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}

		Ship refShip;
		public override void Action(Ship ship, bool isNet = false)
		{
			if (isNet == true)
				return;
			this.refShip = ship;
			if (CargoType == Resource.Void)
			{
				resource = ship.VoidCargo.resourceDictionary.FirstOrDefault(o => o.Value > 0).Key;
			}

			if (resource != Resource.Void && ship.VoidCargo.HasAny(resource))
			{
				if (cargo < capactiy)
				{
					isDirty = true;

					float dif = capactiy - cargo;

					float taken = ship.VoidCargo.TakeMax(resource, dif);
					cargo += taken;
				}
			}
		}

		public float PutCargo(Resource res, float amount)
		{
			resource = res;
			float canPush = Math.Min(amount, capactiy - cargo);
			if (canPush > 0.0f)
			{
				cargo += canPush;
				return canPush;
			}
			return 0.0f;
		}

		public float TakeCargo(float amount)
		{
			float canTake = Math.Min(amount, cargo);
			SpawnCargoFx((int)canTake);
			if (canTake > 0.0f)
			{
				cargo -= canTake;
				return canTake;
			}
			return 0.0f;
		}

		private void SpawnCargoFx(int amount)
		{
			if (refShip.gravPlanet != null)
			{
				Vector2 refinery = refShip.gravPlanet.position;
				Vector2 dir = (worldPosition - refinery);

				if (cargo > 0.0f)
					if (dir.Length() < 256.0f)
					{
						Game1.particleMaster.SpawnTractor(amount, worldPosition, Vector2.Normalize(refinery - worldPosition) * dir.Length() / 40.0f);
					}
			}
		}

		public override void Draw(SpriteBatch batch)
		{
			base.Draw(batch);

			if (resource != Resource.Void && cargo > 0)
			{
				float fraction = cargo / capactiy;
				batch.Draw(mineral, worldPosition, new Rectangle(0, 0, 32, 32), ResourceData.ResourceColor[resource], rotation, new Vector2(16, 16), fraction, SpriteEffects.None, 0.0f);
			}
		}

		public ushort PartId
		{
			get
			{
				return this.guid;
			}

			set
			{
				guid = value;
			}
		}

		private byte[] dataCache;
		public byte[] Data
		{
			get
			{
				BinaryFormatter bf = new BinaryFormatter();
				using (MemoryStream ms = new MemoryStream())
				{
					Serializer.Serialize(ms, new Tuple<short, float>((short)resource, cargo));
					dataCache = ms.ToArray();
					return dataCache;
				}
			}

			set
			{
				dataCache = value;
			}
		}

		private bool isDirty = false;
		public bool IsDirty
		{
			get
			{
				return isDirty;
			}

			set
			{
				isDirty = value;
			}
		}

		public void SetData()
		{
			MemoryStream memStream = new MemoryStream();
			BinaryFormatter binForm = new BinaryFormatter();
			memStream.Write(dataCache, 0, dataCache.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			Tuple<short, float> data = Serializer.Deserialize<Tuple<short, float>>(memStream);

			this.resource = (Resource)Enum.Parse(typeof(Resource), data.Item1.ToString());
			this.cargo = data.Item2;
		}
	}
}
