﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProtoBuf;
using System;
using System.Collections.Generic;
using WindowsGame1.ShipParts;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	public class RCSThruster : ShipPart
	{
		[NonSerialized()]
		private SpriteAnimation exhaust;

		public float thrust = 0.01f;

		[ProtoMember(1)]
		public bool isOn = false;

		public RCSThruster()
		{
			TypeID = 2;
		}

		public RCSThruster(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 2;
			mass = 20.0f;
		}

		public override void LoadContent()
		{
			exhaust = new SpriteAnimation();
			texture = ResourceManager.Load<Texture2D>("Thruster");
			exhaust.LoadContent("Exhaust");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}

		public void UpdateRotation(Ship ship)
		{
			if (parent.localCOMPosition.LengthSquared() < 0.00001f)
				return;
			float sDeg = MathHelper.ToDegrees(ship.rotation);

			float ang = sDeg;

			if (ang > 180)
				ang -= 360;
			if (ang < -180)
				ang += 360;

			Vector2 dir = new Vector2((float)Math.Cos(rotation), (float)Math.Sin(rotation));

			if (ang >= 0.0f)
			{
				dir = -new Vector2(dir.Y, -dir.X);
			}
			else
			{
				dir = new Vector2(dir.Y, -dir.X);
			}

			if (dir != Vector2.Zero)
				dir.Normalize();

			Vector2 r = worldPosition - ship.CentreOfMass;
			r.Normalize();
			float rDeg = MathHelper.ToDegrees((float)Math.Atan2(r.Y, r.X));

			if (rDeg > 180)
				rDeg -= 360;

			float turnDeg = rDeg - sDeg;

			float dirDeg = MathHelper.ToDegrees((float)Math.Atan2(dir.Y, dir.X));

			dirDeg += turnDeg;

			rotation = MathHelper.ToRadians(dirDeg);
		}

		public override void Action(Ship ship, bool isNet = false)
		{
			
			if (IsDestroyed)
			{
				ship.RemovePart(this);
				return;
			}
			if (!isNet)
			{
				UpdateRotation(ship);
			}
			exhaust.Update();
			isOn = ship.rcsOn;
			forward = new Vector2();
			forward.X = (float)Math.Cos(rotation);
			forward.Y = (float)Math.Sin(rotation);
		}

		public Vector2 forward;

		public override void Draw(SpriteBatch batch)
		{

			batch.Draw(texture, worldPosition, null, Color.White, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
			if (isOn)
				exhaust.Draw(batch, worldPosition - forward * 64.0f, rotation);
		}
	}
}
