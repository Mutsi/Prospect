﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using WindowsGame1.ShipParts;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	[ProtoInclude(200, typeof(MinerAuto))]
	class Miner : ShipPart, Activatable
	{
		[NonSerialized()]
		private Texture2D tractor, tractorGlow, beam;

		protected float laserRot = 0.0f;

		protected Vector2 dir = Vector2.Zero;

		protected float laserAnim = 0.0f;
		protected bool isOn = false;
		protected float laserDist = 0.0f;

		protected float mineSpeed = 0.002f;
		protected float mineRange = 15.0f * 32.0f;

		public Miner()
		{
			TypeID = 8;

		}
		public Miner(float x, float y)
		{
			TypeID = 8;
			Cost = 100;
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("GunMount");
			tractor = ResourceManager.Load<Texture2D>("TractorBeam");
			tractorGlow = ResourceManager.Load<Texture2D>("TractorGlow");
			beam = ResourceManager.Load<Texture2D>("TraktorBeam");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}




		protected Mineral miningMineral = null;

		public ushort PartId
		{
			get
			{
				return this.guid;
			}

			set
			{
				guid = value;
			}
		}

		private byte[] dataCache;
		public byte[] Data
		{
			get
			{
				BinaryFormatter bf = new BinaryFormatter();
				using (MemoryStream ms = new MemoryStream())
				{
					Serializer.Serialize(ms, new Tuple<bool, ushort, short>(isOn, (ushort)(laserDist), (short)(Math.Atan2(dir.Y, dir.X) * 100.0f)));

					//bf.Serialize(ms, new Tuple<bool, ushort, short>(isOn, (ushort)(laserDist), (short)(Math.Atan2(dir.Y, dir.X) * 100.0f)));
					dataCache = ms.ToArray();
					return dataCache;
				}
			}

			set
			{
				dataCache = value;
			}
		}

		private bool isDirty = false;
		public bool IsDirty
		{
			get
			{
				return true;
			}

			set
			{
				isDirty = value;
			}
		}

		public void SetData()
		{
			MemoryStream memStream = new MemoryStream();
			BinaryFormatter binForm = new BinaryFormatter();
			memStream.Write(dataCache, 0, dataCache.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			Tuple<bool, ushort, short> data = Serializer.Deserialize<Tuple<bool, ushort, short>>(memStream);

			this.isOn = data.Item1;
			this.laserDist = ((float)data.Item2);
			this.laserRot = ((float)data.Item3) / 100.0f;
			this.dir.X = (float)Math.Cos(laserRot);
			this.dir.Y = (float)Math.Sin(laserRot);
		}

		public override void Action(Ship ship, bool isNet = false)
		{
			if (isNet == true)
				return;
			isOn = ship.fireSystem && (ship.eCharge > 0.5f);
			miningMineral = null;

			if (isOn)
			{
				ship.eCharge -= 0.5f;
				laserDist = mineRange;
				Mineral nearest = null;
				float min = float.MaxValue;

				Planet target = ship.gravPlanet != null ? ship.gravPlanet : ship.gravPlanet;

				if (target != null && target.minerals != null)
					for (int i = 0; i < target.minerals.Count; i++)
					{
						Mineral m = target.minerals[i];

						if (!ship.HasCargoRoom(m.resource))
							continue;

						Rectangle partRect = new Rectangle((int)m.worldPos.X - 16, (int)m.worldPos.Y - 16, 32, 32);

						float dist;
						if (Physics.CollideRayRectangle(partRect, worldPosition, worldPosition + (dir * 32.0f * 15), out dist))
						{
							if (dist < min)
							{
								min = dist;
								nearest = m;
								laserDist = dist;
								miningMineral = nearest;
							}
						}
					}

				if (nearest != null)
				{
					Game1.particleMaster.SpawnTractor(1, nearest.worldPos, Vector2.Normalize(worldPosition - nearest.worldPos) * laserDist / 40.0f);
					nearest.scale -= mineSpeed;
					ship.VoidCargo.AddResource(nearest.resource, 0.1f);

					if (nearest.scale <= 0.0f)
						target.minerals.Remove(nearest);
				}
			}
			Vector2 aimAt = ship.MSWorld;
			if (miningMineral != null)
			{
				aimAt = miningMineral.worldPos;
			}
			dir = Vector2.Normalize(aimAt - worldPosition);
			laserRot = (float)Math.Atan2(dir.Y, dir.X);

			laserAnim += 1.0f;

		}

		public override void Draw(SpriteBatch batch)
		{
			batch.Draw(texture, worldPosition, null, Color.White, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);

			if (laserDist == 0)
			{
				laserDist = mineRange;
			}
			int segments = (int)(laserDist / 32.0f) + 1;
			if (isOn)
			{

				batch.Draw(tractorGlow, worldPosition, null, Color.White, laserAnim, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
				for (int i = 1; i < segments; i++)
				{
					batch.Draw(beam, worldPosition + (dir * 32.0f * i), new Rectangle((int)+laserAnim, 0, 32, 32), Color.Cyan, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
				}
			}
			batch.Draw(tractor, worldPosition, null, Color.White, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}


	}
}
