using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Steamworks;

using NetObj;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using WindowsGame1.NetPacks;
using LiteNetLib;
using System.Threading;
using LiteNetLib.Utils;
using WindowsGame1.Network;
using WindowsGame1.Helpers;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
	public class Game1 : Microsoft.Xna.Framework.Game
	{
		public const int UDPPort = 16785;
		public static GraphicsDeviceManager graphics;

		public const int SCRWIDTH = 1920;
		public const int SCRHEIGHT = 1080;

		public static int DEBUGPARTCOUNT = 0;

		public static List<Ship> GetShips;
		public static ParticleSystem.ParticleMaster particleMaster = new ParticleSystem.ParticleMaster();
		public static ContentManager ContentRef;
		public static GraphicsDevice GraphDevice;
		public static TomShane.Neoforce.Controls.Manager GUIManager;

		public static float Cash = 20000.0f;

		public static bool GetIsServer = false;
		public bool IsServer = false;

		SpriteBatch spriteBatch;
		Effect postFX;
		SoundEffect Prospect1Track;
		public static StandardBasicEffect fx;
		static List<Ship> ships = new List<Ship>();
		public static Ship currentShip;
		MouseState ms;

		Texture2D GridTexture;
		Texture2D backdrop;
		Texture2D backdropPlanet;
		Texture2D lightSpeed;

		public static Matrix cameraTransform;
		Effect MotionBlur;
		RenderTarget2D bgRT;
		RenderTarget2D finalWorldRT;

		ShipBuilderHUD builder = new ShipBuilderHUD();

		public static List<Planet> planets = new List<Planet>();
		static HUD hud;
		public static GUI GUI;

		public static float zoom = 1.0f;
		Random random;
		private float oldScroll;
		private Vector2 editorTarget;


		public int CurrentZoomLevel = 7;
		private float[] zoomLevels = { 0.0001f, 0.0005f, 0.001f, 0.005f, 0.01f, 0.05f, 0.1f, 0.25f, 0.5f, 0.75f, 1.0f, 2.0f };

		static UDPListener netListener;
		static UDPSender netSender;
		private Thread networkThread;

		public static BlockingCollection<Ship> otherShips = new BlockingCollection<Ship>();

		private static NetSocket currentNetSocket = null;
		public static NetSocket CurrentNetSocket
		{
			get { return currentNetSocket; }
		}

		public DateTime LastSent { get; private set; }

		public static bool RemoveBlock<T>(BlockingCollection<T> self, T itemToRemove)
		{
			lock (self)
			{
				T comparedItem;
				var itemsList = new List<T>();
				do
				{
					var result = self.TryTake(out comparedItem);
					if (!result)
						return false;
					if (!comparedItem.Equals(itemToRemove))
					{
						itemsList.Add(comparedItem);
					}
				} while (!(comparedItem.Equals(itemToRemove)));
				Parallel.ForEach(itemsList, t => self.Add(t));
			}
			return true;
		}

		public Game1()
		{
			SteamAPI.Init();

			InactiveSleepTime = new TimeSpan(0);

			GetShips = ships;
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

			IsFixedTimeStep = true;
			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferWidth = SCRWIDTH;
			graphics.PreferredBackBufferHeight = SCRHEIGHT;
			graphics.ApplyChanges();
			ContentRef = Content;

			GraphDevice = graphics.GraphicsDevice;
		}

		~Game1()
		{
		}


		public static void TestPackExec(NetPack pack)
		{
			Ship newShip = Ship.Unpack(pack.content);
			newShip.LoadContent(false);

			for (int j = 0; j < ships.Count; j++)
			{
				if (newShip.guid == ships[j].guid)
				{
					return;
				}
			}

			bool isNew = true;
			lock (otherShips)
			{
				for (int i = 0; i < otherShips.Count; i++)
				{
					if (otherShips.ElementAt(i).guid == newShip.guid)
					{
						newShip.parts = otherShips.ElementAt(i).parts;
						isNew = false;
						RemoveBlock<Ship>(otherShips, otherShips.ElementAt(i));
						otherShips.Add(newShip);
						break;
					}
				}
				if (isNew == true)
				{
					otherShips.Add(newShip);
				}
			}
		}

		public static void ShipPartPackExec(NetPack pack)
		{
			ShipPartPack partPack = ShipPartPack.Unpack(pack.content);

			if (netListener != null)
			{
				netListener.Broadcast(pack);
			}

			lock (otherShips)
			{
				foreach (Ship ship in otherShips)
				{
					if (ship.guid == partPack.ShipGuid)
					{
						partPack.ShipPart.LoadContent();
						ship.parts.Add(partPack.ShipPart);
						break;
					}
				}
			}
		}

		public static void ShipPartRemovePackExec(NetPack pack)
		{
			if (netListener != null)
			{
				netListener.Broadcast(pack);
			}

			ShipPartPack partPack = ShipPartPack.Unpack(pack.content);

			lock (otherShips)
			{
				foreach (Ship ship in otherShips)
				{
					if (ship.guid == partPack.ShipGuid)
					{
						ship.parts.Remove(partPack.ShipPart);
						break;
					}
				}
			}
		}

		public static void ShipPartUpdateExec(NetPack pack)
		{
			if (netListener != null)
			{
				netListener.Broadcast(pack);
			}

			ShipPartPack partPack = ShipPartPack.Unpack(pack.content);
			lock (otherShips)
			{
				foreach (Ship ship in otherShips)
				{
					if (ship.guid == partPack.ShipGuid)
					{
						var candidates = ship.parts.Where(o => o.guid == partPack.ShipPart.guid);
						if (candidates.Count() == 0)
							return;

						ShipPart local = candidates.First();
						if (local == null)
							return;

						local.localX = partPack.ShipPart.localX;
						local.localY = partPack.ShipPart.localY;
						break;
					}
				}
			}
		}

		public static void SendPart(ShipPart part, Ship ship)
		{
			ShipPartPack partPack = new ShipPartPack();
			byte[] data = partPack.Package(ship.guid, part);
			ShipPartData spd = new ShipPartData
			{
				IsRemove = false,
				Data = data
			};

			client.FirstPeer.Send(_netPacketProcessor.Write(spd), DeliveryMethod.ReliableOrdered);
		}

		public static void RemovePart(ShipPart part, Ship ship)
		{
			ShipPartPack partPack = new ShipPartPack();
			byte[] data = partPack.Package(ship.guid, part);
			ShipPartData spd = new ShipPartData
			{
				IsRemove = true,
				Data = data
			};

			client.FirstPeer.Send(_netPacketProcessor.Write(spd), DeliveryMethod.ReliableOrdered);
		}

		public static void UpdatePart(ShipPart part, Ship ship)
		{
			if (netSender != null)
			{
				ShipPartPack partPack = new ShipPartPack();

				NetPack pack = new NetPack();
				pack.content = partPack.Package(ship.guid, part);
				pack.PackID = 0x04;
				pack.isReliable = 0x1;
				netSender.SendPack(pack);
			}

			if (netListener != null)
			{
				ShipPartPack partPack = new ShipPartPack();

				NetPack pack = new NetPack();
				pack.content = partPack.Package(ship.guid, part);
				pack.PackID = 0x04;
				pack.isReliable = 0x1;
				netListener.Broadcast(pack);
			}
		}

		public static void InitNetwork()
		{
			EventBasedNetListener listener = new EventBasedNetListener();
			client = new NetManager(listener);
			client.Start();
			client.Connect("52.232.46.126", UDPPort, "prospectclient");

			listener.PeerConnectedEvent += (peer) =>
			{
				if (currentShip != null)
				{
					ShipSyncPack package = new ShipSyncPack();
					package.PartCount = (short)currentShip.parts.Count();
					package.Parts = currentShip.parts;
					package.ShipGuid = currentShip.guid;
					byte[] data = package.Package();

					ShipData sd = new ShipData
					{
						Data = data
					};
					try
					{
						client.FirstPeer.Send(_netPacketProcessor.Write(sd), DeliveryMethod.ReliableOrdered);
					}
					catch (Exception e)
					{

						throw e;
					}
				}
			};

			listener.NetworkReceiveEvent += (fromPeer, dataReader, deliveryMethod) =>
			{
				try
				{
					_netPacketProcessor.ReadAllPackets(dataReader, fromPeer);
				}
				catch
				{
				}
			};

			_netPacketProcessor.SubscribeReusable<ShipData, NetPeer>(OnShipSyncReceived);
			_netPacketProcessor.SubscribeReusable<ShipUpdate, NetPeer>(OnShipUpdateReceived);
			_netPacketProcessor.SubscribeReusable<ShipPartData, NetPeer>(OnShipPartReceived);

			NetworkActive = true;
			while (NetworkActive)
			{
				client.PollEvents();

				if (currentShip != null)
				{
					//Send update
					ShipUpdate su = new ShipUpdate();
					su.Data = currentShip.Package();
					client.FirstPeer.Send(_netPacketProcessor.Write(su), DeliveryMethod.Unreliable);
				}

				Thread.Sleep(15);
			}

			client.Stop();
			/*ExecutionMap.Map.Add(0x0, new NetPack.Execute(TestPackExec));
			ExecutionMap.Map.Add(0x02, new NetPack.Execute(ShipPartPackExec));
			ExecutionMap.Map.Add(0x03, new NetPack.Execute(ShipPartRemovePackExec));
			ExecutionMap.Map.Add(0x04, new NetPack.Execute(ShipPartUpdateExec));
			ExecutionMap.Map.Add(0x05, new NetPack.Execute(ShipSyncPack.ShipSyncExec));
			ExecutionMap.Map.Add(0x06, new NetPack.Execute(PartStatePack.PartStateExec));

			if (isServer)
			{
				netListener = new UDPListener();
				netListener.NewConnectionEvent += handleNewConnection;
				currentNetSocket = netListener;
				netListener.Listen();

			}
			else
			{
				netSender = new UDPSender("52.232.46.126"); //"84.104.184.25"
				netSender.NewConnectionEvent += handleNewConnection;
				currentNetSocket = netSender;

				NetPack pack = new NetPack();
				pack.content = Encoding.ASCII.GetBytes("Hello Server !");
				pack.PackID = 0x9;
				pack.isReliable = 0x0;

				netSender.SendPack(pack);
			}*/
		}

		private void handleNewConnection(object sender, ClientConnectedArgs e)
		{
			/*foreach (Ship ship in ships)
			{
				ShipSyncPack.SendShipSync(ship);
			}
			if (IsServer)
			{
				lock (otherShips)
				{
					foreach (Ship ship in otherShips)
					{
						ShipSyncPack.SendShipSync(ship, e.connection);
					}
				}
			}*/
		}

		protected override void Initialize()
		{
			GetIsServer = IsServer;
			IsMouseVisible = true;

			base.Initialize();
		}

		SoundEffect bgEffect;
		SoundEffectInstance instance;

		protected override void LoadContent()
		{
			GUIManager = new TomShane.Neoforce.Controls.Manager(this, graphics, "Blue");
			GUIManager.AutoCreateRenderTarget = true;
			GUIManager.Initialize();

			GUI = new GUI();
			spriteBatch = new SpriteBatch(GraphicsDevice);
			GridTexture = ResourceManager.Load<Texture2D>("Grid");
			backdrop = ResourceManager.Load<Texture2D>("Backdrop");
			backdropPlanet = ResourceManager.Load<Texture2D>("BackdropPlanet");
			lightSpeed = ResourceManager.Load<Texture2D>("Lightspeed");
			MotionBlur = ResourceManager.Load<Effect>("MotionBlur");

			//Preload
			ResourceManager.Load<Texture2D>("Dot");
			ResourceManager.Load<Texture2D>("Com");

			bgRT = new RenderTarget2D(GraphicsDevice, SCRWIDTH, SCRHEIGHT);
			finalWorldRT = new RenderTarget2D(GraphicsDevice, SCRWIDTH, SCRHEIGHT);
			rt1 = new RenderTarget2D(graphics.GraphicsDevice, SCRWIDTH, SCRHEIGHT);
			rt2 = new RenderTarget2D(graphics.GraphicsDevice, SCRWIDTH, SCRHEIGHT);
			overlay = new RenderTarget2D(graphics.GraphicsDevice, 1920, 1080);

			postFX = ResourceManager.Load<Effect>("Shaders/Postprocess");
			fx = new StandardBasicEffect(graphics.GraphicsDevice);

			//bgEffect = ResourceManager.Load<SoundEffect>("Track1");
			Prospect1Track = ResourceManager.Load<SoundEffect>("Music/ProspectTrack1");

			instance = Prospect1Track.CreateInstance();
			instance.IsLooped = true;
			instance.Play();


			random = new Random(0); //5150
			builder.LoadContent();
			particleMaster.LoadContent();
			SpawnPlanets(5);

			PersistWorld.StartGame();


			if (ships.Count == 0)
			{
				CreateNewShip();
			}

			//planets[0].position = new Vector2(0, 0);
			//planets[0].r = 25 * 32;
			//planets[0].hasAtmo = false;
			//planets[0].hasRefinery = true;

			currentShip = ships[0];

			networkThread = new Thread(new ThreadStart(InitNetwork));
			networkThread.Start();

			hud = new HUD();
			hud.LoadContent();
		}

		protected override void UnloadContent()
		{
		}

		protected override void OnExiting(Object sender, System.EventArgs args)
		{
			NetworkActive = false;
			networkThread.Join();

			if (netListener != null)
			{
				netListener.Shutdown();
				//Shutdown
			}
			if (netSender != null)
			{
				//Shutdown
				netSender.Shutdown();
			}

			PersistWorld.SaveGame();
			base.OnExiting(sender, args);
		}

		private void SpawnPlanets(int num)
		{
			for (int i = 0; i < num; i++)
			{
				Planet planet = new Planet(random);
				planet.LoadContent();
				planets.Add(planet);
				planet.planetID = i;
			}

			//Spawn moons
			List<Planet> moons = new List<Planet>();
			foreach (Planet planet in planets)
			{
				int numMoons = random.Next(0, 3);

				for (int i = 0; i < numMoons; i++)
				{
					Planet moon = new Planet(random);
					moon.LoadContent();
					moon.planetID = planets.Count;
					moon.parent = planet;
					moons.Add(moon);

					moon.isMoon = true;
					moon.r = (float)(random.NextDouble() * 0.25 + 0.25) * moon.parent.r;
					double moonCycle = random.NextDouble() * (Math.PI * 2.0);
					float moonRadius = (moon.parent.r + moon.r) * 2.5f * ((float)random.NextDouble() * 2.0f + 1.0f);

					moon.position.X = moon.parent.position.X + (float)Math.Cos(moonCycle) * moonRadius;
					moon.position.Y = moon.parent.position.Y + (float)Math.Sin(moonCycle) * moonRadius;
				}
				//Planet gravPlanet = null;

				////Calculate GravPlanet
				//Vector2 maxGrav = Vector2.Zero;
				//float maxForce = 0.0f;
				//foreach (Planet planet in Game1.planets)
				//{
				//	if (!planet.hasRefinery)
				//	{
				//		double dist = Vector2.Distance(position, planet.position);
				//		double force = (1.0 / (dist * dist)) * Ship.GravConstant * (4.0 / 3.0 * Math.PI * Math.Pow(planet.r, 3));

				//		Vector2 gravVec = (float)force * Vector2.Normalize(planet.position - position);
				//		if (gravVec.Length() > maxGrav.Length())
				//		{
				//			maxGrav = gravVec;
				//			gravPlanet = planet;
				//			maxForce = (float)force;
				//		}
				//	}
				//}
				//if (maxForce > 0.0005f && parent == null && gravPlanet != null)
				//{
				//	isMoon = true;
				//	parent = gravPlanet;

				//	r /= 2;
				//	moonRadius = (parent.r + this.r) * 1.5f * ((float)lrandom.NextDouble() * 2.0f + 1.0f) * 2.0f;
				//}
			}

			planets.AddRange(moons);

			foreach (Planet planet in planets)
			{

				//if (planet.hasRefinery)
				//	continue;
				//planet.MoonGenerate();
				planet.Populate();
			}

			HomePlanet = planets.OrderBy(o => o.r).First();

			int count = planets.Count(o => o.isMoon);
		}

		public static Planet HomePlanet;
		public static void CreateNewShip()
		{
			Ship ship = new Ship();
			ships.Add(ship);
			ship.LoadContent();

			ship.position = HomePlanet.position;
			ship.RootPosition = HomePlanet.position;
			ship.gravPlanet = HomePlanet;
		}

		protected override void Update(GameTime gameTime)
		{
			SteamAPI.RunCallbacks();

			currentShip.Name = SteamFriends.GetPersonaName();

			DEBUGPARTCOUNT = 0;

			ms = Mouse.GetState();
			KeyboardState ks = Keyboard.GetState();

			GUI.Update(ks);

			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || ks.IsKeyDown(Keys.Escape))
			{
				this.Exit();
			}

			if (ks.IsKeyDown(Keys.F2))
			{
				graphics.IsFullScreen = true;
				graphics.ApplyChanges();
			}

			if (ks.IsKeyDown(Keys.F4))
				SoundEffect.MasterVolume = 0.0f;
			if (ks.IsKeyDown(Keys.F5))
				SoundEffect.MasterVolume = 0.5f;
			if (ks.IsKeyDown(Keys.F6))
				SoundEffect.MasterVolume = 1.0f;


			Vector2 mouseWorld = Vector2.Transform(new Vector2(ms.X, ms.Y), Matrix.Invert(cameraTransform));
			for (int i = 0; i < ships.Count; i++)
			{
				if (!ships[i].inEditor)
				{
					if (ships[i].Parts.Count == 0)
					{
						ships.RemoveAt(i);
						continue;
					}
					else
					{
						bool hullFound = false;
						foreach (ShipPart part in ships[i].Parts)
						{
							if (part as Hull != null)
								hullFound = true;
						}

						if (!hullFound)
						{
							ships[i] = null;
							ships.RemoveAt(i);
							continue;
						}
					}
				}

				ships[i].Update((float)gameTime.ElapsedGameTime.TotalMilliseconds, planets);
			}

			hud.Update(ms, currentShip);

			currentShip.UserControl(ms, ks, mouseWorld);

			float msRelative = ms.ScrollWheelValue - oldScroll;
			oldScroll = ms.ScrollWheelValue;


			if (msRelative < 0)
			{
				CurrentZoomLevel--;
				if (CurrentZoomLevel < 0)
					CurrentZoomLevel = 0;
			}
			if (msRelative > 0)
			{
				CurrentZoomLevel++;
				if (CurrentZoomLevel > 11)
					CurrentZoomLevel = 11;
			}

			zoom = MathHelper.Lerp(zoom, zoomLevels[CurrentZoomLevel], 0.2f);

			if (builder.selectedPart == null || currentShip.Parts.Count > 0)
			{
				Vector2 editorCamTarget = currentShip.Parts.Count > 0 ? currentShip.CentreOfMass : HomePlanet.position;
				editorTarget = Vector2.Lerp(editorTarget, editorCamTarget, 1.0f);
			}

			Vector2 shipCamTarget = currentShip.Parts.Count > 0 ? currentShip.CentreOfMass : HomePlanet.position;

			Vector2 camTarget = currentShip.inEditor ? editorTarget : currentShip.CentreOfMass;
			cameraTransform = Matrix.CreateTranslation(new Vector3(SCRWIDTH / 2 / zoom, SCRHEIGHT / 2 / zoom, 0.0f) - new Vector3(camTarget, 0.0f));
			cameraTransform *= Matrix.CreateScale(zoom);
			if (currentShip.inEditor)
				builder.Update(ms, ks, currentShip, mouseWorld);


			foreach (Planet planet in planets)
			{
				planet.Update();
			}

			particleMaster.Update();

			GUIManager.Update(gameTime);

			//Net Stuff
			/*var timeSince = DateTime.Now - LastSent;
			if (timeSince > new TimeSpan(0, 0, 30))
			{
				ShipSyncPack.SendShipSync(currentShip);
				LastSent = DateTime.Now;
			}*/


			netCount++;
			if (netCount > netLimit)
			{
				netCount = 0;
				//netSender.SendPack(pack);
				//PartStatePack.SendShipSync(currentShip);
			}

			lock (otherShips)
			{
				foreach (Ship otherShip in otherShips)
				{
					otherShip.NetUpdate();
					if (otherShip.parts != null)
					{
						foreach (ShipPart part in otherShip.parts)
						{
							part.NetUpdate(otherShip);
						}
					}
				}
			}

			base.Update(gameTime);
		}

		private static void OnShipSyncReceived(ShipData arg1, NetPeer arg2)
		{
			ShipSyncPack pack = ShipSyncPack.Unpack(arg1.Data);

			lock (otherShips)
			{
				//Upsert
				if (!otherShips.Any(o => o.guid == pack.ShipGuid))
				{
					Ship ship = new Ship();
					ship.parts = pack.Parts;
					ship.guid = pack.ShipGuid;
					ship.LoadContent(true);
					otherShips.Add(ship);
				}
				else
				{
					otherShips.First(o => o.guid == pack.ShipGuid).parts = pack.Parts;
					otherShips.First(o => o.guid == pack.ShipGuid).LoadContent(true);
				}
			}
		}

		private static void OnShipUpdateReceived(ShipUpdate arg1, NetPeer arg2)
		{
			lock (otherShips)
			{
				Ship shipUpdate = Ship.Unpack(arg1.Data);

				Ship ship = otherShips.FirstOrDefault(o => o.guid == shipUpdate.guid);

				if (ship != null)
				{
					shipUpdate.parts = ship.parts;
					RemoveBlock<Ship>(otherShips, ship);
					otherShips.Add(shipUpdate);
				}
			}
		}

		private static void OnShipPartReceived(ShipPartData arg1, NetPeer arg2)
		{
			lock (otherShips)
			{
				ShipPartPack pack = ShipPartPack.Unpack(arg1.Data);
				Ship ship = otherShips.FirstOrDefault(o => o.guid == pack.ShipGuid);
				//Upsert
				if (ship != null)
				{
					pack.ShipPart.LoadContent();

					if (ship.parts == null)
						ship.parts = new List<ShipPart>();

					if (!arg1.IsRemove)
					{
						ship.parts.Add(pack.ShipPart);
					}
					else
					{
						ship.parts.RemoveAll(o => o.guid == pack.ShipPart.guid);
					}
				}
			}
		}

		RenderTarget2D overlay;
		RenderTarget2D rt1;
		RenderTarget2D rt2;
		private int netCount = 0;
		private int netLimit = 2;

		protected override void Draw(GameTime gameTime)
		{
			graphics.GraphicsDevice.SetRenderTarget(rt1);
			spriteBatch.Begin();
			spriteBatch.Draw(backdrop, new Vector2(0, 0), Microsoft.Xna.Framework.Color.White);
			spriteBatch.Draw(lightSpeed, Vector2.Zero, new Microsoft.Xna.Framework.Color(1.0f, 1.0f, 1.0f, 1.0f));
			spriteBatch.End();

			graphics.GraphicsDevice.SetRenderTarget(rt2);
			spriteBatch.Begin();
			Vector2 planetPos = Vector2.Zero;
			if (currentShip.AirFraction > float.Epsilon && currentShip.gravPlanet != null)
			{
				GraphicsDevice.Clear(currentShip.gravPlanet.atmoColor);
				planetPos = Vector2.Transform(currentShip.gravPlanet.position, cameraTransform);
				spriteBatch.Draw(backdropPlanet, planetPos, null, currentShip.gravPlanet.atmoColor, 0.0f, new Vector2(SCRWIDTH / 2, SCRWIDTH / 2), zoom + 0.5f, SpriteEffects.None, 0.0f);
			}
			spriteBatch.End();

			graphics.GraphicsDevice.SetRenderTarget(finalWorldRT);
			graphics.GraphicsDevice.Clear(Microsoft.Xna.Framework.Color.Transparent);

			spriteBatch.Begin();
			spriteBatch.Draw(rt1, Vector2.Zero, Microsoft.Xna.Framework.Color.White);
			if (currentShip.gravPlanet != null)
			{
				float aStr = currentShip.AirFraction * currentShip.gravPlanet.atmoStrenght;
				spriteBatch.Draw(rt2, Vector2.Zero, new Microsoft.Xna.Framework.Color(aStr, aStr, aStr, aStr));
			}
			spriteBatch.End();

			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp, null, null, null, cameraTransform);

			Vector2 cullPos = currentShip.Parts.Count > 0 ? currentShip.CentreOfMass : HomePlanet.position;
			foreach (Planet planet in planets)
			{
				planet.Draw(spriteBatch, cameraTransform, zoom, cullPos);
			}


			foreach (Ship ship in ships)
				ship.Draw(spriteBatch, (float)gameTime.ElapsedGameTime.TotalSeconds, ship == currentShip);

			lock (otherShips)
			{
				foreach (Ship otherShip in otherShips)
				{
					if (otherShip != null && otherShip.parts != null)
					{
						if (otherShip.parts.Count > 0)
						{
							foreach (ShipPart part in otherShip.parts)
							{
								part.Draw(spriteBatch);
							}
						}
					}
				}
			}

			//Draw Grid
			if (currentShip.inEditor)
			{
				for (int y = -240; y < 240; y += 32)
				{
					for (int x = -240; x < 240; x += 32)
					{
						spriteBatch.Draw(GridTexture, new Vector2(currentShip.RootPosition.X + x + 16, currentShip.RootPosition.Y + y + 16), Microsoft.Xna.Framework.Color.White);
					}
				}
			}
			spriteBatch.End();
			particleMaster.Draw(spriteBatch, cameraTransform);

			graphics.GraphicsDevice.SetRenderTarget(null);


			Matrix projection = Matrix.CreateOrthographicOffCenter(0,
				GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 0, 0, 1);
			Matrix halfPixelOffset = Matrix.CreateTranslation(-0.5f, -0.5f, 0);
			postFX.Parameters["MatrixTransform"].SetValue(halfPixelOffset * projection);
			Vector2 camPos = new Vector2(cameraTransform.Translation.X, cameraTransform.Translation.Y);
			camPos.Normalize();
			postFX.Parameters["PlayerPosition"].SetValue(camPos);

			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.PointWrap, null, null, postFX);
			spriteBatch.End();

			graphics.GraphicsDevice.SetRenderTarget(overlay);
			graphics.GraphicsDevice.Clear(Microsoft.Xna.Framework.Color.Transparent);

			//Draw screen (hud, gui etc)
			spriteBatch.Begin(SpriteSortMode.Immediate, null, SamplerState.PointWrap, null, null, null);
			if (currentShip.inEditor)
			{
				Vector2 mouseWorld = Vector2.Transform(new Vector2(ms.X, ms.Y), Matrix.Invert(cameraTransform));
				builder.Draw(spriteBatch, mouseWorld, zoom, currentShip, cameraTransform);
			}
			hud.Draw(spriteBatch, currentShip, (int)(1920), (int)(1080));
			GUI.Draw(spriteBatch);

			//Draw other player labels
			lock (otherShips)
			{
				foreach (Ship otherShip in otherShips)
				{
					if (otherShip != null && otherShip.parts != null)
					{
						if (otherShip.parts.Count > 0)
						{
							Vector2 shipScrPos = Vector2.Transform(otherShip.CentreOfMass, cameraTransform);
							Vector2 textsize = HUD.font.MeasureString(otherShip.Name);

							Vector2 offset = new Vector2(-textsize.X / 2, -textsize.Y / 2);

							spriteBatch.DrawString(HUD.font, otherShip.Name, shipScrPos + offset, Color.Yellow);
						}
					}
				}
			}

			spriteBatch.End();

			GUIManager.Draw(gameTime);

			graphics.GraphicsDevice.SetRenderTarget(null);
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp, null, null, null);
			spriteBatch.Draw(finalWorldRT, Vector2.Zero, Microsoft.Xna.Framework.Color.White);
			spriteBatch.Draw(overlay, new Rectangle(0, 0, SCRWIDTH, SCRHEIGHT), Microsoft.Xna.Framework.Color.White);
			spriteBatch.Draw(GUIManager.RenderTarget, Vector2.Zero, Microsoft.Xna.Framework.Color.White);
			spriteBatch.End();

			Gizmos.DrawRays(spriteBatch, graphics.GraphicsDevice, fx);

			/*foreach (Ship ship in ships)
			{

				ship.CalculateEllipsoid();
				ship.DrawEllipsoid();
			}*/

			//CalculateEllipsoid();
			//DrawEllipsoid(fx);


			base.Draw(gameTime);
		}

		VertexPositionColor[] Path = new VertexPositionColor[64];
		float CenterX = 0;
		float CenterY = 0;
		float MajorAxisA = 2000;
		float MinorAxisB = 1500;
		private static bool NetworkActive;
		private static NetPacketProcessor _netPacketProcessor = new NetPacketProcessor();
		private static NetManager client;

		private void CalculateEllipsoid()
		{
			if (currentShip != null && currentShip.OrbitalElement != null)
			{
				Vector2 camPos = new Vector2(cameraTransform.Translation.X, cameraTransform.Translation.Y);

				MajorAxisA = currentShip.OrbitalElement.SemiMajorAxis;
				MinorAxisB = currentShip.OrbitalElement.SemiMinorAxis;

				float fociDistance = (float)Math.Sqrt(MajorAxisA * MajorAxisA - MinorAxisB * MinorAxisB);

				Vector2 rotatedFoci = new Vector2(fociDistance, 0.0f);
				rotatedFoci = rotatedFoci.Rotate(currentShip.OrbitalElement.ArgumentOfPeriapsis);

				CenterX = currentShip.gravPlanet.position.X + rotatedFoci.X;
				CenterY = currentShip.gravPlanet.position.Y + rotatedFoci.Y;

				Vector2 center = new Vector2(CenterX, CenterY);

				Vector2 centerTransformed = Vector2.Transform(center, cameraTransform);
				CenterX = centerTransformed.X;
				CenterY = centerTransformed.Y;
			}

			Path = new VertexPositionColor[64];
			List<VertexPositionColor> Vertices = new List<VertexPositionColor>();


			Vector3 axis = new Vector3(0, 0, -1);
			float angle = currentShip.OrbitalElement.ArgumentOfPeriapsis;
			for (double i = 0; i < 2 * Math.PI; i += 0.0001)
			{
				var xx = (this.MajorAxisA * zoom) * (float)(Math.Cos(i));
				var yy = (this.MinorAxisB * zoom) * (float)(Math.Sin(i));

				Vector3 add3 = Vector3.Transform(new Vector3(xx, yy, 0.0f), Matrix.CreateFromAxisAngle(axis, angle));

				var x = this.CenterX + add3.X;
				var y = this.CenterY + add3.Y;

				//Vector2 rot = new Vector2(x, y);
				//rot = rot.Rotate(currentShip.OrbitalElement.ArgumentOfPeriapsis);
				//Vector3 rot3 = Vector3.Transform(new Vector3(x, y, 0), Matrix.CreateFromAxisAngle(axis, angle));

				Vertices.Add(new VertexPositionColor(new Vector3(x, y, add3.Z), Color.Red));
			}

			Vertices.Add(new VertexPositionColor(
				new Vector3((float)(this.CenterX + (this.MajorAxisA * zoom) *
					(float)(Math.Cos(0))),
					(float)(this.CenterY + (this.MinorAxisB * zoom) * (float)(Math.Sin(0))), 0), Color.DarkGray));

			Path = Vertices.ToArray();
		}

		public void DrawEllipsoid(BasicEffect effect)
		{
			//BasicEffect effect = new BasicEffect(GraphDevice);
			//effect.World = Matrix.CreateTranslation(new Vector3(0,0,0));
			//effect.LightingEnabled = false;
			//effect.TextureEnabled = false;
			//effect.VertexColorEnabled = true;
			//effect.CurrentTechnique.Passes[0].Apply();
			effect.CurrentTechnique.Passes[0].Apply();
			graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, Path, 0, Path.Length - 1);
		}
	}

	public class StandardBasicEffect : BasicEffect
	{
		public StandardBasicEffect(GraphicsDevice graphicsDevice)
			: base(graphicsDevice)
		{
			this.VertexColorEnabled = true;
			this.Projection = Matrix.CreateOrthographicOffCenter(
				0, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height, 0, 0, 1);
		}

		public StandardBasicEffect(BasicEffect effect)
			: base(effect) { }

		public new BasicEffect Clone()
		{
			return new StandardBasicEffect(this);
		}
	}
}
