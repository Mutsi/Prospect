﻿using CelestialMechanics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using WindowsGame1.Helpers;
using WindowsGame1.Models;

namespace WindowsGame1
{
	public static class Vector2Extension
	{
		public static Vector2d Rotate(this Vector2d v, float radians)
		{
			double sin = Math.Sin(radians);
			double cos = Math.Cos(radians);

			double tx = v.X;
			double ty = v.Y;
			v.X = (cos * tx) - (sin * ty);
			v.Y = (sin * tx) + (cos * ty);
			return v;
		}
		public static Vector2d SafeNormalize(this Vector2d v)
		{
			if (v.Length == 0.0f)
				return Vector2d.Zero;
			else
				return Vector2d.Normalize(v);
		}

		public static Vector2 Rotate(this Vector2 v, float radians)
		{
			double sin = Math.Sin(radians);
			double cos = Math.Cos(radians);

			double tx = v.X;
			double ty = v.Y;
			v.X = (float)((cos * tx) - (sin * ty));
			v.Y = (float)((sin * tx) + (cos * ty));
			return v;
		}
		public static Vector2 SafeNormalize(this Vector2 v)
		{
			if (v.Length() == 0.0f)
				return Vector2.Zero;
			else
				return Vector2.Normalize(v);
		}
	}

	[Serializable()]
	[ProtoContract]
	public class Ship
	{
		[NonSerialized()]
		public Texture2D dotTexture;
		[NonSerialized()]
		public Texture2D comTexture;
		[NonSerialized()]
		KeyboardState oldKS;
		[NonSerialized()]
		public Planet gravPlanet;
		[NonSerialized()]
		public float outOfBalance = 0.0f;

		public const float DEG2RAD = 0.017453292f;
		public const float RAD2DEG = 57.29578049f;

		public float mass = 0.0f;

		public Vector2d position = Vector2d.Zero;
		public Vector2 linearV = Vector2.Zero;

		[NonSerialized()]
		public float VisViva = 0.0f;
		[NonSerialized()]
		public float SemiMajor = 0.0f;
		[NonSerialized()]
		public float Eccentricity = 0.0f;
		[NonSerialized()]
		public float SpecificEnergy = 0.0f;

		public float Throttle = 1.0f;
		public float angularV = 0.0f;
		public Vector2d CentreOfMass;

		public Vector2d RootPosition;
		[NonSerialized()]
		public float AirDrag;
		[NonSerialized()]
		public float AirDragAngular;
		[NonSerialized()]
		public float AirFraction;
		Vector2d maxGrav = Vector2d.Zero;
		[NonSerialized()]
		public float DeltaTime;

		public Vector2 mouseDir;
		public Vector2 MSWorld;
		public bool fireSystem = false;

		public const double GravConstant = 0.00000058f;

		[NonSerialized()]
		float dtBalance;
		[NonSerialized()]
		public float crossSection;
		[NonSerialized()]
		private bool onPlanet;

		public ReadOnlyCollection<ShipPart> Parts
		{
			get { return parts.AsReadOnly(); }
		}

		public bool HasCargoRoom(Resource res)
		{
			List<Cargobay> cargos = parts.Where(o => o as Cargobay != null).Cast<Cargobay>().ToList();
			return cargos.Any(o =>
				(o.CargoType == res || o.CargoType == Resource.Void) && o.HasRoom);
		}

		public float CargoSpaceLeft(Resource res)
		{
			List<Cargobay> cargos = parts.Where(o => o as Cargobay != null).Cast<Cargobay>().ToList();
			return cargos.Where(o =>
				(o.CargoType == res || o.CargoType == Resource.Void) && o.HasRoom).Sum(o => o.SpaceLeft);
		}

		public float GetCargo(Resource res)
		{
			List<Cargobay> cargos = parts.Where(o => o as Cargobay != null).Cast<Cargobay>().ToList();
			return cargos.Where(o => o.CargoType == res).Sum(o => o.CargoAmount);
		}

		public float AddCargo(Resource res, float amount)
		{
			List<Cargobay> cargos = parts.Where(o => o as Cargobay != null).Cast<Cargobay>().ToList();
			cargos = cargos.Where(o => (o.CargoType == res || o.CargoType == Resource.Void) && o.HasRoom).ToList();

			float sumGiven = 0.0f;
			float remaining = amount;

			foreach (Cargobay cargo in cargos)
			{
				float given = cargo.PutCargo(res, remaining);
				sumGiven += given;
				remaining = amount - sumGiven;

				if (remaining == 0.0f)
					return sumGiven;
			}

			return sumGiven;
		}

		public float RemoveCargo(Resource res, float amount)
		{
			List<Cargobay> cargos = parts.Where(o => o as Cargobay != null).Cast<Cargobay>().ToList();
			cargos = cargos.Where(o => o.CargoType == res).ToList();

			float sumTaken = 0.0f;
			float remaining = amount;

			foreach (Cargobay cargo in cargos)
			{
				float taken = cargo.TakeCargo(remaining);
				sumTaken += taken;
				remaining = amount - sumTaken;

				if (remaining == 0.0f)
					return sumTaken;
			}

			return sumTaken;
		}

		[NonSerialized()]
		public OrbitalElements OrbitalElement;

		[NonSerialized()]
		public Matrix PositionRotationMatrix = Matrix.Identity;
		private bool isDirty = false;

		public Vector2 Forward = new Vector2();

		public float eChargeNew = 0.0f;
		public float eChargeMax = 0.0f;
		public float eCharge = 0.0f;

		public bool inEditor = true;

		[NonSerialized]
		public Rectangle Bounds = Rectangle.Empty;

		[NonSerialized]
		public CargoController VoidCargo = new CargoController();

		[ProtoMember(1)]
		public string Name = "Unnamed Ship";
		//[ProtoMember(2)]
		public List<ShipPart> parts = new List<ShipPart>();
		[ProtoMember(2)]
		public float rotation = 0.0f;
		[ProtoMember(3)]
		public double posX { get { return position.X; } set { position.X = value; } }
		[ProtoMember(4)]
		public double posY { get { return position.Y; } set { position.Y = value; } }
		[ProtoMember(5)]
		public double comX { get { return CentreOfMass.X; } set { CentreOfMass.X = value; } }
		[ProtoMember(6)]
		public double comY { get { return CentreOfMass.Y; } set { CentreOfMass.Y = value; } }
		[ProtoMember(7)]
		public Guid guid = Guid.Empty;
		[ProtoMember(8)]
		public bool rcsOn = false;
		[ProtoMember(9)]
		public bool thrusterOn = false;

		public ushort partIdCur = 0;

		public void AddPart(ShipPart part)
		{
			part.guid = partIdCur;
			partIdCur++;
			parts.Add(part);
			CalculateRootPosition();
			Game1.SendPart(part, this);

			if (part as Hull != null)
				isDirty = true;
		}

		public void RemoveAllParts()
		{
			while (parts.Count > 0)
				RemovePart(parts[0]);
		}

		public void RemovePart(ShipPart part)
		{
			if (part == null)
				return;

			part.parent = null;
			part.IsDestroyed = true;

			Hull hull = part as Hull;
			if (hull != null)
			{
				if (hull.IsRoot)
				{
					if (parts.Count > 1)
						findNewRoot(hull);
				}
			}
			if (!parts.Remove(part))
			{
				//throw new Exception("Could not destroy part");
			}

			//TODO: CLEANUP
			foreach (ShipPart transferPart in parts)
			{
				transferPart.Transfer(this);
			}

			CalculateCenterOfMass();
			Game1.RemovePart(part, this);

			if (hull != null)
				isDirty = true;
		}

		private void findNewRoot(Hull currentHull)
		{
			foreach (ShipPart part in parts)
			{
				Hull newRoot = part as Hull;
				if (newRoot != null && newRoot != currentHull)
				{
					newRoot.IsRoot = true;
					return;
				}
			}

			//No hull found -> delete this ship
			parts.Clear();
		}


		public Ship()
		{
		}

		public Ship(Texture2D dot, Texture2D com)
		{
			dotTexture = dot;
			comTexture = com;
		}


		public void LoadContent()
		{
			if (guid == Guid.Empty)
				guid = Guid.NewGuid();

			dotTexture = ResourceManager.Load<Texture2D>("Dot");
			comTexture = ResourceManager.Load<Texture2D>("COM");

			VoidCargo = new CargoController();
		}

		public void LoadContent(bool LoadParts)
		{
			if (guid == Guid.Empty)
				guid = Guid.NewGuid();

			dotTexture = ResourceManager.Load<Texture2D>("Dot");
			comTexture = ResourceManager.Load<Texture2D>("COM");
			VoidCargo = new CargoController();

			gravPlanet = null;

			if (parts != null)
				foreach (ShipPart part in parts)
				{
					part.LoadContent();
				}
		}
		public void CalculateCenterOfMass()
		{
			int partCount = parts.Count;
			if (partCount == 0)
				return;

			float totalmass = 0.0f;
			float totalx = 0.0f;
			float totaly = 0.0f;

			foreach (ShipPart h in parts)
			{
				totalmass += h.Mass();
				totalx += (float)h.localX * h.Mass() * 32.0f;
				totaly += (float)h.localY * h.Mass() * 32.0f;
			}

			CentreOfMass = position + new Vector2d(totalx / totalmass, totaly / totalmass);
		}


		private float CalculateMass()
		{
			float totalMass = 0.0f;
			foreach (IPart part in parts)
				totalMass += part.Mass();

			return totalMass;
		}

		float CrossProduct(Vector2 v1, Vector2 v2)
		{
			return (v1.X * v2.Y) - (v1.Y * v2.X);
		}


		private void CalculateBounds()
		{
			float minX = float.MaxValue, maxX = float.MinValue;
			float minY = float.MaxValue, maxY = float.MinValue;

			foreach (IPart part in parts)
			{
				Vector2 p = part.WorldPosition().xna;
				minX = Math.Min(minX, p.X);
				maxX = Math.Max(maxX, p.X);
				minY = Math.Min(minY, p.Y);
				maxY = Math.Max(maxY, p.Y);
			}

			Vector2 dimension = new Vector2(maxX, maxY) - new Vector2(minX, minY);

			Bounds = new Rectangle((int)minX - 16, (int)minY - 16, (int)(dimension.X) + 32, (int)(dimension.Y) + 32);
		}

		void ApplyImpulse(Ship A, Ship B)
		{
			foreach (IPart partA in A.parts)
			{
				if (partA as Hull == null)
					continue;

				Rectangle recA = new Rectangle((int)(partA.WorldPosition().X), (int)(partA.WorldPosition().Y), 32, 32);
				foreach (IPart partB in B.parts)
				{
					if (partB as Hull == null)
						continue;

					Rectangle recB = new Rectangle((int)(partB.WorldPosition().X), (int)(partB.WorldPosition().Y), 32, 32);
					if (recA.Intersects(recB))
					{
						Game1.particleMaster.SpawnDebries(1, Vector2d.Lerp(partA.WorldPosition(), partB.WorldPosition(), 0.5f).xna);

						if ((partA as Hull).Damage(0.001f, A))
						{
							return;
						}
						if ((partB as Hull).Damage(0.001f, B))
						{
							return;
						}


						Vector3 normala3 = Vector3.Normalize(new Vector3((float)A.CentreOfMass.X, (float)A.CentreOfMass.Y, 0.0f) - new Vector3((float)recA.Center.X, (float)recA.Center.Y, 0.0f));
						Vector3 normalb3 = Vector3.Normalize(new Vector3((float)B.CentreOfMass.X, (float)B.CentreOfMass.Y, 0.0f) - new Vector3((float)recB.Center.X, (float)recB.Center.Y, 0.0f));

						Vector3 rotation3a = new Vector3(0, A.angularV, 0);
						Vector3 rotation3b = new Vector3(0, B.angularV, 0);

						Vector3 av = new Vector3(A.linearV.X, A.linearV.Y, 0.0f) + Vector3.Cross(normala3, rotation3a);
						Vector3 bv = new Vector3(B.linearV.X, B.linearV.Y, 0.0f) + Vector3.Cross(normalb3, rotation3b);

						Vector3 rv = bv - av;
						Vector2 rv2 = new Vector2(rv.X, rv.Y);

						Vector2 normala = new Vector2(normalb3.X, normalb3.Y);

						float contactVel = Vector2.Dot(rv2, normala);

						// Do not resolve if velocities are separating
						if (contactVel > 0)
							return;

						// Calculate restitution
						float e = MathHelper.Min(0.5f, 0.5f);

						// Calculate impulse scalar
						float j = -(1.0f + e) * contactVel;
						j /= (1.0f / A.mass) + (1.0f / B.mass);

						// Apply impulse
						Vector2 impulse = j * normala;
						A.linearV -= (1.0f / A.mass) * impulse;
						B.linearV += (1.0f / B.mass) * impulse;

						float cacheAngV = A.angularV;
						A.angularV = B.angularV;
						B.angularV = A.angularV;

					}
				}
			}

		}

		private float findTime(float dAng, float vAng, float aAng)
		{
			float det = vAng * vAng - 4 * aAng * dAng;
			float t1, t2;
			if (det >= 0)
			{
				t1 = (-vAng - (float)Math.Sqrt(det)) / (2.0f * aAng);
				t2 = (-vAng + (float)Math.Sqrt(det)) / (2.0f * aAng);
				if (t1 >= 0 && t2 >= 0)
					return Math.Min(t1, t2);
				if (t1 >= 0)
					return t1;
				if (t2 >= 0)
					return t2;
			}
			return float.MaxValue;
		}

		private const float tau = (float)(2.0f * Math.PI);

		private Vector2 rcsThrust(Vector2 target)
		{
			Vector2 thrust = Vector2.Zero;

			foreach (IPart part in parts)
			{
				RCSThruster thruster = part as RCSThruster;
				if (thruster == null)
					continue;

				thrust += thruster.forward * thruster.thrust;
			}
			thrust *= (1.0f / mass);

			return thrust;
		}

		private float rcsTorque()
		{
			float aAng = 0.0f;
			foreach (RCSThruster thruster in parts.OfType<RCSThruster>())
			{
				if (thruster.parent.LocalCOMPosition().LengthSquared() < 0.00001f)
					continue;
				float r = thruster.parent.LocalCOMPosition().Length();
				aAng += thruster.thrust * r;
			}

			foreach (RCSWheel rcsWheel in parts.OfType<RCSWheel>())
			{
				if (rcsWheel.isOn)
				{
					float r = rcsWheel.parent.LocalCOMPosition().Length();
					if (r < 16.0f)
						r = 16.0f;
					aAng += (1.0f / r) * rcsWheel.torque;
				}
			}

			aAng *= (1.0f / mass);

			return aAng;
		}

		bool planetBoundsCollide(Planet planet)
		{
			// Find the closest point to the circle within the rectangle
			float closestX = MathHelper.Clamp((float)planet.position.X, Bounds.Left, Bounds.Right);
			float closestY = MathHelper.Clamp((float)planet.position.Y, Bounds.Top, Bounds.Bottom);

			// Calculate the distance between the circle's center and this closest point
			float distanceX = (float)planet.position.X - closestX;
			float distanceY = (float)planet.position.Y - closestY;

			// If the distance is less than the circle's radius, an intersection occurs
			float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
			return distanceSquared < (planet.r * planet.r);
		}

		bool CollidePlanet(Planet planet, bool shouldBreak)
		{
			if (planet == null)
			{
				Console.WriteLine("@@@ PLANET NULL IN COLLIDE PLANET @@@@");
				return false;
			}
			if (!planetBoundsCollide(planet))
				return false;

			bool collided = false;
			for (int i = 0; i < parts.Count; i++)
			{
				if (parts[i] as Hull == null)
					continue;
				double dist = Vector2d.Distance(parts[i].WorldPosition(), planet.position);
				if (dist < 16 + planet.r)
				{
					if (CalculateRelativeVelocityMagnitude() > 1.0f && shouldBreak && !onPlanet)
					{
						RemovePart(parts[i]);
					}
					collided = true;
				}
			}

			return collided;
		}

		private void rcsFire(Vector2 target)
		{
			float aAng = 0.0f;
			float aAngMax = rcsTorque();

			float tAng = (float)Math.Atan2(target.Y, target.X);
			float dAng = ((tAng - rotation) % tau + tau) % tau;
			var t1 = findTime(-(dAng - tau), angularV, -aAngMax);
			var t2 = findTime(-(dAng), angularV, aAngMax);

			float t = Math.Min(t1, t2);
			if (t >= 1)
			{
				rcsOn = true;

				var dir = t1 < t2 ? -1 : 1;
				var brake = dir * angularV - t * aAngMax;
				if (brake >= .01)
					aAng = -2 * angularV * t;
				else if (brake < 0)
					aAng = dir * aAngMax;
				else
					aAng = 0;
			}
			else
			{
				aAng = 0;
				if (Math.Abs(angularV) <= aAngMax)
				{
					angularV = 0;
				}
			}
			aAng = Math.Max(-aAngMax, Math.Min(aAng, aAngMax));

			angularV += aAng;
			linearV += rcsThrust(target);
		}


		Vector2 calculateDragAcceleration(Planet planet)
		{
			if (linearV == Vector2.Zero)
				return Vector2.Zero;

			float dragCoef = 1.0f;
			crossSection = CalculateCrossSection();
			float ForceDrag = 0.5f * (planet.atmoStrenght * 3.0f) * (linearV.Length() * linearV.Length()) * dragCoef * crossSection;
			float ForceDragAngular = 5f * (planet.atmoStrenght * 3.0f) * (angularV * angularV) * dragCoef * crossSection;

			float distToPlanet = (float)Vector2d.Distance(CentreOfMass, planet.position);
			float falloff = (float)(1.0 - ((distToPlanet - planet.r) / (planet.r * 1.5 - planet.r)));
			falloff = MathHelper.Clamp(falloff, 0.0f, 1.0f);
			falloff = falloff * falloff;

			AirFraction = falloff;
			AirDrag = ForceDrag * falloff;

			float angForceDrag = 0.5f * (planet.atmoStrenght * 3.0f) * (linearV.Length() * linearV.Length()) * dragCoef;
			angularV += (angForceDrag * outOfBalance * falloff * 0.01f) / (float)parts.OfType<Hull>().Count();

			AirDragAngular = ForceDragAngular * falloff;

			return linearV.SafeNormalize() * (ForceDrag / mass) * falloff;
		}

		Vector2 calculateDragAcceleration(Planet planet, Vector2 vel, Vector2d pos, float CrossSection)
		{
			float density = planet.atmoStrenght * 3.0f; //1.2 being air kg/m^3
			float dragCoef = 1.0f;
			float ForceDrag = 0.5f * density * (vel.Length() * vel.Length()) * dragCoef * CrossSection;

			float distToPlanet = (float)Vector2d.Distance(pos, planet.position);
			float falloff = 1.0f - (((float)distToPlanet - planet.r) / (planet.r * 1.5f - planet.r));
			falloff = MathHelper.Clamp(falloff, 0.0f, 1.0f);
			falloff = MathHelper.Clamp(falloff, 0.0f, 1.0f);
			falloff = falloff * falloff;

			return linearV.SafeNormalize() * (ForceDrag / mass) * falloff;
		}

		float CalculateRelativeVelocityMagnitude()
		{

			//TODO FIX GRAVPLANET NULL REF AFTER LOADING SHIP ON PLANET
			//return linearV.Length();

			//This might fix issue stated above
			if (gravPlanet == null)
			{
				return linearV.Length();
			}

			return (linearV - (gravPlanet.Velocity / DeltaTime)).Length();
		}

		bool NaNCheck(float value)
		{
			return float.IsNaN(value) || float.IsInfinity(value);
		}
		bool NaNCheck(double value)
		{
			return double.IsNaN(value) || double.IsInfinity(value);
		}

		public float CalculateCrossSection()
		{
			if (parts.Count == 0)
			{
				return 0.0f;
			}
			Vector2 v = Forward;
			Vector2 cross = new Vector2(v.Y, -v.X);
			Vector2 fwd = linearV.SafeNormalize();
			Vector2 crossFwd = new Vector2(fwd.Y, -fwd.X);

			float min0 = float.MaxValue;
			float max0 = float.MinValue;
			float min1 = float.MaxValue;
			float max1 = float.MinValue;

			foreach (ShipPart part in parts)
			{
				Vector2 p = (part.WorldPosition() - CentreOfMass).SafeNormalize().xna;
				float distance = (float)Vector2d.Distance(part.WorldPosition(), CentreOfMass) + 16;
				float d0 = Vector2.Dot(cross, p) * distance;
				float d1 = Vector2.Dot(v, p) * distance;

				if (d0 < min0)
				{
					min0 = d0;
					aeroMin0 = part.worldPosition.xna;
				}
				if (d0 > max0)
				{
					max0 = d0;
					aeroMax0 = part.worldPosition.xna;
				}
				if (d1 < min1)
				{
					min1 = d1;
					aeroMin1 = part.worldPosition.xna;
				}
				if (d1 > max1)
				{
					max1 = d1;
					aeroMax1 = part.worldPosition.xna;
				}
			}

			float outOfBalance0 = Math.Abs(min0) / 32 - max0 / 32;
			float outOfBalance1 = Math.Abs(min1) / 32 - max1 / 32;
			outOfBalance = outOfBalance0 + outOfBalance1;

			int minY = parts.Min(o => o.localY);
			int maxY = parts.Max(o => o.localY);
			float lengthY = Math.Abs(minY - maxY) * 32.0f + 32.0f;

			int minX = parts.Min(o => o.localX);
			int maxX = parts.Max(o => o.localX);
			float lengthX = Math.Abs(minX - maxX) * 32.0f + 32.0f;

			float dotFwd = Vector2.Dot(Forward, fwd);
			float dotCross = Vector2.Dot(Forward, crossFwd);
			float dotRight = Vector2.Dot(Vector2.UnitX, fwd);

			int crossedX = (int)(Math.Abs(lengthX * dotCross));
			int crossedY = (int)(Math.Abs(lengthY * dotFwd));

			float a0 = Vector2.Distance(aeroMin0, CentreOfMass.xna) * (Vector2.Dot((aeroMin0 - CentreOfMass.xna).SafeNormalize(), crossFwd));
			aeroMin0End = aeroMin0 - fwd * a0;

			float a1 = Vector2.Distance(aeroMin1, CentreOfMass.xna) * (Vector2.Dot((aeroMin1 - CentreOfMass.xna).SafeNormalize(), crossFwd));
			aeroMin1End = aeroMin1 - fwd * a1;

			float a2 = Vector2.Distance(aeroMax0, CentreOfMass.xna) * (Vector2.Dot((aeroMax0 - CentreOfMass.xna).SafeNormalize(), crossFwd));
			aeroMax0End = aeroMax0 - fwd * a2;

			float a3 = Vector2.Distance(aeroMax1, CentreOfMass.xna) * (Vector2.Dot((aeroMax1 - CentreOfMass.xna).SafeNormalize(), crossFwd));
			aeroMax1End = aeroMax1 - fwd * a3;

			float sumTorque = a0 + a1 + a2 + a3;
			outOfBalance = sumTorque / 32.0f;


			float torque = 0.0f;
			foreach (ShipPart aeroSurface in parts.OfType<Hull>())
			{
				float partTorque = getAeroTorqueForPart(aeroSurface);
				torque += partTorque;
			}
			outOfBalance = torque / 32.0f;
			//outOfBalance = 0;
			return Math.Max(crossedX, crossedY);
		}

		float getAeroTorqueForPart(ShipPart part)
		{
			Vector2 fwd = linearV.SafeNormalize();
			Vector2d crossFwd = new Vector2d(fwd.Y, -fwd.X);

			float a0 = (float)(Vector2d.Distance(part.worldPosition, CentreOfMass) * (Vector2d.Dot((part.worldPosition - CentreOfMass).SafeNormalize(), crossFwd)));

			return a0;
		}

		Vector2 aeroMin0 = new Vector2();
		Vector2 aeroMax0 = new Vector2();
		Vector2 aeroMin1 = new Vector2();
		Vector2 aeroMax1 = new Vector2();

		Vector2 aeroMin0End = new Vector2();
		Vector2 aeroMax0End = new Vector2();
		Vector2 aeroMin1End = new Vector2();
		Vector2 aeroMax1End = new Vector2();

		//On Collision
		void CollideWithBody(Ship other)
		{
			if (this == other)
				return;

			if (Bounds.Intersects(other.Bounds))
			{

				ApplyImpulse(this, other);
			}
		}


		public void UserControl(MouseState ms, KeyboardState ks, Vector2 mouseWorld)
		{
			rcsOn = false;
			thrusterOn = false;

			mouseDir = mouseWorld - CentreOfMass.xna;
			MSWorld = mouseWorld;

			if (ks.IsKeyDown(Keys.X) && oldKS.IsKeyUp(Keys.X) && linearV.Length() < 0.03f)
			{
				position = new Vector2d((int)(position.X / 32) * 32, (int)(position.Y / 32) * 32);
				CalculateRootPosition();
				inEditor = !inEditor;
				linearV = Vector2.Zero;
				rotation = 0.0f;
				angularV = 0.0f;

				foreach (ShipPart part in parts)
				{
					part.IsNew = false;
				}
			}

			if (ks.IsKeyDown(Keys.P))
			{
				for (int i = 0; i < parts.Count; i++)
				{
					Seperator sep = parts[i] as Seperator;
					if (sep != null)
					{
						sep.Action(this);
					}
				}
			}

			if (ks.IsKeyDown(Keys.Space))
			{
				rcsFire(mouseDir);
			}

			if (ks.IsKeyDown(Keys.LeftControl))
			{
				rcsFire(linearV.SafeNormalize());
			}
			if (ks.IsKeyDown(Keys.LeftAlt))
			{
				rcsFire(-linearV.SafeNormalize());
			}

			fireSystem = ms.LeftButton == ButtonState.Pressed;

			if (ks.IsKeyDown(Keys.W) || GamePad.GetState(0).Buttons.A == ButtonState.Pressed)
			{
				thrusterOn = true;
				Vector2 totalThrust = Vector2.Zero;
				float totalTorque = 0.0f;
				foreach (IPart part in parts)
				{
					Thruster thruster = part as Thruster;
					if (thruster == null) continue;
					Vector2 thusterForce = thruster.GetForce(Throttle);
					totalThrust += thusterForce;
					if (thusterForce.Length() > 0.0f)
						totalTorque += CrossProduct(thruster.LocalCOMPosition(), thruster.thrust * new Vector2(1, 0));
				}

				totalThrust *= 1.0f / mass;
				totalTorque *= 1.0f / mass * 0.001f;

				linearV += totalThrust;
				angularV += totalTorque;
			}

			oldKS = ks;
		}


		public void NetUpdate()
		{
			PositionRotationMatrix = Matrix.CreateTranslation(new Vector3(-CentreOfMass.xna, 0.0f)) * Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(new Vector3(CentreOfMass.xna, 0.0f));
		}

		int leap = 0;
		public void Update(float dt, List<Planet> planets)
		{
			//if (gravPlanet != null)
			//	position += gravPlanet.Velocity;

			if (NaNCheck(linearV.X) || NaNCheck(angularV))
				Debug.WriteLine("ERROR");

			if (!inEditor)
			{
				Vector2 timeV = linearV * dt;
				position += new Vector2d(timeV.X, timeV.Y);
				rotation += angularV;
			}

			CalculateMass();
			CalculateBounds();
			CalculateCenterOfMass();

			PositionRotationMatrix = Matrix.CreateTranslation(new Vector3(-CentreOfMass.xna, 0.0f)) * Matrix.CreateRotationZ(rotation) * Matrix.CreateTranslation(new Vector3(CentreOfMass.xna, 0.0f));

			//Collision
			foreach (Ship otherShip in Game1.GetShips)
			{
				if (this == otherShip)
					continue;

				CollideWithBody(otherShip);
			}

			maxGrav = Vector2d.Zero;
			AirDrag = 0.0f;
			DeltaTime = dt;

			//Calculate GravPlanet
			foreach (Planet planet in planets)
			{
				double comX = CentreOfMass.X;
				double comY = CentreOfMass.Y;
				double pX = planet.position.X;
				double pY = planet.position.Y;

				double difX = comX - pX;
				double difY = comY - pY;

				double dist = Math.Sqrt(difX * difX + difY * difY);
				double force = (1.0 / (dist * dist)) * GravConstant * (4.0 / 3.0 * Math.PI * Math.Pow(planet.r, 3));

				Vector2d gravVec = (float)force * Vector2d.Normalize(planet.position - CentreOfMass);
				if (gravVec.Length > maxGrav.Length)
				{
					maxGrav = gravVec;
					gravPlanet = planet;
				}
			}

			//Woops
			if (gravPlanet == null)
				return;

			if (!inEditor)
			{
				CollidePlanet(gravPlanet, true);
				onPlanet = Vector2d.Distance(CentreOfMass, gravPlanet.position) < gravPlanet.r;

				//Air Drag
				if (gravPlanet.hasAtmo)
				{
					linearV -= calculateDragAcceleration(gravPlanet);
					if (linearV.Length() > 0.01f)
					{
						linearV -= (Vector2.Normalize(linearV) * 0.002f * AirFraction);
					}

					if (AirDragAngular > 0.0f)
					{
						if (angularV > 0.0f)
						{
							angularV -= angularV * angularV * AirDragAngular;
							if (angularV < 0.0f)
								angularV = 0.0f;
						}
						if (angularV < 0.0f)
						{
							angularV += angularV * angularV * AirDragAngular;
							if (angularV > 0.0f)
								angularV = 0.0f;
						}
					}
				}

				//Apply gravity to velocity, max radius grav versus forward on planet to simulate take off/landing and normal COM based gravity offplanet
				if (onPlanet)
				{
					//Gravity Deaccel
					if (linearV.Length() > 0.0f)
					{
						double force = (1.0 / (gravPlanet.r * gravPlanet.r)) * GravConstant * (4.0 / 3.0 * Math.PI * Math.Pow(gravPlanet.r, 3));
						Vector2 gravDecel = (float)force * Vector2.Normalize(-linearV);
						linearV += gravDecel;
					}
				}
				else
				{
					linearV += maxGrav.xna;
				}

				//TODO: Float calculate Ship MINMAX
				float diameter = (float)Math.Sqrt(Bounds.Width * Bounds.Width + Bounds.Height * Bounds.Height);

				Vector2 Vs = linearV * dt;// * 60.0f;
				float Gs = (float)GravConstant * dt;// * 60.0f;

				//Calculate oribtal elements
				float gravPlanetMass = (float)(4.0 / 3.0 * Math.PI * Math.Pow(gravPlanet.r, 3));
				Vector2d R = gravPlanet.position - CentreOfMass;
				float r = (float)R.Length;


				OrbitalElement = CartesianToOrbitalElements(Gs, mass, gravPlanetMass, R.xna, Vs);
				float e = OrbitalElement.Eccentricity;
				float a = OrbitalElement.SemiMajorAxis;
				float Mu = (float)Gs * (mass + gravPlanetMass);

				SemiMajor = a;
				Eccentricity = e;

				//Calc vis-viva velocity
				VisViva = (float)Math.Sqrt(gravPlanetMass * (2.0f / r - 1.0f / (float)a));

				SpecificEnergy = -Mu / (2.0f * SemiMajor);

				//position += linearV * dt;
				//rotation += angularV;
			}
			else
			{
				crossSection = CalculateCrossSection();
			}

			eChargeNew = 0.0f;
			eChargeMax = 0.0f;

			mass = CalculateMass();

			//Set checked to false for all hull parts
			if (isDirty)
			{
				Hull rootHull = null;
				foreach (IPart part in Parts)
				{
					Hull other = part as Hull;
					if (other != null)
					{
						other.isChecked = false;
						if (other.IsRoot)
							rootHull = other;
					}
				}
				Hull.FindRoot(rootHull, this);
				isDirty = false;
			}

			//Update all parts
			for (int i = 0; i < parts.Count; i++)
			{
				parts[i].Update(this);
			}

			eCharge += eChargeNew;
			eCharge = MathHelper.Clamp(eCharge, 0.0f, eChargeMax);

			//voidCargo = 0.0f;

			if (Math.Abs(rotation) > Math.PI * 2.0)
				rotation = 0;

			Forward.X = (float)Math.Cos(rotation);
			Forward.Y = (float)Math.Sin(rotation);

			//Break off ship if any hull is not checked
			if (!inEditor)
			{
				foreach (IPart part in Parts)
				{
					Hull other = part as Hull;
					if (other != null)
					{
						if (other.isChecked == false)
						{
							other.BreakoffShip(this);
							break;
						}
					}
				}
			}

			leap++;
		}


		public void CalculateRootPosition()
		{
			Vector2d p = Vector2d.Zero;
			foreach (ShipPart part in parts)
			{
				Hull h = part as Hull;
				if (h != null && h.IsRoot)
					p = h.WorldPosition() - new Vector2d(16, 16);
			}

			RootPosition = p;
		}

		public byte[] Package()
		{
			MemoryStream stream = new MemoryStream();
			Serializer.Serialize<Ship>(stream, this);

			return stream.ToArray();
		}

		public static Ship Unpack(byte[] package)
		{
			if (package == null || package.Length == 0)
				return null;

			MemoryStream stream = new MemoryStream(package);
			stream.Flush();
			Ship ship = Serializer.Deserialize<Ship>(stream);

			ship.position.X = ship.posX;
			ship.position.Y = ship.posY;
			ship.CentreOfMass.X = ship.comX;
			ship.CentreOfMass.Y = ship.comY;

			foreach (ShipPart shipPart in ship.parts)
			{
				shipPart.rotation = ship.rotation;
			}

			return ship;
		}


		public void Draw(SpriteBatch batch, float deltaTime, bool activeShip)
		{
			foreach (ShipPart part in parts)
			{
				if (part as Hull != null)
				{
					part.Draw(batch);
				}
			}

			foreach (ShipPart part in parts)
			{
				if (part as Hull == null)
				{
					part.Draw(batch);
				}
			}

			if (activeShip)
			{
				dtBalance = 0.06f; //Magic ?
				Color onPlanetC = new Color(1.0f, 1.0f, 1.0f, 0.5f);
				if (onPlanet)
					onPlanetC = new Color(0.0f, 1.0f, 0.0f, 0.5f);

				if (mass > 0.0f)
					batch.Draw(comTexture, CentreOfMass.xna, null, onPlanetC, 0.0f, new Vector2(16, 16), 0.5f, SpriteEffects.None, 0.0f);

				//batch.Draw(comTexture, new Vector2(Bounds.X, Bounds.Y), null, Color.White, 0.0f, new Vector2(16, 16), 0.5f, SpriteEffects.None, 0.0f);
				//batch.Draw(comTexture, new Vector2(Bounds.X + Bounds.Width, Bounds.Y), null, Color.White, 0.0f, new Vector2(16, 16), 0.5f, SpriteEffects.None, 0.0f);
				//batch.Draw(comTexture, new Vector2(Bounds.X, Bounds.Y + Bounds.Height), null, Color.White, 0.0f, new Vector2(16, 16), 0.5f, SpriteEffects.None, 0.0f);
				//batch.Draw(comTexture, new Vector2(Bounds.X + Bounds.Width, Bounds.Y + Bounds.Height), null, Color.White, 0.0f, new Vector2(16, 16), 0.5f, SpriteEffects.None, 0.0f);

				if (OrbitalElement != null)
				{
					float a = OrbitalElement.SemiMajorAxis;
					float b = a * (float)Math.Sqrt(1.0 - Math.Pow(OrbitalElement.Eccentricity, 2.0));
				}
				//Prediction Trajectory
				if (linearV.Length() > 0.001f && !onPlanet)
				{
					Vector2d iP = CentreOfMass;
					Vector2 iV = linearV;

					bool hitPlanet = false;
					for (int i = 0; i < 50000; i += 1)
					{
						if (hitPlanet)
							break;
						iP += new Vector2d(iV.X, iV.Y);
						double dist = Vector2d.Distance(iP, gravPlanet.position);
						if (dist < gravPlanet.r)
						{
							hitPlanet = true;
							break;
						}
						double force = (1.0 / (dist * dist)) * GravConstant * (4.0 / 3.0 * Math.PI * Math.Pow(gravPlanet.r, 3));

						Vector2d gravVec = Vector2d.Normalize(gravPlanet.position - iP) * force;

						iV += (gravVec * dtBalance).xna;
						if (gravPlanet.hasAtmo)
							iV -= calculateDragAcceleration(gravPlanet, iV, iP, crossSection) * dtBalance;

						if (i % 256 == 0)
							batch.Draw(dotTexture, iP.xna, null, new Color(1.0f, 1.0f, 1.0f, 0.4f), 0.0f, new Vector2(5, 5), 1.5f / (Game1.zoom * 0.1f) * 0.02f, SpriteEffects.None, 0.0f);
					}

					/*Vector2 pos = Vector2.Zero;
					float px = (float)Math.Cos(OrbitalElement.TrueLongitude);
					float py = (float)Math.Sin(OrbitalElement.TrueLongitude);

					Vector2 pos0 = new Vector2(-OrbitalElement.Periapsis, 0);
					Vector2 pos1 = new Vector2(OrbitalElement.Apoapsis, 0);
					//pos0 = pos0.Rotate(OrbitalElement.TrueLongitude);
					//pos1 = pos1.Rotate(OrbitalElement.TrueLongitude);

					Vector2 pInc = Vector2.Normalize(new Vector2(px, py));
					for (int i = 0; i < (int)SemiMajor * 2.0; i++)
					{
						pos = Vector2.Lerp(pos0, pos1, i / (SemiMajor * 2.0f));
						batch.Draw(dotTexture, pos, null, new Color(1.0f, 1.0f, 1.0f, 0.4f), 0.0f, new Vector2(5, 5), 1.5f / (Game1.zoom * 0.1f) * 0.02f, SpriteEffects.None, 0.0f);
					}*/
				}

#if DEBUG
				Gizmos.DebugRays.Clear();
				Vector2 fwd = Vector2.Normalize(linearV);
				foreach (ShipPart aeroSurface in parts.OfType<Hull>())
				{
					float force = ((AirDragAngular * 128.0f) + AirDrag) * 0.2f;
					float partTorque = getAeroTorqueForPart(aeroSurface);
					Vector2 endTorque = aeroSurface.worldPosition.xna - fwd * partTorque * force;

					Gizmos.DebugRays.Add(new GizmoRay
					{
						A = Vector2.Transform(aeroSurface.worldPosition.xna, Game1.cameraTransform),
						B = Vector2.Transform(endTorque, Game1.cameraTransform)
					});
				}
#endif
			}
		}


		VertexPositionColor[] Path = new VertexPositionColor[64];

		public void CalculateEllipsoid()
		{
			Vector2 _limits = new Vector2(-180, 180); //[degrees]
			if (OrbitalElement == null)
				return;
			var semiLatusRectum = Kepler.ComputeSemiLatusRectum(OrbitalElement.Periapsis, OrbitalElement.Eccentricity);
			var orientation = Kepler.ComputeOrientation(0f, 0f, OrbitalElement.ArgumentOfPeriapsis);
			//var semiMajorAxis = Kepler.ComputeSemiMajorAxis(_periapsis, _eccentricity);
			//var rate = Kepler.ComputeRate(_period, _limits.x * Deg2Rad, _limits.y * Deg2Rad);

			//variables required
			Vector3[] path = new Vector3[51];
			//Vector3 periapsisV = Vector3.Transform(Vector3.Right, orientation) * OrbitalElement.Periapsis;
			//Vector3 positionV = orientation * Kepler.ComputePosition(Kepler.ComputeRadius(semiLatusRectum, OrbitalElement.Eccentricity, trueAnomaly), trueAnomaly);

			//elliptical and circular
			//build list of vectors for path
			double step, lower, upper;
			double r;

			lower = Kepler.ComputeTrueAnomaly(Kepler.ComputeEccentricAnomaly(_limits.X * DEG2RAD, OrbitalElement.Eccentricity), OrbitalElement.Eccentricity);
			upper = Kepler.ComputeTrueAnomaly(Kepler.ComputeEccentricAnomaly(_limits.Y * DEG2RAD, OrbitalElement.Eccentricity), OrbitalElement.Eccentricity);
			step = (upper - lower) / 50;

			for (int i = 0; i <= 50; i++)
			{
				r = Kepler.ComputeRadius(semiLatusRectum, OrbitalElement.Eccentricity, lower + i * step);
				path[i] = Kepler.ComputePosition(r, lower + i * step);


				//path[i] -= new Vector3(gravPlanet.position.X, 0.0f, gravPlanet.position.Y);
				path[i] = Vector3.Transform(path[i], orientation);
				//path[i] += new Vector3(gravPlanet.position.X, 0.0f, gravPlanet.position.Y);
				path[i].X = -path[i].X;
				path[i].Y = path[i].Z;
				path[i].Z = 0;


				//path[i] = path[i] * Game1.zoom;
				path[i] = Vector3.Transform(path[i], Game1.cameraTransform);
			}

			Path = new VertexPositionColor[64];
			List<VertexPositionColor> Vertices = new List<VertexPositionColor>();

			for (int i = 0; i < 50; i++)
			{
				Vertices.Add(new VertexPositionColor(path[i], Color.Red));
			}
			Vertices.Add(new VertexPositionColor(path[0], Color.Red));

			Path = Vertices.ToArray();
		}

		public void DrawEllipsoid()
		{
			if (Path == null)
				return;
			//BasicEffect effect = new BasicEffect(GraphDevice);
			//effect.World = Matrix.CreateTranslation(new Vector3(0,0,0));
			//effect.LightingEnabled = false;
			//effect.TextureEnabled = false;
			//effect.VertexColorEnabled = true;
			//effect.CurrentTechnique.Passes[0].Apply();
			Game1.fx.CurrentTechnique.Passes[0].Apply();
			Game1.graphics.GraphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, Path, 0, Path.Length - 1);
		}

		public class OrbitalElements
		{
			public float SemiMajorAxis;
			public float SemiMinorAxis;
			public float Eccentricity;
			public float EccentricAnomaly;
			public float MeanAnomaly;
			public float TrueLongitude;
			public float Apoapsis;
			public float Periapsis;
			public float Period;
			public float ArgumentOfPeriapsis;
			public float Inclination;

			public override string ToString()
			{
				return $"Eccentricity '{Eccentricity}'\tMean Anomaly '{MeanAnomaly}'\tPeriod '{Period}'\tArgument '{ArgumentOfPeriapsis}'\tInclination '{Inclination}'";
			}
		}

		//TAKEN FROM http://orbitsimulator.com/formulas/OrbitalElements.html CONVERTED TO C#
		public static OrbitalElements CartesianToOrbitalElements(float G, float m0, float m1, Vector2 position, Vector2 velocity)
		{
			float Mu = (float)G * (m0 + m1);

			//Prepare for R and V <><><> rest is original conversion from http://orbitsimulator.com/formulas/OrbitalElements.html
			float Rx = position.X;
			float Ry = position.Y;
			float Rz = float.Epsilon;
			float Vx = velocity.X;
			float Vy = velocity.Y;
			float Vz = float.Epsilon;
			//Prepare for R and V <><><> rest is original conversion from http://orbitsimulator.com/formulas/OrbitalElements.html

			float R = (float)Math.Sqrt(Rx * Rx + Ry * Ry + Rz * Rz);
			float V = (float)Math.Sqrt(Vx * Vx + Vy * Vy + Vz * Vz);
			float a = 1.0f / (2.0f / R - V * V / Mu); //  semi-major axis

			float Hx = Ry * Vz - Rz * Vy;
			float Hy = Rz * Vx - Rx * Vz;
			float Hz = Rx * Vy - Ry * Vx;
			float H = (float)Math.Sqrt(Hx * Hx + Hy * Hy + Hz * Hz);

			float p = H * H / Mu;
			float q = Rx * Vx + Ry * Vy + Rz * Vz;  // dot product of r*v

			float E = (float)Math.Sqrt(1.0f - p / a);  // eccentricity

			float Ex = 1.0f - R / a;
			float Ey = q / (float)Math.Sqrt(a * Mu);

			float i = (float)Math.Acos(Hz / H);
			float lan = 0;
			if (i != 0) { lan = (float)Math.Atan2(Hx, -Hy); }

			float TAx = H * H / (R * Mu) - 1.0f;
			float TAy = H * q / (R * Mu);
			float TA = (float)Math.Atan2(TAy, TAx);
			float Cw = (Rx * (float)Math.Cos(lan) + Ry * (float)Math.Sin(lan)) / R;

			float Sw = 0;
			if (i == 0 || i == (float)Math.PI)
			{ Sw = (Ry * (float)Math.Cos(lan) - Rx * (float)Math.Sin(lan)) / R; }
			else
			{ Sw = Rz / (R * (float)Math.Sin(i)); }

			float W = (float)Math.Atan2(Sw, Cw) - TA;
			if (W < 0) { W = 2.0f * (float)Math.PI + W; }

			float u = (float)Math.Atan2(Ey, Ex); // eccentric anomoly
			float M = u - E * (float)Math.Sin(u); // Mean anomoly
			float TL = W + TA + lan; // True longitude

			while (TL >= 2.0f * (float)Math.PI) { TL = TL - 2.0f * (float)Math.PI; }

			float PlusMinus = a * E;
			float Periapsis = a - PlusMinus;
			float Apoapsis = a + PlusMinus;
			float Period = 2.0f * (float)Math.PI * (float)Math.Sqrt((a * a * a / ((float)G * (m0 + m1))));

			//Semi minor
			float b = a * (float)Math.Sqrt(1.0 - Math.Pow(E, 2.0));

			return new OrbitalElements
			{
				Apoapsis = Apoapsis,
				EccentricAnomaly = u,
				Eccentricity = E,
				MeanAnomaly = M,
				Periapsis = Periapsis,
				Period = Period,
				SemiMajorAxis = a,
				SemiMinorAxis = b,
				TrueLongitude = TL,
				ArgumentOfPeriapsis = W,
				Inclination = i
			};
		}
	}
}
