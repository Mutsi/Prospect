﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
	[Serializable()]
	public struct Vector2d
	{
		[NonSerialized()]
		public const double kEpsilon = 1E-05d;
		public double X;
		public double Y;

		public double this[int index]
		{
			get
			{
				switch (index)
				{
					case 0:
						return this.X;
					case 1:
						return this.Y;
					default:
						throw new IndexOutOfRangeException("Invalid Vector2d index!");
				}
			}
			set
			{
				switch (index)
				{
					case 0:
						this.X = value;
						break;
					case 1:
						this.Y = value;
						break;
					default:
						throw new IndexOutOfRangeException("Invalid Vector2d index!");
				}
			}
		}

		public Vector2 xna { get { return new Vector2((float)X, (float)Y); } }

		public Vector2d normalized
		{
			get
			{
				Vector2d vector2d = new Vector2d(this.X, this.Y);
				vector2d.Normalize();
				return vector2d;
			}
		}

		public double Length
		{
			get
			{
				return Math.Sqrt(this.X * this.X + this.Y * this.Y);
			}
		}

		public double sqrMagnitude
		{
			get
			{
				return this.X * this.X + this.Y * this.Y;
			}
		}

		public static Vector2d Zero
		{
			get
			{
				return new Vector2d(0.0d, 0.0d);
			}
		}

		public static Vector2d one
		{
			get
			{
				return new Vector2d(1d, 1d);
			}
		}

		public static Vector2d up
		{
			get
			{
				return new Vector2d(0.0d, 1d);
			}
		}

		public static Vector2d right
		{
			get
			{
				return new Vector2d(1d, 0.0d);
			}
		}

		public Vector2d(double x, double y)
		{
			this.X = x;
			this.Y = y;
		}

		/*public static implicit operator Vector2d(Vector3d v)
		{
			throw new NotImplementedException();
			//return new Vector2d(v.x, v.y);
		}

		public static implicit operator Vector3d(Vector2d v)
		{
			throw new NotImplementedException();
			//return new Vector3d(v.x, v.y, 0.0d);
		}*/

		public static Vector2d operator +(Vector2d a, Vector2d b)
		{
			return new Vector2d(a.X + b.X, a.Y + b.Y);
		}

		public static Vector2d operator -(Vector2d a, Vector2d b)
		{
			return new Vector2d(a.X - b.X, a.Y - b.Y);
		}

		public static Vector2d operator -(Vector2d a)
		{
			return new Vector2d(-a.X, -a.Y);
		}

		public static Vector2d operator *(Vector2d a, double d)
		{
			return new Vector2d(a.X * d, a.Y * d);
		}

		public static Vector2d operator *(float d, Vector2d a)
		{
			return new Vector2d(a.X * d, a.Y * d);
		}

		public static Vector2d operator /(Vector2d a, double d)
		{
			return new Vector2d(a.X / d, a.Y / d);
		}

		public static bool operator ==(Vector2d lhs, Vector2d rhs)
		{
			return Vector2d.SqrMagnitude(lhs - rhs) < 0.0 / 1.0;
		}

		public static bool operator !=(Vector2d lhs, Vector2d rhs)
		{
			return (double)Vector2d.SqrMagnitude(lhs - rhs) >= 0.0 / 1.0;
		}

		public void Set(double new_x, double new_y)
		{
			this.X = new_x;
			this.Y = new_y;
		}

		public static Vector2d Lerp(Vector2d from, Vector2d to, double t)
		{
			throw new NotImplementedException();
			//t = Math.Clamp01(t);
			//return new Vector2d(from.x + (to.x - from.x) * t, from.y + (to.y - from.y) * t);
		}

		public static Vector2d MoveTowards(Vector2d current, Vector2d target, double maxDistanceDelta)
		{
			Vector2d vector2 = target - current;
			double magnitude = vector2.Length;
			if (magnitude <= maxDistanceDelta || magnitude == 0.0d)
				return target;
			else
				return current + vector2 / magnitude * maxDistanceDelta;
		}

		public static Vector2d Scale(Vector2d a, Vector2d b)
		{
			return new Vector2d(a.X * b.X, a.Y * b.Y);
		}

		public void Scale(Vector2d scale)
		{
			this.X *= scale.X;
			this.Y *= scale.Y;
		}

		public void Normalize()
		{
			double magnitude = this.Length;
			if (magnitude > 9.99999974737875E-06)
				this = this / magnitude;
			else
				this = Vector2d.Zero;
		}

		public override string ToString()
		{
			/*
      string fmt = "({0:D1}, {1:D1})";
      object[] objArray = new object[2];
      int index1 = 0;
      // ISSUE: variable of a boxed type
      __Boxed<double> local1 = (ValueType) this.x;
      objArray[index1] = (object) local1;
      int index2 = 1;
      // ISSUE: variable of a boxed type
      __Boxed<double> local2 = (ValueType) this.y;
      objArray[index2] = (object) local2;
      */
			return "not implemented";
		}

		public string ToString(string format)
		{
			/* TODO:
      string fmt = "({0}, {1})";
      object[] objArray = new object[2];
      int index1 = 0;
      string str1 = this.x.ToString(format);
      objArray[index1] = (object) str1;
      int index2 = 1;
      string str2 = this.y.ToString(format);
      objArray[index2] = (object) str2;
      */
			return "not implemented";
		}

		public override int GetHashCode()
		{
			return this.X.GetHashCode() ^ this.Y.GetHashCode() << 2;
		}

		public override bool Equals(object other)
		{
			if (!(other is Vector2d))
				return false;
			Vector2d vector2d = (Vector2d)other;
			if (this.X.Equals(vector2d.X))
				return this.Y.Equals(vector2d.Y);
			else
				return false;
		}

		public static double Dot(Vector2d lhs, Vector2d rhs)
		{
			return lhs.X * rhs.X + lhs.Y * rhs.Y;
		}

		public static double Angle(Vector2d from, Vector2d to)
		{
			throw new NotImplementedException();
			//return Math.Acos(Math.Clamp(Vector2d.Dot(from.normalized, to.normalized), -1d, 1d)) * 57.29578d;
		}

		public static double Distance(Vector2d a, Vector2d b)
		{
			return (a - b).Length;
		}

		public static Vector2d ClampMagnitude(Vector2d vector, double maxLength)
		{
			if (vector.sqrMagnitude > maxLength * maxLength)
				return vector.normalized * maxLength;
			else
				return vector;
		}

		public static double SqrMagnitude(Vector2d a)
		{
			return (a.X * a.X + a.Y * a.Y);
		}

		public double SqrMagnitude()
		{
			return (this.X * this.X + this.Y * this.Y);
		}

		public static Vector2d Min(Vector2d lhs, Vector2d rhs)
		{
			return new Vector2d(Math.Min(lhs.X, rhs.X), Math.Min(lhs.Y, rhs.Y));
		}

		public static Vector2d Max(Vector2d lhs, Vector2d rhs)
		{
			return new Vector2d(Math.Max(lhs.X, rhs.X), Math.Max(lhs.Y, rhs.Y));
		}
	}
}
