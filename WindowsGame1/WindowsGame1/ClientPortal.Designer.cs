﻿namespace WindowsGame1
{
	partial class ClientPortal
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.versionLabel = new System.Windows.Forms.Label();
			this.startButton = new System.Windows.Forms.Button();
			this.startServerButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// versionLabel
			// 
			this.versionLabel.AutoSize = true;
			this.versionLabel.Location = new System.Drawing.Point(37, 21);
			this.versionLabel.Name = "versionLabel";
			this.versionLabel.Size = new System.Drawing.Size(41, 13);
			this.versionLabel.TabIndex = 0;
			this.versionLabel.Text = "version";
			this.versionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(23, 47);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(75, 23);
			this.startButton.TabIndex = 1;
			this.startButton.Text = "Start Client";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// startServerButton
			// 
			this.startServerButton.Location = new System.Drawing.Point(135, 47);
			this.startServerButton.Name = "startServerButton";
			this.startServerButton.Size = new System.Drawing.Size(75, 23);
			this.startServerButton.TabIndex = 2;
			this.startServerButton.Text = "StartServer";
			this.startServerButton.UseVisualStyleBackColor = true;
			this.startServerButton.Click += new System.EventHandler(this.startServerButton_Click);
			// 
			// ClientPortal
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(232, 82);
			this.Controls.Add(this.startServerButton);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.versionLabel);
			this.Name = "ClientPortal";
			this.Text = "ClientPortal";
			this.Load += new System.EventHandler(this.ClientPortal_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label versionLabel;
		private System.Windows.Forms.Button startButton;
		private System.Windows.Forms.Button startServerButton;
	}
}