﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Storage;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


namespace WindowsGame1
{
	class ShipBuilderHUD
	{
		private int selectedID = 0;
		List<Texture2D> icons = new List<Texture2D>();
		MouseState prevMs;

		Texture2D fuelEnd;

		public ShipPart selectedPart;

		OldTextBox saveText;
		TextList loadList;
		OldButton saveBtn;
		OldButton loadBtn;
		OldButton fuelBtn;

		public void LoadContent()
		{
			icons.Add(ResourceManager.Load<Texture2D>("Hull"));
			icons.Add(ResourceManager.Load<Texture2D>("Engine"));
			icons.Add(ResourceManager.Load<Texture2D>("Thruster"));
			icons.Add(ResourceManager.Load<Texture2D>("Solarpanel"));
			icons.Add(ResourceManager.Load<Texture2D>("Battery"));
			icons.Add(ResourceManager.Load<Texture2D>("FuelTank"));
			icons.Add(ResourceManager.Load<Texture2D>("FuelLine"));
			icons.Add(ResourceManager.Load<Texture2D>("Seperator"));
			icons.Add(ResourceManager.Load<Texture2D>("Laser"));
			icons.Add(ResourceManager.Load<Texture2D>("TractorBeam"));
			icons.Add(ResourceManager.Load<Texture2D>("TractorBeamA"));
			icons.Add(ResourceManager.Load<Texture2D>("Cargobay"));
			icons.Add(ResourceManager.Load<Texture2D>("Falcon"));
			icons.Add(ResourceManager.Load<Texture2D>("TractorBeam"));

			saveText = new OldTextBox(new Rectangle(600, 400, 300, 64));
			loadList = new TextList(new Rectangle(200, 200, 300, 0));

			saveBtn = new OldButton(new Rectangle(800, 0, 0, 0), "Save");
			loadBtn = new OldButton(new Rectangle(1200, 0, 0, 0), "Load");
			fuelBtn = new OldButton(new Rectangle(1200, 900, 0, 0), "Refuel: ");

			saveText.IsActive = false;
			loadList.IsActive = false;

			Game1.GUI.items.Add(saveText);
			Game1.GUI.items.Add(loadList);
			Game1.GUI.items.Add(saveBtn);
			Game1.GUI.items.Add(loadBtn);
			Game1.GUI.items.Add(fuelBtn);
			Game1.GUI.focus = saveText;

			fuelEnd = ResourceManager.Load<Texture2D>("FuelEnd");
		}

		private bool fuelLinking = false;
		private FuelTank linkFuelTank0, linkFuelTank1;
		private Thruster linkThruster0;
		private void FuelLink(MouseState ms, Ship ship, Vector2 msWorld)
		{
			if (ms.LeftButton == ButtonState.Pressed && prevMs.LeftButton == ButtonState.Released)
			{
				foreach (ShipPart part in ship.Parts)
				{
					Thruster thruster = part as Thruster;
					if (thruster == null)
						continue;
					Vector2 wPos = new Vector2(part.localX * 32, part.localY * 32) + ship.position;
					Rectangle partRect = new Rectangle((int)wPos.X - 16, (int)wPos.Y - 16, 32, 32);
					if (partRect.Contains((int)msWorld.X, (int)msWorld.Y))
					{
						if (linkFuelTank0 != null)
							linkThruster0 = thruster;
						break;
					}
				}
				foreach (ShipPart part in ship.Parts)
				{
					FuelTank tank = part as FuelTank;
					if (tank == null)
						continue;
					Vector2 wPos = new Vector2(part.localX * 32, part.localY * 32) + ship.position;
					Rectangle partRect = new Rectangle((int)wPos.X - 16, (int)wPos.Y - 16, 32, 32);
					if (partRect.Contains((int)msWorld.X, (int)msWorld.Y))
					{
						if (linkFuelTank0 == null)
							linkFuelTank0 = tank;
						else
							linkFuelTank1 = tank;
						break;
					}
				}
			}

			if (linkFuelTank0 != null && linkThruster0 != null)
			{
				linkThruster0.fuelTank = linkFuelTank0;

				linkFuelTank0 = null;
				linkFuelTank1 = null;
				linkThruster0 = null;
			}

			if (linkFuelTank0 != null && linkFuelTank1 != null)
			{
				linkFuelTank0.fuelTank = linkFuelTank1;
				linkFuelTank1.children++;

				linkFuelTank0 = null;
				linkFuelTank1 = null;
				linkThruster0 = null;
			}

			prevMs = ms;
		}

		private void SaveShip(Ship ship, string name)
		{
			if (!Directory.Exists("Saves"))
				Directory.CreateDirectory("Saves");

			string filename = "Saves/" + name + ".sav";

			// Check to see whether the save exists.
			if (File.Exists(filename))
				// Delete it so that we can create one fresh.
				File.Delete(filename);

			Stream TestFileStream = File.Create(filename);
			BinaryFormatter serializer = new BinaryFormatter();
			serializer.Serialize(TestFileStream, ship);
			TestFileStream.Close();

			TestFileStream.Close();
		}

		private void LoadShip(Ship ship, string name)
		{
			if (!Directory.Exists("Saves"))
				Directory.CreateDirectory("Saves");

			Game1.GetShips.Remove(ship);

			Ship loadShip;

			Stream TestFileStream = File.OpenRead(name);
			BinaryFormatter deserializer = new BinaryFormatter();
			loadShip = (Ship)deserializer.Deserialize(TestFileStream);
			TestFileStream.Close();

			loadShip.LoadContent(true);

			Planet home = Game1.planets.OrderBy(o => o.r).First();
			loadShip.position = home.position;
			loadShip.RootPosition = home.position;
			loadShip.gravPlanet = home;

			loadShip.CalculateCenterOfMass();
			Game1.GetShips.Add(loadShip);

		}

		public void Update(MouseState ms, KeyboardState ks, Ship ship, Vector2 msWorld)
		{
			ship.linearV = Vector2.Zero;
			ship.angularV = 0.0f;

			Vector2 mouseShipSpace = Physics.SnapCalculate(msWorld - ship.position);
			int mouseShipX = (int)(mouseShipSpace.X / 32.0f);
			int mouseShipY = (int)(mouseShipSpace.Y / 32.0f);

			float canBuy = 0.0f;
			foreach (IPart part in ship.Parts)
			{
				FuelTank tank = part as FuelTank;
				if (tank != null)
					canBuy += tank.capacity - tank.fuel;
			}
			fuelBtn.SetText("Refuel: " + canBuy);

			if (fuelBtn.Update(ms) && Game1.Cash >= canBuy)
			{
				Game1.Cash -= canBuy;
				foreach (IPart part in ship.Parts)
				{
					FuelTank tank = part as FuelTank;
					if (tank != null)
						tank.fuel = tank.capacity;
				}
			}

			if (saveBtn.Update(ms))
			{
				if (saveText.IsActive)
				{
					SaveShip(ship, saveText.text);
					saveText.IsActive = false;
				}
				else
				{
					saveText.IsActive = true;
				}
			}

			loadList.Update(ms);
			if (loadBtn.Update(ms))
			{
				if (!Directory.Exists("Saves"))
					Directory.CreateDirectory("Saves");
				loadList.files = System.IO.Directory.GetFiles("Saves/", "*.sav");

				if (loadList.IsActive)
				{
					LoadShip(ship, loadList.files[loadList.selected]);
					loadList.IsActive = false;
				}
				else
				{
					loadList.IsActive = true;
				}
			}

			if (selectedPart != null)
			{
				Vector2 snap = ship.position - Physics.SnapCalculate(ship.position);
				Vector2 offset = Vector2.Zero;
				Vector2 pos = Physics.SnapCalculate(msWorld - ship.position);
				selectedPart.SetLocal(mouseShipX, mouseShipY);
			}

			if (ms.LeftButton == ButtonState.Pressed && (prevMs.LeftButton == ButtonState.Released))
			{
				for (int i = 0; i < icons.Count; i++)
				{
					Rectangle iconRect = new Rectangle(0, i * 32, 32, 32);
					if (iconRect.Contains(ms.X, ms.Y))
					{
						if (selectedPart != null)
						{
							Game1.Cash += selectedPart.Cost;
							ship.RemovePart(selectedPart);
						}

						fuelLinking = false;
						if (i == 6)
						{
							fuelLinking = true;
							selectedID = -1;
							return;
						}
						else if (Game1.Cash > 0)
						{
							selectedID = i;
							return;
						}
					}
				}
			}

			if (ms.LeftButton == ButtonState.Pressed && (prevMs.LeftButton == ButtonState.Released || ks.IsKeyDown(Keys.LeftShift)))
			{
				bool partSelected = false;

				if (fuelLinking)
				{
					FuelLink(ms, ship, msWorld);
				}
				else if (!partSelected)
				{
					bool partPicked = false;

					//Pick up part
					/*if (selectedPart == null)
					{
						foreach (ShipPart part in ship.Parts)
						{
							Vector2 wPos = new Vector2(part.localX * 32, part.localY * 32) + ship.position;
							Rectangle partRect = new Rectangle((int)wPos.X - 16, (int)wPos.Y - 16, 32, 32);
							if (partRect.Contains((int)msWorld.X, (int)msWorld.Y))
							{
								selectedPart = part;
								part.parent = null;
								partPicked = true;
							}
						}
					}*/

					//Create part
					ShipPart newPart = GetNewPart(mouseShipX, mouseShipY, ship, selectedID);
					if (!partPicked && selectedID > -1 && newPart.IsValid(mouseShipX, mouseShipY, ship))
					{
						Buypart(newPart, ship);
						if (ship.parts.Count == 1)
						{
							ship.position = msWorld;
						}
					}

				}

			}

			if (ms.RightButton == ButtonState.Pressed && prevMs.RightButton == ButtonState.Released)
			{
				fuelLinking = false;
				if (linkFuelTank0 != null)
				{
					linkFuelTank0.fuelTank = null;
				}
				if (linkFuelTank1 != null)
				{
					linkFuelTank1.fuelTank = null;
				}
				linkFuelTank0 = null;
				linkFuelTank1 = null;
				linkThruster0 = null;

				if (selectedID > -1)
				{
					selectedID = -1;
				}
				else
				{
					for (int i = ship.Parts.Count - 1; i >= 0; i--)
					{
						Vector2 wPos = new Vector2(ship.parts[i].localX * 32, ship.parts[i].localY * 32) + ship.position;
						Rectangle partRect = new Rectangle((int)wPos.X - 16, (int)wPos.Y - 16, 32, 32);
						if (partRect.Contains((int)msWorld.X, (int)msWorld.Y))
						{
							if (ship.Parts[i].IsNew)
								Game1.Cash += ship.Parts[i].Cost;
							else
								Game1.Cash += ship.Parts[i].Cost / 2;
							ship.RemovePart(ship.Parts[i]);
							break;
						}
					}
				}
			}

			prevMs = ms;
		}


		float fuelAnim = 0.0f;
		Vector2 fuelSnap = Vector2.Zero;
		public void Draw(SpriteBatch batch, Vector2 msWorld, float zoom, Ship ship, Matrix camera)
		{
			fuelAnim += 0.2f;
			for (int i = 0; i < icons.Count; i++)
			{
				Color col = Color.White;
				if (i == selectedID)
					col = Color.Red;
				batch.Draw(icons[i], new Vector2(0, i * 32), col);
			}

			foreach (IPart part in ship.Parts)
			{
				FuelTank tank = part as FuelTank;

				if (tank == null)
					continue;

				if (tank.fuelTank != null)
				{
					Vector2 dir = new Vector2(tank.fuelTank.localX - tank.localX, tank.fuelTank.localY - tank.localY) * 32;

					float rotation = (float)Math.Atan2(dir.Y, dir.X);

					int segments = (int)(dir.Length() / 16.0f);
					for (int i = 1; i < segments; i++)
					{
						batch.Draw(icons[6], Vector2.Transform(Vector2.Lerp(new Vector2(tank.localX * 32, tank.localY * 32), new Vector2(tank.fuelTank.localX * 32, tank.fuelTank.localY * 32), (float)i / (float)segments) + ship.position, camera), new Rectangle((int)-fuelAnim, 0, 32, 32), Color.White, rotation, new Vector2(16, 16), zoom, SpriteEffects.None, 0.0f);
					}

					batch.Draw(fuelEnd, Vector2.Transform(new Vector2(tank.localX * 32, tank.localY * 32) + ship.position, camera), null, Color.White, rotation, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);
					batch.Draw(fuelEnd, Vector2.Transform(new Vector2(tank.fuelTank.localX * 32, tank.fuelTank.localY * 32) + ship.position, camera), null, Color.White, rotation, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);
				}
			}


			foreach (IPart part in ship.Parts)
			{
				Thruster thruster = part as Thruster;

				if (thruster == null || thruster.fuelTank == null)
					continue;

				Vector2 dir = new Vector2(thruster.localX - thruster.fuelTank.localX, thruster.localY - thruster.fuelTank.localY) * 32;
				float rotation = (float)Math.Atan2(dir.Y, dir.X);

				int segments = (int)(dir.Length() / 16.0f);
				for (int i = 1; i < segments; i++)
				{
					batch.Draw(icons[6], Vector2.Transform(Vector2.Lerp(new Vector2(thruster.localX * 32, thruster.localY * 32), new Vector2(thruster.fuelTank.localX * 32, thruster.fuelTank.localY * 32), (float)i / (float)segments) + ship.position, camera), new Rectangle((int)-fuelAnim, 0, 32, 32), Color.White, rotation, new Vector2(16, 16), zoom, SpriteEffects.None, 0.0f);
				}

				batch.Draw(fuelEnd, Vector2.Transform(new Vector2(thruster.localX * 32, thruster.localY * 32) + ship.position, camera), null, Color.White, rotation, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);
				batch.Draw(fuelEnd, Vector2.Transform(new Vector2(thruster.fuelTank.localX * 32, thruster.fuelTank.localY * 32) + ship.position, camera), null, Color.White, rotation, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);
			}

			Vector2 snap = msWorld;
			if (fuelLinking)
			{
				batch.Draw(fuelEnd, Vector2.Transform(snap, camera), null, Color.White, 0.0f, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);
				if (linkFuelTank0 != null)
				{
					batch.Draw(fuelEnd, Vector2.Transform(new Vector2(linkFuelTank0.localX * 32, linkFuelTank0.localY * 32) + ship.position, camera), null, Color.White, 0.0f, new Vector2(8, 8), zoom, SpriteEffects.None, 0.0f);

					Vector2 dir = snap - linkFuelTank0.WorldPosition();
					float rotation = (float)Math.Atan2(dir.Y, dir.X);

					int segments = (int)(dir.Length() / 16.0f);
					for (int i = 1; i < segments; i++)
					{
						batch.Draw(icons[6], Vector2.Transform(Vector2.Lerp(linkFuelTank0.WorldPosition() - ship.position, snap - ship.position, (float)i / (float)segments) + ship.position, camera), new Rectangle((int)-fuelAnim, 0, 32, 32), new Color(0.2f, 0.2f, 0.2f, 0.2f), rotation, new Vector2(16, 16), zoom, SpriteEffects.None, 0.0f);
					}

				}
			}

			if (selectedID > -1)
			{
				//batch.Draw(icons[selectedID], Vector2.Transform(snap, camera) - new Vector2(16, 16), Color.White);
				batch.Draw(icons[selectedID], Vector2.Transform(snap, camera), null, new Color(0.2f, 0.2f, 0.2f, 0.2f), 0.0f, new Vector2(16, 16), zoom, SpriteEffects.None, 0.0f);
			}
		}

		private void Buypart(ShipPart part, Ship ship)
		{
			if (part != null && Game1.Cash >= part.Cost)
			{
				part.LoadContent();
				ship.AddPart(part);
				Game1.Cash -= part.Cost;
			}
		}

		private ShipPart GetNewPart(int x, int y, Ship ship, int id)
		{
			if (id == -1)
				return null;
			ShipPart newPart = null;
			switch (id)
			{
				case 0:
					{
						newPart = new Hull(x, y);
						if (ship.Parts.Count == 0)
							(newPart as Hull).IsRoot = true;
					}
					break;
				case 1:
					{
						newPart = new Thruster(x, y);
					}
					break;
				case 2:
					{
						newPart = new RCSThruster(x, y);
					}
					break;
				case 3:
					{
						newPart = new Solarpanel(x, y);
					}
					break;
				case 4:
					{
						newPart = new Battery(x, y);
					}
					break;
				case 5:
					{
						newPart = new FuelTank(x, y);
					}
					break;
				case 6:
					{
						//fuelline
					}
					break;
				case 7:
					{
						newPart = new Seperator(x, y);
					}
					break;
				case 8:
					{
						newPart = new Laser(x, y);
					}
					break;
				case 9:
					{
						newPart = new Miner(x, y);
					}
					break;
				case 10:
					{
						newPart = new MinerAuto(x, y);
					}
					break;
				case 11:
					{
						newPart = new Cargobay(x, y);
					}
					break;
				case 12:
					{
						newPart = new Falcon(x, y);
					}
					break;
				case 13:
					{
						newPart = new TractorBeam(x, y);
					}
					break;
				default:
					break;
			}
			newPart.localX = x;
			newPart.localY = y;
			newPart.TypeID = (byte)id;
			return newPart;
		}
	}
}
