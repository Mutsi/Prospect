﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsGame1.Models;

namespace WindowsGame1
{
	[Serializable()]
	public class Mineral
	{
		public Vector2 worldPos;
		public int type;
		public float rotation = 0.0f;
		public float scale = 1.0f;
		public Resource resource;

		private Color color;

		public Mineral(Resource res)
		{
			resource = res;
			color = ResourceData.ResourceColor[res];
		}

		public void Draw(SpriteBatch batch, Texture2D sheet)
		{
			batch.Draw(sheet, worldPos, new Rectangle(0, type * 32, 32, 32), color, rotation, new Vector2(16, 16), scale, SpriteEffects.None, 0);
		}
	}

	[Serializable()]
	public class Planet
	{
		[NonSerialized()]
		public int planetID;
		[NonSerialized()]
		public Vector2 position;
		[NonSerialized()]
		private Texture2D texture, atmo, refactory;
		[NonSerialized()]
		public float r = 0;
		[NonSerialized()]
		public List<Mineral> minerals;
		[NonSerialized()]
		public Color color;
		[NonSerialized()]
		public bool hasAtmo = false;
		[NonSerialized()]
		public float atmoStrenght = 0.0f;
		[NonSerialized()]
		public Color atmoColor;
		[NonSerialized()]
		public bool isMoon;
		[NonSerialized()]
		public Planet parent;
		[NonSerialized()]
		private float moonRadius = 0.0f;
		[NonSerialized()]
		private float moonCycle = 0.0f;
		[NonSerialized()]
		private float moonSpeed = 0.0003f;
		[NonSerialized()]
		public Vector2 Velocity = Vector2.Zero;
		[NonSerialized()]
		private Vector2 prevPos;
		[NonSerialized()]
		public string Name = "";
		[NonSerialized()]
		Random lrandom;
		[NonSerialized()]
		private Texture2D mineralSheet;
		[NonSerialized()]
		public Building Building;

		public Planet(Random random)
		{
			minerals = new List<Mineral>();
			r = ((float)(random.NextDouble() * 1000) + 500) * 32;

			lrandom = new Random(random.Next());

			if (lrandom.NextDouble() > 0.7f)
			{
				hasAtmo = true;
				atmoStrenght = (float)lrandom.NextDouble();
			}

			color = new Color(lrandom.Next(255), lrandom.Next(255), lrandom.Next(255));
			atmoColor = new Color(lrandom.Next(255), lrandom.Next(255), lrandom.Next(255));
			atmoColor.A = (byte)(atmoStrenght * 255.0f);
			float range = 10000000.0f;
			position.X = (float)random.NextDouble() * range - range / 2;
			position.Y = (float)random.NextDouble() * range - range / 2;

			Name = Generator.GenerateName(lrandom);
		}

		public void MoonGenerate()
		{
			Planet gravPlanet = null;

			//Calculate GravPlanet
			Vector2 maxGrav = Vector2.Zero;
			float maxForce = 0.0f;
			foreach (Planet planet in Game1.planets)
			{
				double dist = Vector2.Distance(position, planet.position);
				double force = (1.0 / (dist * dist)) * Ship.GravConstant * (4.0 / 3.0 * Math.PI * Math.Pow(planet.r, 3));

				Vector2 gravVec = (float)force * Vector2.Normalize(planet.position - position);
				if (gravVec.Length() > maxGrav.Length())
				{
					maxGrav = gravVec;
					gravPlanet = planet;
					maxForce = (float)force;
				}
			}
			if (maxForce > 0.0005f && parent == null && gravPlanet != null)
			{
				isMoon = true;
				parent = gravPlanet;

				r /= 2;
				moonRadius = (parent.r + this.r) * 1.5f * ((float)lrandom.NextDouble() * 2.0f + 1.0f) * 2.0f;
			}
		}

		public void Populate()
		{
			if (lrandom.NextDouble() > 0.7f)
			{
				for (int y = (int)-r; y < r; y += 64)
				{
					for (int x = (int)-r; x < r; x += 64)
					{
						if (lrandom.NextDouble() > 0.9995f && Math.Sqrt(x * x + y * y) < r)
						{
							int selectResource = lrandom.Next(0, ResourceData.Minerals.Count);
							Resource res = ResourceData.Minerals[selectResource];
							Mineral mineral = new Mineral(res);
							mineral.worldPos.X = x + position.X;
							mineral.worldPos.Y = y + position.Y;

							mineral.type = lrandom.Next(0, mineralSheet.Height / 32);
							mineral.rotation = (float)(lrandom.NextDouble() * Math.PI);
							mineral.scale = (float)lrandom.NextDouble() * 25.0f + 5.0f;

							if (mineral.resource == Resource.Silver)
								mineral.scale *= 0.5f;
							if (mineral.resource == Resource.Gold)
								mineral.scale *= 0.25f;

							minerals.Add(mineral);
						}
					}
				}
			}
			else
			{
				Building = new Building(this, lrandom);
			}
		}

		public void Update()
		{
			/*if (isMoon)
			{
				position.X = parent.position.X + (float)Math.Cos(moonCycle) * moonRadius;
				position.Y = parent.position.Y + (float)Math.Sin(moonCycle) * moonRadius;
				moonCycle += moonSpeed;
			}

			Velocity = position - prevPos;
			prevPos = position;*/
		}

		public void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("Planet");
			if (hasAtmo)
				atmo = ResourceManager.Load<Texture2D>("Atmo");

			mineralSheet = ResourceManager.Load<Texture2D>("MineralSheet");

			refactory = ResourceManager.Load<Texture2D>("Refactory");
		}

		public void Draw(SpriteBatch batch, Matrix cameraTransform, float zoom, Vector2 playerPos)
		{
			//Cull offscreen planets
			float scrWidth = 1920 / zoom;
			float scrHeight = 1080 / zoom;
			float dScr = (float)Math.Sqrt(scrWidth * scrWidth + scrHeight * scrHeight);
			float distanceToPlayer = Vector2.Distance(position, playerPos);
			if (distanceToPlayer > r + dScr)
				return;

			if (hasAtmo)
				batch.Draw(atmo, position, null, atmoColor, 0.0f, new Vector2(800, 800), r / 1600.0f * 3.0f, SpriteEffects.None, 0.0f);

			Rectangle screenRect = new Rectangle(-128, -128, 1920 + 128, 1080 + 128);

			batch.Draw(texture, position, null, color, 0.0f, new Vector2(250, 250), r / 500.0f * 2.0f, SpriteEffects.None, 0.0f);

			foreach (Mineral mineral in minerals)
			{
				mineral.Draw(batch, mineralSheet);
			}

			if (Building != null)
			{
				batch.Draw(refactory, position, null, Color.White, 0.0f, new Vector2(48, 48), 1.0f, SpriteEffects.None, 0.0f);
				Game1.particleMaster.SpawnSmoke(100, position);
			}
		}

	}
}
