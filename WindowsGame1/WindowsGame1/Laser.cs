﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    [Serializable()]
    [ProtoContract]
    public class Laser : ShipPart
    {
        [NonSerialized()]
        private Texture2D laser, beam;

        public float laserRot = 0.0f;
        public Vector2 dir = Vector2.Zero;

        public float laserAnim = 0.0f;
        public bool isOn = false;
        public float laserDist = 0.0f;




        public Laser()
        {
            TypeID = 7;

        }
        public Laser(float x, float y)
        {
            TypeID = 7;
            Cost = 350;
        }

        public override void LoadContent()
        {
            texture = ResourceManager.Load<Texture2D>("GunMount");
            laser = ResourceManager.Load<Texture2D>("Laser");
            beam = ResourceManager.Load<Texture2D>("Beam");
			//if (guid == Guid.Empty)
				//guid = Guid.NewGuid();
		}





		public override void Action(Ship ship, bool isNet = false)
        {
            isOn = ship.fireSystem;

            dir = Vector2.Normalize(ship.MSWorld - worldPosition);
            laserRot = (float)Math.Atan2(dir.Y, dir.X);

            if (isOn)
            {
                laserDist = 16.0f * 32.0f;
                foreach (Ship other in Game1.GetShips)
                {
                    if (ship != other)
                    {
                        Hull nearest = null;
                        float min = 16.0f * 32.0f;

                        for (int i = 0; i < other.Parts.Count; i++)
                        {
                            Hull h = other.Parts[i] as Hull;
                            if (h == null)
                                continue;

                            Rectangle partRect = new Rectangle((int)h.WorldPosition().X - 16, (int)h.WorldPosition().Y - 16, 32, 32);

                            float dist;
                            if (Physics.CollideRayRectangle(partRect, worldPosition, worldPosition + (dir * 32.0f * 15), out dist))
                            {
                                if (dist < min)
                                {
                                    min = dist;
                                    nearest = h;
                                    laserDist = dist;
                                }
                            }
                        }

                        if (nearest != null)
                        {
                            Game1.particleMaster.SpawnDebries(1, nearest.WorldPosition());
                            nearest.Damage(0.01f, other);
                        }
                    }
                }
            }

        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(laser, worldPosition, null, Color.White, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);


            int segments = (int)(laserDist / 32.0f) + 1;
            if (isOn)
                for (int i = 1; i < segments; i++)
                {
                    batch.Draw(beam, worldPosition + (dir * 32.0f * i), new Rectangle((int)-laserAnim, 0, 32, 32), Color.White, laserRot, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0.0f);
                }
        }
    }
}
