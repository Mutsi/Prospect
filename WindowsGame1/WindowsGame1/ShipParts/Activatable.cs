﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1.ShipParts
{
	public interface Activatable
	{
		bool IsDirty { get; set; }
		ushort PartId { get; set; }
		byte[] Data { get; set; }
		void SetData();
	}
}