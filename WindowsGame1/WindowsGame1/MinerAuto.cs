﻿using Microsoft.Xna.Framework;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	class MinerAuto : Miner
	{

		public MinerAuto()
		{

			TypeID = 9;
			mineRange = 15.0f * 32.0f * 5.0f;
		}
		public MinerAuto(float x, float y)
		{
			TypeID = 9;
			Cost = 1000;
			mineRange = 15.0f * 32.0f * 5.0f;
		}


		public override void Action(Ship ship, bool isNet = false)
		{
			if (isNet == true)
				return;
			isOn = (ship.eCharge > 1.0f);
			miningMineral = null;

			if (isOn)
			{
				laserDist = mineRange;
				Mineral nearest = null;
				float min = float.MaxValue;

				Planet target = ship.gravPlanet != null ? ship.gravPlanet : ship.gravPlanet;

				if (target != null && target.minerals != null)
					for (int i = 0; i < ship.gravPlanet.minerals.Count; i++)
					{
						Mineral m = ship.gravPlanet.minerals[i];
						if (!ship.HasCargoRoom(m.resource))
							continue;

						float dist = Vector2.Distance(worldPosition, m.worldPos);
						if (dist < min && dist < mineRange)
						{
							min = dist;
							nearest = m;
							laserDist = dist;
							miningMineral = nearest;
						}
					}

				if (nearest != null)
				{
					Game1.particleMaster.SpawnTractor(1, nearest.worldPos, Vector2.Normalize(worldPosition - nearest.worldPos) * laserDist / 40.0f);
					nearest.scale -= mineSpeed;
					ship.VoidCargo.AddResource(nearest.resource, 0.1f);

					if (nearest.scale <= 0.0f)
						ship.gravPlanet.minerals.Remove(nearest);
				}
				else
				{
					isOn = false;
				}
				ship.eCharge -= 1.0f;

			}
			Vector2 aimAt = ship.MSWorld;
			if (miningMineral != null)
			{
				aimAt = miningMineral.worldPos;
			}
			dir = Vector2.Normalize(aimAt - worldPosition);
			laserRot = (float)Math.Atan2(dir.Y, dir.X);

			laserAnim += 1.0f;

		}

	}
}
