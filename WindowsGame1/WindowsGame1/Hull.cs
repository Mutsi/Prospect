﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	public class Hull : ShipPart
	{
		public float health = 1.0f;
		public bool IsRoot = false;
		public bool isChecked = false;

		public Hull()
		{

		}

		public Hull(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 0;
			Cost = 50;
		}

		public bool Damage(float dmg, Ship parent)
		{
			health -= 0.01f;
			if (health <= 0.0f)
			{
				parent.RemovePart(this);
				return true;
			}
			return false;
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("Hull");
		}


		public override bool ValidatePos(int x, int y, Ship ship)
		{
			if (ship.Parts.Count == 0)
			{
				x = 0;
				y = 0;
				IsRoot = true;
				return true;
			}

			int i = ship.parts.Count(o => o.localX == x && o.localY == y);
			bool valid0 = ship.parts.OfType<Hull>().Any(o => o.localX == x - 1 && o.localY == y);
			bool valid1 = ship.parts.OfType<Hull>().Any(o => o.localX == x + 1 && o.localY == y);
			bool valid2 = ship.parts.OfType<Hull>().Any(o => o.localX == x && o.localY == y - 1);
			bool valid3 = ship.parts.OfType<Hull>().Any(o => o.localX == x && o.localY == y + 1);
			float xx = x;
			float yy = y;

			return (valid0 || valid1 || valid2 || valid3) && i == 0;
		}

		public static bool FindRoot(Hull hull, Ship ship)
		{
			if (hull == null)
				return false;

			hull.isChecked = true;
			foreach (ShipPart part in ship.Parts)
			{
				Hull other = part as Hull;
				if (other == null || other.isChecked)
					continue;

				bool found = false;
				if (hull.localX == other.localX + 1 && hull.localY == other.localY)
				{
					found = FindRoot(other, ship);
				}
				if (hull.localX == other.localX - 1 && hull.localY == other.localY)
				{
					found = FindRoot(other, ship);
				}
				if (hull.localX == other.localX && hull.localY == other.localY + 1)
				{
					found = FindRoot(other, ship);
				}
				if (hull.localX == other.localX && hull.localY == other.localY - 1)
				{
					found = FindRoot(other, ship);
				}
			}

			return true;
		}

		public override void Update(Ship ship)
		{
			rotation = ship.rotation;
			Vector2 localCom = ship.CentreOfMass - ship.position;
			localCOMPosition = new Vector2(localX * 32, localY * 32) - localCom;

			Vector3 translation = Vector3.Transform(new Vector3(ship.CentreOfMass, 0.0f) + new Vector3(localCOMPosition, 0.0f), ship.PositionRotationMatrix);
			worldPosition.X = translation.X;
			worldPosition.Y = translation.Y;
		}

		public void BreakoffShip(Ship ship)
		{
			IsRoot = true;
			Ship newShip = new Ship(ship.dotTexture, ship.comTexture);
			newShip.guid = Guid.NewGuid();

			newShip.position = new Vector2(ship.position.X, ship.position.Y);
			newShip.rotation = ship.rotation;
			newShip.angularV = ship.angularV;
			newShip.inEditor = false;

			List<ShipPart> toDel = new List<ShipPart>();
			foreach (ShipPart part in ship.Parts)
			{
				Hull h = part as Hull;
				if (h != null && !h.isChecked)
				{
					toDel.Add(h);
				}
			}

			while (toDel.Count > 0)
			{
				ship.RemovePart(toDel[0]);
				newShip.AddPart(toDel[0]);
				toDel.RemoveAt(0);
			}

			newShip.CalculateCenterOfMass();
			Vector2 dif = newShip.CentreOfMass - ship.CentreOfMass;
			dif.Normalize();

			newShip.linearV = ship.linearV;
			Game1.GetShips.Add(newShip);


			//Cleanup various part info sharing
			foreach (ShipPart part in ship.Parts)
			{
				part.Transfer(ship);
			}
			foreach (ShipPart part in newShip.Parts)
			{
				part.Transfer(newShip);
			}
		}

		public override void Transfer(Ship ship)
		{
			//isChecked = false;
		}

		public override void Draw(SpriteBatch batch)
		{
			Color col = Color.White;
			if (isChecked == false)
				col = Color.Red;

			col *= health;
			col.A = 255;

			if (IsRoot)
				col = Color.Green;

			batch.Draw(texture, worldPosition, null, col, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}
	}
}
