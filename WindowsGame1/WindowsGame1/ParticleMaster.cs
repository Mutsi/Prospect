﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using WindowsGame1;

namespace ParticleSystem
{
	public class ParticleMaster
	{
		public const int MAXPARTICLES = 20000;

		private Queue<Particle> freeParticles = new Queue<Particle>(MAXPARTICLES);
		public List<Particle> particleList = new List<Particle>(MAXPARTICLES);

		private Texture2D part1Text;
		private Texture2D part2Text, part3Text;

		private Random random = new Random();

		public void LoadContent()
		{
			part1Text = ResourceManager.Load<Texture2D>("Particles/Fire");
			part2Text = ResourceManager.Load<Texture2D>("Particles/Debris");
			part3Text = ResourceManager.Load<Texture2D>("MineralSheet");

			for (int i = 0; i < MAXPARTICLES; i++)
			{
				freeParticles.Enqueue(new Particle());
			}
		}

		public void Spawn(int amount, Vector2 worldPos, Vector2 dir)
		{
			for (int i = 0; i < amount; i++)
			{
				float x = (float)random.NextDouble() - 0.5f;
				float y = (float)random.NextDouble() - 0.5f;
				x *= 16.0f;
				y *= 16.0f;

				float randomStartRotation = (float)random.NextDouble();
				randomStartRotation *= 360.0f;
				//Life,lifeRnd, Size, Rotation, randomStartRotation, Type, Start Position, Speed
				if (freeParticles.Count > 0)
				{
					Particle newParticle = freeParticles.Dequeue();
					newParticle.Set(5, 5, 1.0f, 0.01f, randomStartRotation, 1, worldPos + new Vector2(x, y) + (dir * (float)random.NextDouble() * 128) / 5, dir * 3.0f, Color.DarkCyan);
					particleList.Add(newParticle);
				}
			}
		}
		public void SpawnFalcon(int amount, Vector2 worldPos, Vector2 dir)
		{
			for (int i = 0; i < amount; i++)
			{
				float x = (float)random.NextDouble() - 0.5f;
				float y = (float)random.NextDouble() - 0.5f;
				x *= 16.0f;
				y *= 16.0f;

				float randomStartRotation = (float)random.NextDouble();
				randomStartRotation *= 360.0f;
				//Life,lifeRnd, Size, Rotation, randomStartRotation, Type, Start Position, Speed
				if (freeParticles.Count > 0)
				{
					Particle newParticle = freeParticles.Dequeue();
					newParticle.Set(15, 5, 0.33f, 0.01f, randomStartRotation, 1, worldPos + new Vector2(x, y) + (dir * (float)random.NextDouble() * 128) / 5, dir * 3.0f, Color.OrangeRed);
					particleList.Add(newParticle);
				}
			}
		}


		public void SpawnDebries(int amount, Vector2 worldPos)
		{
			for (int i = 0; i < amount; i++)
			{
				Vector2 dir = new Vector2((float)random.NextDouble() - 0.5f, (float)random.NextDouble() - 0.5f);
				float x = (float)random.NextDouble() - 0.5f;
				float y = (float)random.NextDouble() - 0.5f;
				x *= 8.0f;
				y *= 8.0f;

				float randomStartRotation = (float)random.NextDouble();
				randomStartRotation *= 360.0f;

				if (freeParticles.Count > 0)
				{
					Particle newParticle = freeParticles.Dequeue();
					newParticle.Set(50, 0, 0.3f * (float)random.NextDouble(), 0.0f, randomStartRotation, 2, worldPos + new Vector2(x, y), dir * 3.0f, Color.White);
					particleList.Add(newParticle);
				}
				//Life,lifeRnd, Size, Rotation, randomStartRotation, Type, Start Position, Speed
				//particleList.Add(new Particle(, dir * 3.0f));
			}
		}

		public void SpawnTractor(int amount, Vector2 worldPos, Vector2 dir)
		{
			for (int i = 0; i < amount; i++)
			{
				float x = (float)random.NextDouble() - 0.5f;
				float y = (float)random.NextDouble() - 0.5f;
				x *= 8.0f;
				y *= 8.0f;

				float randomStartRotation = (float)random.NextDouble();
				randomStartRotation *= 360.0f;

				if (freeParticles.Count > 0)
				{
					Particle newParticle = freeParticles.Dequeue();
					newParticle.Set(30, 10, 0.3f * (float)random.NextDouble(), 0.01f, randomStartRotation, 3, worldPos + new Vector2(x, y), dir, Color.White);
					particleList.Add(newParticle);
				}
				//Life,lifeRnd, Size, Rotation, randomStartRotation, Type, Start Position, Speed
				//particleList.Add(new Particle(30, 10, 0.3f * (float)random.NextDouble(), 0.01f, randomStartRotation, 3, worldPos + new Vector2(x, y), dir));
			}
		}

		public void SpawnSmoke(int amount, Vector2 worldPos)
		{
			Vector2 randSpeed = new Vector2((float)random.NextDouble(), (float)random.NextDouble());
			randSpeed.X = -0.2f;
			randSpeed.Y = 0.5f;

			randSpeed.X += (float)random.NextDouble() * 0.3f;


			float randomStartRotation = (float)random.NextDouble();
			randomStartRotation *= 360.0f;

			if (freeParticles.Count > 0)
			{
				Particle newParticle = freeParticles.Dequeue();
				newParticle.Set(50, 100, 0.3f, 0.05f, randomStartRotation, 1, worldPos, randSpeed, Color.White);
				particleList.Add(newParticle);
			}
			//Life,lifeRnd, Size, Rotation, randomStartRotation, Type, Start Position, Speed
			//particleList.Add(new Particle());
		}

		public void Update()
		{
			for (int i = 0; i < particleList.Count; i++)
			{
				if (particleList[i].type == 0)
				{
					freeParticles.Enqueue(particleList[i]);
					particleList.Remove(particleList[i]);
				}
				else
				{
					particleList[i].Update();
				}
			}
		}

		public void Draw(SpriteBatch spriteBatch, Matrix camTrans)
		{
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, null, null, null, null, camTrans);
			foreach (Particle particle in particleList)
			{
				if (particle.type == 1)
				{
					spriteBatch.Draw(part1Text, particle.position, null, particle.color, particle.currentRotation, new Vector2(16 * particle.size, 16 * particle.size), particle.size, SpriteEffects.None, 0f);
				}
			}
			spriteBatch.End();

			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, null, null, null, null, camTrans);
			foreach (Particle particle in particleList)
			{
				if (particle.type == 2)
				{
					spriteBatch.Draw(part2Text, particle.position, null, particle.color, particle.currentRotation, new Vector2(16 * particle.size, 16 * particle.size), particle.size, SpriteEffects.None, 0f);
				}

				if (particle.type == 3)
				{
					spriteBatch.Draw(part3Text, particle.position, new Rectangle(0, 0, 32, 32), Color.White, particle.currentRotation, new Vector2(16 * particle.size, 16 * particle.size), particle.size, SpriteEffects.None, 0f);
				}
			}
			spriteBatch.End();
		}
	}
}
