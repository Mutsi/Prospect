﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
	public static class ResourceManager
	{
		public static Dictionary<string, object> ResourceCache = new Dictionary<string, object>();

		public static T Load<T>(string name)
		{
			if (ResourceCache.ContainsKey(name))
				return (T)ResourceCache[name];

			T resource = Game1.ContentRef.Load<T>(name);

			ResourceCache.Add(name, resource);

			return resource;
		}
	}
}
