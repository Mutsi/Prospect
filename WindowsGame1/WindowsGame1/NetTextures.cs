﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    public class NetTextures
    {
        public static Dictionary<int, Texture2D> Textures = new Dictionary<int, Texture2D>();

        public static Texture2D hull;
        public static Texture2D GetHull()
        {
            if (hull == null)
                hull = Game1.ContentRef.Load<Texture2D>("Hull");
            return hull;
        }

        public static Type GetType(int id)
        {
            Type selectedPart = typeof(Hull);

            switch (id)
            {
                case 0:
                    {
                        selectedPart = typeof(Hull);
                    }
                    break;
                case 1:
                    {
                        selectedPart = typeof(Thruster);
                    }
                    break;
                case 2:
                    {
                        selectedPart = typeof(RCSThruster);
                    }
                    break;
                case 3:
                    {
                        selectedPart = typeof(Solarpanel);
                    }
                    break;
                case 4:
                    {
                        selectedPart = typeof(Battery);
                    }
                    break;
                case 5:
                    {
                        selectedPart = typeof(FuelTank);
                    }
                    break;
                case 6:
                    {
                        //fuelline
                    }
                    break;
                case 7:
                    {
                        selectedPart = typeof(Seperator);
                    }
                    break;
                case 8:
                    {
                        selectedPart = typeof(Laser);
                    }
                    break;
                case 9:
                    {
                        selectedPart = typeof(Miner);
                    }
                    break;
                case 10:
                    {
                        selectedPart = typeof(MinerAuto);
                    }
                    break;
                case 11:
                    {
                        selectedPart = typeof(Cargobay);
                    }
                    break;
                case 12:
                    {
                        selectedPart = typeof(Falcon);
                    }
                    break;
                case 13:
                    {
                        selectedPart = typeof(TractorBeam);
                    }
                    break;
                default:
                    break;
            }
            return selectedPart;
        }

        public static ShipPart GetPart(int id, Vector2 snapgrid, float rotation)
        {
            ShipPart selectedPart = new ShipPart() ;

            switch (id)
            {
                case 0:
                    {
                        selectedPart = new Hull((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 1:
                    {
                        selectedPart = new Thruster((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 2:
                    {
                        selectedPart = new RCSThruster((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 3:
                    {
                        selectedPart = new Solarpanel((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 4:
                    {
                        selectedPart = new Battery((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 5:
                    {
                        selectedPart = new FuelTank((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 6:
                    {
                        //fuelline
                    }
                    break;
                case 7:
                    {
                        selectedPart = new Seperator((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 8:
                    {
                        selectedPart = new Laser((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 9:
                    {
                        selectedPart = new Miner((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 10:
                    {
                        selectedPart = new MinerAuto((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 11:
                    {
                        selectedPart = new Cargobay((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 12:
                    {
                        selectedPart = new Falcon((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                case 13:
                    {
                        selectedPart = new Miner((int)snapgrid.X/32, (int)snapgrid.Y/32);
                    }
                    break;
                default:
                    break;
            }

            selectedPart.worldPosition = snapgrid;
            selectedPart.rotation = rotation;
            return selectedPart;
        }
    }
}
