﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using ProtoBuf;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	public class FuelTank : ShipPart
	{
		[NonSerialized()]
		private Texture2D texture2;

		public float capacity = 50;

		[ProtoMember(1)]
		public float fuel;
		protected float fuelDensity = 2.5f;

		private float fuelAnim = 0.0f;

		public FuelTank fuelTank;
		public int children = 0;

		public FuelTank()
		{
			TypeID = 5;

		}

		public FuelTank(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 5;
			fuel = capacity;
			mass = 10.0f;
			Cost = 100;
		}

		public override float Mass()
		{
			return mass + fuel * fuelDensity;
		}


		public override void Transfer(Ship ship)
		{
			bool tankFound = false;
			if (fuelTank != null)
			{
				foreach (ShipPart part in ship.Parts)
				{
					if (part == fuelTank)
					{
						tankFound = true;
						return;
					}
				}
				if (!tankFound)
				{
					fuelTank = null;
				}
			}

		}


		public override void Action(Ship ship, bool isNet = false)
		{

			if (fuelTank != null && fuelTank.IsDestroyed)
				fuelTank = null;

			fuelAnim += 0.11f;

			//Transfer fuel to parent tank
			if (fuelTank != null && fuel > 0.0f && fuelTank.fuel < fuelTank.capacity)
			{
				float dif = fuelTank.capacity - fuelTank.fuel;
				dif /= fuelTank.children;
				if (fuel >= dif)
				{
					fuelTank.fuel += dif;
					fuel -= dif;
				}
				else
				{
					fuelTank.fuel += fuel;
					fuel = 0.0f;
				}
			}
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("FuelTank");
			texture2 = ResourceManager.Load<Texture2D>("LiquidFuel");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}

		public override void Draw(SpriteBatch batch)
		{
			Color fuelCol = new Color((float)Math.Sin(fuelAnim) * 0.3f + 0.9f, (float)Math.Sin(fuelAnim) * 0.3f + 0.9f, (float)Math.Sin(fuelAnim) * 0.3f + 0.9f, 1.0f);
			batch.Draw(texture2, worldPosition, null, fuelCol, rotation, new Vector2(16, 16), (fuel / capacity), SpriteEffects.None, 0);
			batch.Draw(texture, worldPosition, null, Color.White, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}
	}
}
