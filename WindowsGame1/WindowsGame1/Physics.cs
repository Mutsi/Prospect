﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    public static class Physics
    {
        public static bool CollideRayRectangle(Rectangle rec, Vector2 p0, Vector2 p1, out float distance)
        {
            // Find min and max X for the segment

            float minX = p0.X;
            float maxX = p1.X;

            if (p0.X > p1.X)
            {
                minX = p1.X;
                maxX = p0.X;
            }

            // Find the intersection of the segment's and rectangle's x-projections

            if (maxX > rec.Right)
            {
                maxX = rec.Right;
            }

            if (minX < rec.Left)
            {
                minX = rec.Left;
            }

            if (minX > maxX) // If their projections do not intersect return false
            {
                distance = 0.0f;
                return false;
            }

            // Find corresponding min and max Y for min and max X we found before

            float minY = p0.Y;
            float maxY = p1.Y;

            float dx = p1.X - p0.X;

            if (Math.Abs(dx) > 0.0000001)
            {
                float a = (p1.Y - p0.Y) / dx;
                float b = p0.Y - a * p0.X;
                minY = a * minX + b;
                maxY = a * maxX + b;
            }

            if (minY > maxY)
            {
                float tmp = maxY;
                maxY = minY;
                minY = tmp;
            }

            // Find the intersection of the segment's and rectangle's y-projections

            if (maxY > rec.Bottom)
            {
                maxY = rec.Bottom;
            }

            if (minY < rec.Top)
            {
                minY = rec.Top;
            }

            if (minY > maxY) // If Y-projections do not intersect return false
            {
                distance = 0.0f;
                return false;
            }

            distance = Vector2.Distance(new Vector2(rec.Center.X, rec.Center.Y), p0);
            return true;
        }

        public static float TriangleArea(Vector2 A, Vector2 B, Vector2 C)
        {
            return (C.X * B.Y - B.X * C.Y) - (C.X * A.Y - A.X * C.Y) + (B.X * A.Y - A.X * B.Y);
        }
        public static bool isInsideSquare(Vector2 A, Vector2 B, Vector2 C, Vector2 D, Vector2 P)
        {
            if (TriangleArea(A, B, P) > 0 || TriangleArea(B, C, P) > 0 || TriangleArea(C, D, P) > 0 || TriangleArea(D, A, P) > 0)
            {
                return false;
            }
            return true;
        }

        public static readonly int gridWidth = 32;
        public static readonly Vector2 Grid = new Vector2(32, 32);
        public static readonly Vector2 HalfGrid = new Vector2(Grid.X / 2, Grid.Y / 2);

        // ~~~~ Round to nearest Grid point ~~~~
        public static Vector2 SnapCalculate(Vector2 p)
        {

            int nearGridX = gridWidth * (int)Math.Round(p.X / gridWidth);
            int nearGridY = gridWidth * (int)Math.Round(p.Y / gridWidth);

            //int snapX = ((int)((p.X + HalfGrid.X) / Grid.X)) * (int)Grid.X;
            //int snapY = ((int)((p.Y + HalfGrid.Y) / Grid.Y)) * (int)Grid.Y;

            return new Vector2(nearGridX, nearGridY);
        }

        public static string ConvertKeyToChar(Keys key, bool shift)
        {
            switch (key)
            {
                case Keys.Space: return " ";

                // Escape Sequences 
                //case Keys.Enter: return "\n";                         // Create a new line 
                //case Keys.Tab: return "\t";                           // Tab to the right 

                // D-Numerics (strip above the alphabet) 
                case Keys.D0: return shift ? ")" : "0";
                case Keys.D1: return shift ? "!" : "1";
                case Keys.D2: return shift ? "@" : "2";
                case Keys.D3: return shift ? "#" : "3";
                case Keys.D4: return shift ? "$" : "4";
                case Keys.D5: return shift ? "%" : "5";
                case Keys.D6: return shift ? "^" : "6";
                case Keys.D7: return shift ? "&" : "7";
                case Keys.D8: return shift ? "*" : "8";
                case Keys.D9: return shift ? "(" : "9";

                // Numpad 
                case Keys.NumPad0: return "0";
                case Keys.NumPad1: return "1";
                case Keys.NumPad2: return "2";
                case Keys.NumPad3: return "3";
                case Keys.NumPad4: return "4";
                case Keys.NumPad5: return "5";
                case Keys.NumPad6: return "6";
                case Keys.NumPad7: return "7";
                case Keys.NumPad8: return "8";
                case Keys.NumPad9: return "9";
                case Keys.Add: return "+";
                case Keys.Subtract: return "-";
                case Keys.Multiply: return "*";
                case Keys.Divide: return "/";
                case Keys.Decimal: return ".";

                // Alphabet 
                case Keys.A: return shift ? "A" : "a";
                case Keys.B: return shift ? "B" : "b";
                case Keys.C: return shift ? "C" : "c";
                case Keys.D: return shift ? "D" : "d";
                case Keys.E: return shift ? "E" : "e";
                case Keys.F: return shift ? "F" : "f";
                case Keys.G: return shift ? "G" : "g";
                case Keys.H: return shift ? "H" : "h";
                case Keys.I: return shift ? "I" : "i";
                case Keys.J: return shift ? "J" : "j";
                case Keys.K: return shift ? "K" : "k";
                case Keys.L: return shift ? "L" : "l";
                case Keys.M: return shift ? "M" : "m";
                case Keys.N: return shift ? "N" : "n";
                case Keys.O: return shift ? "O" : "o";
                case Keys.P: return shift ? "P" : "p";
                case Keys.Q: return shift ? "Q" : "q";
                case Keys.R: return shift ? "R" : "r";
                case Keys.S: return shift ? "S" : "s";
                case Keys.T: return shift ? "T" : "t";
                case Keys.U: return shift ? "U" : "u";
                case Keys.V: return shift ? "V" : "v";
                case Keys.W: return shift ? "W" : "w";
                case Keys.X: return shift ? "X" : "x";
                case Keys.Y: return shift ? "Y" : "y";
                case Keys.Z: return shift ? "Z" : "z";

                // Oem 
                //case Keys.OemOpenBrackets: return shift ? "{" : "[";
                //case Keys.OemCloseBrackets: return shift ? "}" : "]";
                //case Keys.OemComma: return shift ? "<" : ",";
                //case Keys.OemPeriod: return shift ? ">" : ".";
                //case Keys.OemMinus: return shift ? "_" : "-";
                //case Keys.OemPlus: return shift ? "+" : "=";
                //case Keys.OemQuestion: return shift ? "?" : "/";
                //case Keys.OemSemicolon: return shift ? ":" : ";";
                //case Keys.OemQuotes: return shift ? "\"" : "'";
                //case Keys.OemPipe: return shift ? "|" : "\\";
                //case Keys.OemTilde: return shift ? "~" : "`";
            }

            return string.Empty;
        }

    }
}
