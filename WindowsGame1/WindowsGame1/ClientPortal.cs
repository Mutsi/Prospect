﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace WindowsGame1
{
	public partial class ClientPortal : Form
	{
		public static bool RestartRequired = false;
		public static bool isServer = false;

		public ClientPortal()
		{
			bool xnaInstalled = false;
			//CheckXNAFramework(out xnaInstalled);

			/*if (!xnaInstalled)
			{
				MessageBox.Show("Could not find an installation for XNA 4.0 redistributable please install from miscrosoft.", "XNA 4.0 Runtime Installation Error",
		MessageBoxButtons.OK, MessageBoxIcon.Error);

				Application.Exit();
			}*/

			InitializeComponent();

			string curDir = Environment.CurrentDirectory;
			FileStream fileStream = new FileStream(curDir + "/version.txt", FileMode.OpenOrCreate);
			StreamReader reader = new StreamReader(fileStream);
			string localVersion = reader.ReadToEnd();
			string remoteVersion = CheckVersion();

			reader.Close();
			fileStream.Close();

			StreamWriter writer = new StreamWriter(curDir + "/version.txt", false);
			writer.Write(remoteVersion);
			writer.Flush();
			writer.Close();

			if (localVersion != remoteVersion)
			{
				MessageBox.Show("Update Required", "Close this message and please wait while the client is being updated.");
				Application.Exit();
				ClientPortal.RestartRequired = true;
			}
		}

		private void ClientPortal_Load(object sender, EventArgs e)
		{

		}

		public string CheckVersion()
		{
			/*bool isMutsi = false;
			string mutsiWeb = "84.104.184.25";
			IPAddress[] addresslist = Dns.GetHostAddresses(mutsiWeb);
			string externalip = new WebClient().DownloadString("http://icanhazip.com").Trim();

			foreach (IPAddress theaddress in addresslist)
			{
				if (theaddress.ToString() == externalip)
					isMutsi = true;
			}

			*/
			//Console.WriteLine(externalip);

			try
			{
				//string address = isMutsi ? "127.0.0.1" : "52.232.46.126";
				string address = "52.232.46.126";
				FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format("ftp://{0}:21/version.txt", address));
				request.Method = WebRequestMethods.Ftp.DownloadFile;
				request.UsePassive = false;
				request.UseBinary = true;
				request.Credentials = new NetworkCredential("prospectclient", "");
				//request.KeepAlive = false;
				//request.UseBinary = true;
				//request.UsePassive = true;
				FtpWebResponse response = (FtpWebResponse)request.GetResponse();
				Stream responseStream = response.GetResponseStream();
				StreamReader reader = new StreamReader(responseStream);
				string version = reader.ReadToEnd();
				Console.WriteLine("Download Complete", response.StatusDescription);
				reader.Close();
				response.Close();

				versionLabel.Text = version;
				return version;
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				versionLabel.Text = "Update Service Offline";
				return "";
			}

		}
		public string CheckXNAFramework(out bool ok)
		{
			string output = "";

			string baseKeyName = @"SOFTWARE\Microsoft\XNA\Game Studio";
			Microsoft.Win32.RegistryKey installedFrameworkVersions = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(baseKeyName);

			string[] versionNames = installedFrameworkVersions.GetSubKeyNames();

			bool found = false;
			foreach (string s in versionNames)
			{
				if (s == "v4.0")
				{
					found = true;
					break;
				}
			}
			if (found)
			{
				output += "Microsoft XNA Framework found successfully.\n";
				ok = true;
			}
			else
			{
				output += "Correct version of the Microsoft XNA Framework not found. Please install version 4.0 or higher.\n";
				ok = false;
			}

			return output;
		}

		private void startButton_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void startServerButton_Click(object sender, EventArgs e)
		{
			isServer = true;
			this.Close();
		}
	}
}
