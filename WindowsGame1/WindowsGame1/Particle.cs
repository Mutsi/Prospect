﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ParticleSystem
{
    public class Particle
    {
        public float life;
        public float size;
        public float rotation;
        public int type;

        public float currentRotation;

        public Vector2 position;
        public Vector2 speed;
        public Color color;

        private byte fadeAmountA, fadeAmountR, fadeAmountG, fadeAmountB;


        public void Set(float life, float lifeRnd, float size, float rotation, float randomStartRotation, int type, Vector2 position, Vector2 speed, Color color)
        {
            this.life = life;
            this.size = size;
            this.rotation = rotation;
            this.type = type;
            this.position = position;
            this.speed = speed;
            this.currentRotation = randomStartRotation;

            this.color = color;

            this.fadeAmountA = (byte)(color.A / this.life);
            this.fadeAmountR = (byte)(color.R / this.life);
            this.fadeAmountG = (byte)(color.G / this.life);
            this.fadeAmountB = (byte)(color.B / this.life);
        }

        public void Update()
        {
            if (life > 0)
            {
                life--;
                color.A -= (byte)fadeAmountA;

                color.R -= (byte)fadeAmountR;
                color.G -= (byte)fadeAmountG;
                color.B -= (byte)fadeAmountB;

                this.position += speed;
                this.currentRotation += rotation;
            }
            else
            {
                type = 0;
            }


        }
    }
}
