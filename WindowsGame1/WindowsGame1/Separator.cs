﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;

namespace WindowsGame1
{
    [Serializable()]
    [ProtoContract]
    class Seperator : ShipPart
    {
        public Seperator()
        {
            isRunning = false;
            TypeID = 6;
        }


        public Seperator(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 6;
            isRunning = false;
        }

        public override void Action(Ship ship, bool isNet = false)
        {
            ship.RemovePart(parent);
            ship.RemovePart(this);
        }

        public override void LoadContent()
        {
            texture = ResourceManager.Load<Texture2D>("Seperator");
			//if (guid == Guid.Empty)
				//guid = Guid.NewGuid();
		}

		public override void Draw(SpriteBatch batch)
        {
            batch.Draw(texture, worldPosition, null, Color.White, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
        }
    }
}
