﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NetObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TomShane.Neoforce.Controls;
using WindowsGame1.Models;

namespace WindowsGame1
{
	class HUD
	{
		public static SpriteFont font;
		private Texture2D iconPrograde;
		private Texture2D nixel;
		private Texture2D bg;

		private Texture2D landClear;
		private Texture2D arrow;

		private SpriteFont digiFont;


		Window ShipWindow;
		Label ShipInfoLabel;

		Window ShipListWindow;
		ListBox ShipList;
		Button NewShipButton;

		Window ClientListWindow;
		ListBox ClientList;

		Window TradeWindow;
		ListBox TradeList;
		private Label buyLabel;
		private Label sellLabel;
		private TrackBar tbBuy;
		private TrackBar tbSell;

		private TrackBar ThrottleTrackbar;

		private void CreateClientListWindow()
		{

			ClientListWindow = new Window(Game1.GUIManager);

			ClientList = new ListBox(Game1.GUIManager);
			ClientList.SetPosition(0, 0);
			ClientList.Width = ClientListWindow.Width;
			ClientList.AutoHeight(5);


			ClientListWindow.Text = "Connections";
			ClientListWindow.Add(ClientList);
			ClientListWindow.AutoScroll = false;
			ClientListWindow.SetSize(340, 240);
			ClientListWindow.SetPosition(16, Game1.SCRHEIGHT - ClientListWindow.Height - 16);
			ClientListWindow.Alpha = 180;
			ClientListWindow.CloseButtonVisible = false;
			ClientListWindow.IconVisible = false;
			ClientListWindow.MovableArea = new Rectangle(0, 0, 320, 240);
			ClientListWindow.CaptionVisible = false;


			Game1.GUIManager.Add(ClientListWindow);
		}

		private void CreateTradeListWindow()
		{
			TradeWindow = new Window(Game1.GUIManager);
			TradeList = new ListBox(Game1.GUIManager);
			TradeList.SetPosition(0, 0);
			TradeList.Width = TradeWindow.Width - 128;
			TradeList.AutoHeight(5);

			TradeWindow.Text = "Trade";
			TradeWindow.AutoScroll = false;
			TradeWindow.SetSize(512, 240);
			TradeWindow.SetPosition(300, Game1.SCRHEIGHT - TradeWindow.Height - 16);
			TradeWindow.Alpha = 180;
			TradeWindow.CloseButtonVisible = false;
			TradeWindow.IconVisible = false;
			TradeWindow.MovableArea = new Rectangle(0, 0, 320, 240);
			TradeWindow.CaptionVisible = false;
			TradeWindow.Visible = false;

			TradeWindow.Add(TradeList);

			buyLabel = new Label(Game1.GUIManager);
			buyLabel.Alignment = Alignment.TopLeft;
			buyLabel.Text = "BUY\nYeah\nFJkj";
			buyLabel.SetSize(256, 128);
			buyLabel.SetPosition(256 + 16, 4);

			sellLabel = new Label(Game1.GUIManager);
			buyLabel.Alignment = Alignment.TopLeft;
			sellLabel.SetSize(256, 128);
			sellLabel.Text = "SELL\nYeah\nFJkj";
			sellLabel.SetPosition(256 + 16, 32);

			tbBuy = new TrackBar(Game1.GUIManager);
			tbBuy.SetPosition(256 + 16, 48);
			tbBuy.SetSize(128, 16);
			tbBuy.ValueChanged += TbBuy_ValueChanged;
			Game1.GUIManager.Add(tbBuy);

			tbSell = new TrackBar(Game1.GUIManager);
			tbSell.SetPosition(256 + 16, tbBuy.Top + 74);
			tbSell.SetSize(128, 16);
			tbSell.ValueChanged += TbSell_ValueChanged;
			Game1.GUIManager.Add(tbSell);

			Button btn0 = new Button(Game1.GUIManager);
			btn0.SetPosition(tbBuy.Left + tbBuy.Width, tbBuy.Top);
			btn0.Text = "Buy";
			btn0.SetSize(48, 16);
			btn0.Click += Btn0_Click;

			Button btn1 = new Button(Game1.GUIManager);
			btn1.SetPosition(tbSell.Left + tbSell.Width, tbSell.Top);
			btn1.Text = "Sell";
			btn1.SetSize(48, 16);
			btn1.Click += Btn1_Click;

			TradeWindow.Add(buyLabel);
			TradeWindow.Add(sellLabel);
			TradeWindow.Add(tbBuy);
			TradeWindow.Add(tbSell);
			TradeWindow.Add(btn0);
			TradeWindow.Add(btn1);

			//TradeList.AutoHeight(TradeList.Items.Count);
			TradeList.AutoHeight(50);
			TradeList.Width = 256;

			TradeList.ItemIndexChanged += TradeList_ItemIndexChanged;
			TradeList.ItemIndex = 0;

			Game1.GUIManager.Add(TradeWindow);
		}

		/// BUY
		private void Btn0_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			float amount = tbBuy.Value;

			Building b = Game1.currentShip.gravPlanet.Building;
			List<Resource> availableResources = new List<Resource>();
			availableResources.AddRange(b.Buying.Keys);
			availableResources.AddRange(b.Selling.Keys);
			availableResources = availableResources.Distinct().ToList();

			Resource res = availableResources[TradeList.ItemIndex];

			if (tbBuy.Value > 0 && res != Resource.Void)
			{
				float buyPrice = b.Selling[res];
				float totalBuyPrice = buyPrice * tbBuy.Value;

				if (totalBuyPrice <= Game1.Cash)
				{
					Game1.currentShip.AddCargo(res, amount);
					Game1.Cash -= totalBuyPrice;
					tbBuy.Value = 0;
					tbSell.Value = 0;
					UpdateTrade();
				}
			}
		}
		/// SELL
		private void Btn1_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			float amount = tbSell.Value;


			Building b = Game1.currentShip.gravPlanet.Building;
			List<Resource> availableResources = new List<Resource>();
			availableResources.AddRange(b.Buying.Keys);
			availableResources.AddRange(b.Selling.Keys);
			availableResources = availableResources.Distinct().ToList();

			Resource res = availableResources[TradeList.ItemIndex];

			if (tbSell.Value > 0 && res != Resource.Void)
			{
				float sellPrice = b.Buying[res];

				float totalSellPrice = sellPrice * tbSell.Value;

				Game1.currentShip.RemoveCargo(res, amount);
				Game1.Cash += totalSellPrice;
				tbBuy.Value = 0;
				tbSell.Value = 0;
				UpdateTrade();
			}
		}

		private void TbSell_ValueChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			UpdateTrade();
		}

		private void TbBuy_ValueChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			UpdateTrade();
		}

		private void TradeList_ItemIndexChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			UpdateTrade();
		}

		private void UpdateTrade()
		{
			TradeList.Items.Clear();
			if (Game1.currentShip != null && Game1.currentShip.gravPlanet != null)
			{
				Building b = Game1.currentShip.gravPlanet.Building;
				if (b == null)
				{
					return;
				}

				List<Resource> availableResources = new List<Resource>();
				availableResources.AddRange(b.Buying.Keys);
				availableResources.AddRange(b.Selling.Keys);
				availableResources = availableResources.Distinct().ToList();

				foreach (Resource addRes in availableResources)
				{
					TradeList.Items.Add(addRes.ToString());
					TradeList.AutoHeight(50);
				}
				if (TradeList.ItemIndex > -1)
				{
					try
					{
						Resource res = availableResources[TradeList.ItemIndex];

						float available = Game1.currentShip.GetCargo(res);
						float spaceLeft = Game1.currentShip.CargoSpaceLeft(res);

						tbBuy.Range = b.Selling.ContainsKey(res) ? (int)Math.Ceiling(spaceLeft) : 0;
						tbSell.Range = b.Buying.ContainsKey(res) ? (int)Math.Ceiling(available) : 0;

						float buyPrice = b.Selling.ContainsKey(res) ? b.Selling[res] : 0.0f;
						float totalBuyPrice = buyPrice * tbBuy.Value;

						float sellPrice = b.Buying.ContainsKey(res) ? b.Buying[res] : 0.0f;
						float totalSellPrice = sellPrice * tbSell.Value;

						string priceBuyString = b.Selling.ContainsKey(res) ? $"Price {buyPrice} total {totalBuyPrice.ToString("0.00")}" : "Unavailable at this location";
						string priceSellString = b.Buying.ContainsKey(res) ? $"Price {sellPrice} total {totalSellPrice.ToString("0.00")}" : "Unavailable at this location";

						string cargoBuyString = $"{tbBuy.Value}/{spaceLeft}";
						string cargoSellString = $"{tbSell.Value}/{available}";

						buyLabel.Text = $"Buy {res}\n{priceBuyString}\n{cargoBuyString}";
						sellLabel.Text = $"Sell {res}\n{priceSellString}\n{cargoSellString}";

					}
					catch (Exception e)
					{
						TradeList.ItemIndex = -1;
					}
				}
			}
			else
			{
				tbBuy.Range = 0;
				tbSell.Range = 0;
				buyLabel.Text = "";
				sellLabel.Text = "";
			}
		}

		void ClientListNameBox_TextChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
		}

		public void LoadContent()
		{
			CreateClientListWindow();
			CreateTradeListWindow();

			ThrottleTrackbar = new TrackBar(Game1.GUIManager);
			ThrottleTrackbar.SetSize(128, 24);
			ThrottleTrackbar.SetPosition(1920 / 2 - 128 / 2, 1080 - 24);
			ThrottleTrackbar.Range = 100;
			ThrottleTrackbar.Value = 100;
			ThrottleTrackbar.ValueChanged += ThrottleTrackbar_ValueChanged;
			Game1.GUIManager.Add(ThrottleTrackbar);

			ShipWindow = new Window(Game1.GUIManager);
			ShipWindow.Alpha = 180;
			ShipWindow.SetSize(150, 508);
			ShipWindow.SetPosition(Game1.SCRWIDTH - ShipWindow.Width - 16, 16);
			ShipWindow.Text = "Ship Information";
			ShipWindow.AutoScroll = false;
			ShipWindow.CloseButtonVisible = false;
			ShipWindow.IconVisible = false;
			ShipWindow.CaptionVisible = false;

			Game1.GUIManager.Add(ShipWindow);

			ShipInfoLabel = new Label(Game1.GUIManager);
			ShipWindow.Add(ShipInfoLabel);
			ShipInfoLabel.SetSize(ShipInfoLabel.Parent.Width, 500);

			ShipListWindow = new Window(Game1.GUIManager);
			ShipListWindow.Alpha = 180;
			ShipListWindow.CloseButtonVisible = false;
			ShipListWindow.AutoScroll = false;
			ShipListWindow.SetSize(320, 240);
			ShipListWindow.SetPosition(Game1.SCRWIDTH - ShipListWindow.Width - 16, Game1.SCRHEIGHT - ShipListWindow.Height - 16);
			ShipListWindow.IconVisible = false;
			ShipListWindow.CaptionVisible = false;

			NewShipButton = new Button(Game1.GUIManager);
			NewShipButton.Text = "New Ship";
			NewShipButton.Click += NewShipButton_Click;
			NewShipButton.SetSize(ShipListWindow.Width - 16, 60);
			ShipListWindow.Add(NewShipButton);

			Game1.GUIManager.Add(ShipListWindow);

			ShipList = new ListBox(Game1.GUIManager);
			ShipList.SetPosition(0, 60);
			ShipList.Width = ShipListWindow.Width;
			ShipList.AutoHeight(5);
			ShipList.ItemIndexChanged += ShipList_ItemIndexChanged;
			ShipListWindow.Add(ShipList);

			font = ResourceManager.Load<SpriteFont>("Hudfont");
			digiFont = ResourceManager.Load<SpriteFont>("digi");
			iconPrograde = ResourceManager.Load<Texture2D>("Prograde");
			bg = ResourceManager.Load<Texture2D>("HudBG");
			nixel = ResourceManager.Load<Texture2D>("Nixel");

			landClear = ResourceManager.Load<Texture2D>("HudLC");
			arrow = ResourceManager.Load<Texture2D>("HudArrow");

		}

		private void ThrottleTrackbar_ValueChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			if (Game1.currentShip != null)
			{
				Game1.currentShip.Throttle = (float)ThrottleTrackbar.Value / 100.0f;
			}
		}

		void ShipList_ItemIndexChanged(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			try
			{
				Game1.currentShip = Game1.GetShips[ShipList.ItemIndex];
			}
			catch
			{
				Game1.currentShip = null;
			}

		}

		void NewShipButton_Click(object sender, TomShane.Neoforce.Controls.EventArgs e)
		{
			Game1.CreateNewShip();
		}

		public void DrawPlanetOverlay(SpriteBatch batch, Ship shop, int scr, int scrh)
		{
			if (Game1.zoom > 0.02f)
				return;
			foreach (Planet planet in Game1.planets)
			{
				Vector2 planetScrPos = Vector2.Transform(planet.position, Game1.cameraTransform);
				Vector2 tempOffset = Vector2.Transform(planet.position + new Vector2(planet.r), Game1.cameraTransform);
				float planetHeight = tempOffset.Y - planetScrPos.Y;

				string planetLabel = planet.isMoon ? $"{planet.Name} ({planet.parent.Name})" : planet.Name;
				//string planetLabel = planet.Name;
				Vector2 textsize = font.MeasureString(planetLabel);

				Vector2 offset = new Vector2(-textsize.X / 2, -planetHeight - textsize.Y);

				Color textCol = planet.isMoon ? Color.LightBlue : Color.LightGreen;
				batch.DrawString(font, planetLabel, planetScrPos + offset, textCol);
			}
		}

		public void Update(MouseState ms, Ship ship)
		{
			if (ship.gravPlanet != null && ship.gravPlanet.Building != null && ship.linearV.Length() < 0.1f)
			{
				//if (!TradeWindow.Visible)
				{
					TradeWindow.Visible = true;
					UpdateTrade();
				}
			}
			else
			{
				if (TradeWindow.Visible)
					TradeWindow.Visible = false;
			}

			ShipList.Items.Clear();
			for (int i = 0; i < Game1.GetShips.Count; i++)
			{
				string gPlanet = "null";
				string oPlanet = "null";
				if (Game1.GetShips[i].gravPlanet != null)
					gPlanet = Game1.GetShips[i].gravPlanet.Name;
				if (Game1.GetShips[i].gravPlanet != null)
					oPlanet = Game1.GetShips[i].gravPlanet.Name;

				ShipList.Items.Add(Game1.GetShips[i].Parts.Count + "-" + gPlanet + "-" + oPlanet);
				ShipList.AutoHeight(50);
			}

			if (ship != null)
				ShipInfoLabel.Text = String.Format("Mass: {0}\nLinearV: {1}\nElectric Charge: {2}\nAir Force: {3}\nVisViva: {4}\nSemi-Major: {5}\nEccentricity: {6}\nSpecE: {7}\ncross: {8}\nOOB:{9}",
					ship.mass.ToString("0.000"), ship.linearV != Vector2.Zero ? ship.linearV.Length().ToString("0.000") : "0", ship.eCharge.ToString("0.000"), ship.AirDrag.ToString("0.000"), ship.VisViva.ToString("0.000"), ship.SemiMajor.ToString("0.000"), ship.Eccentricity.ToString("0.000"), ship.SpecificEnergy.ToString("0.000"), ship.crossSection.ToString("0"), ship.outOfBalance.ToString("0.00"));

			ShipListWindow.Text = ShipList.ItemIndex.ToString();

			ClientList.Items.Clear();

			//TODO: Lock here 
			//lock (Game1.CurrentNetSocket.UDPConnections)
			/*{
				foreach (UDPConnection connection in Game1.CurrentNetSocket.UDPConnections.Values)
				{
					ClientList.Items.Add(connection.endpoint.ToString() + " avg: " + ((int)connection.AvgLatency).ToString() + " unconf: " + connection.ReliableCache.Count.ToString() + " lastpack : " + connection.lastUpdate.ToString("HH:mm:ss"));
					ClientList.AutoHeight(50);
				}
			}*/
		}

		public void Draw(SpriteBatch batch, Ship ship, int scrw, int scrh)
		{
			batch.Draw(bg, new Rectangle(0, 0, 1920, 1080), Color.White);

			float arrowRot = ship.AirDrag / 10.0f * (float)Math.PI;
			batch.Draw(arrow, new Rectangle(1106, 1036, 100, 100), null, Color.White, arrowRot, new Vector2(50, 50), SpriteEffects.FlipHorizontally, 0.0f);

			if (ship.linearV.Length() < 0.03f)
			{
				batch.Draw(landClear, new Rectangle(0, 0, 1920, 1080), Color.Green);
			}
			else
			{
				batch.Draw(landClear, new Rectangle(0, 0, 1920, 1080), Color.Red);
			}

			Vector2 prograde = ship.linearV;
			if (prograde.Length() < 0.001f)
			{
				prograde = Vector2.Zero;
			}
			prograde.Normalize();
			batch.Draw(iconPrograde, new Vector2(scrw / 2.0f, scrh / 2.0f) + prograde * 128.0f, null, Color.White, 0.0f, new Vector2(8, 8), 1.0f, SpriteEffects.None, 0.0f);
			batch.Draw(iconPrograde, new Vector2(scrw / 2.0f, scrh / 2.0f) - prograde * 128.0f, null, Color.Red, 0.0f, new Vector2(8, 8), 1.0f, SpriteEffects.None, 0.0f);


			int shipSpeed = (int)(ship.linearV.Length() * 50.0f);
			if (shipSpeed > 0)
			{
				string speedStr = shipSpeed.ToString();
				float strWidth = digiFont.MeasureString(speedStr).X;
				batch.DrawString(digiFont, speedStr, new Vector2(scrw * 0.5f - strWidth * 0.5f, scrh * 0.93f), Color.White);
			}

			DrawPlanetOverlay(batch, ship, scrw, scrh);

			batch.DrawString(font, "Ships Count: " + Game1.GetShips.Count, new Vector2(120, 0), Color.White);
			batch.DrawString(font, "Part Count: " + Game1.DEBUGPARTCOUNT, new Vector2(120, 32), Color.White);
			batch.DrawString(font, "Cash: " + Game1.Cash, new Vector2(120, 64), Color.White);
		}
	}
}
