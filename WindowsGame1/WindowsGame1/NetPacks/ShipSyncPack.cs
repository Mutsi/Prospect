﻿using NetObj;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace WindowsGame1.NetPacks
{
	[ProtoContract]
	public class ShipSyncPack
	{
		[ProtoMember(1)]
		public Guid ShipGuid = Guid.Empty;
		[ProtoMember(2)]
		public List<ShipPart> Parts;
		[ProtoMember(3)]
		public short PartCount;

		private const int numPartsInPack = 25;

		public byte[] Package()
		{
			MemoryStream stream = new MemoryStream();
			Serializer.Serialize<ShipSyncPack>(stream, this);
			return stream.ToArray();
		}

		public static ShipSyncPack Unpack(byte[] data)
		{
			MemoryStream stream = new MemoryStream(data);
			return Serializer.Deserialize<ShipSyncPack>(stream);
		}

		private static Dictionary<Guid, ShipSyncPack> packCache = new Dictionary<Guid, ShipSyncPack>();

		public static void SendShipSync(Ship ship, UDPConnection connection = null)
		{
			ShipPartPack partPack = new ShipPartPack();

			if (ship.parts.Count == 0)
				return;

			List<ShipSyncPack> partBatches = new List<ShipSyncPack>();

			int cur = 0;
			while (cur < ship.parts.Count)
			{
				//Full pack
				if (ship.parts.Count - cur >= numPartsInPack)
				{
					ShipSyncPack batch = new ShipSyncPack();
					batch.ShipGuid = ship.guid;
					batch.PartCount = (short)ship.Parts.Count;
					batch.Parts = ship.parts.GetRange(cur, numPartsInPack);
					partBatches.Add(batch);
					cur += numPartsInPack;
				}
				else if (ship.parts.Count - cur > 0)
				{
					int take = ship.parts.Count - cur;
					ShipSyncPack batch = new ShipSyncPack();
					batch.ShipGuid = ship.guid;
					batch.PartCount = (short)ship.Parts.Count;
					batch.Parts = ship.parts.GetRange(cur, take);
					partBatches.Add(batch);
					break;
				}
			}

			foreach (ShipSyncPack batch in partBatches)
			{
				NetPack pack = new NetPack();
				pack.content = batch.Package();
				pack.PackID = 0x05;
				pack.isReliable = 0x1;

				if (Game1.GetIsServer)
				{
					UDPListener netListener = (UDPListener)Game1.CurrentNetSocket;
					if (connection == null)
						netListener.Broadcast(pack);
					else
					{
						pack.connection = connection;
						netListener.SendPack(pack);
					}

				}
				else
				{
					UDPSender netSender = (UDPSender)Game1.CurrentNetSocket;
					netSender.SendPack(pack);

				}
			}
		}

		public static void ShipSyncExec(NetPack pack)
		{
			if (pack.content == null)
				return;
			if (Game1.GetIsServer)
			{
				UDPListener netListener = (UDPListener)Game1.CurrentNetSocket;
				netListener.Broadcast(pack);
			}

			ShipSyncPack syncPack = ShipSyncPack.Unpack(pack.content);

			//Own ship
			if (Game1.GetShips.Where(o => o.guid == syncPack.ShipGuid).Count() > 0)
			{
				return;
			}

			//Add to cache
			lock (packCache)
			{
				if (packCache.ContainsKey(syncPack.ShipGuid))
				{
					foreach (var part in syncPack.Parts)
					{
						if (!packCache[syncPack.ShipGuid].Parts.Any(o => o.guid == part.guid))
							packCache[syncPack.ShipGuid].Parts.Add(part);
					}
				}
				else
					packCache.Add(syncPack.ShipGuid, syncPack);
			}

			//We are receiving less than we have in our pack, wipe pack, start over.
			if (syncPack.PartCount < packCache[syncPack.ShipGuid].Parts.Count)
			{
				lock (packCache)
				{
					Console.WriteLine($"Error {syncPack.PartCount} parts found, total should be {packCache[syncPack.ShipGuid].Parts.Count}!");
					packCache.Remove(syncPack.ShipGuid);
				}
			}
			//Sync completed execute update
			if (syncPack.PartCount == packCache[syncPack.ShipGuid].Parts.Count)
			{
				//execute sync
				lock (Game1.otherShips)
				{
					for (int i = 0; i < Game1.otherShips.Count; i++)
					{
						if (Game1.otherShips.ElementAt(i).guid == syncPack.ShipGuid)
						{
							lock (packCache)
							{
								Game1.otherShips.ElementAt(i).parts = packCache[syncPack.ShipGuid].Parts;
								packCache.Remove(syncPack.ShipGuid);
							}

							foreach (ShipPart part in Game1.otherShips.ElementAt(i).parts)
							{
								part.LoadContent();
							}
							return;
						}
					}

					//No Ship Found we must create it
					Ship newShip = new Ship();
					newShip.guid = syncPack.ShipGuid;
					lock (packCache)
					{
						newShip.parts = packCache[syncPack.ShipGuid].Parts;
						packCache.Remove(syncPack.ShipGuid);
					}
					newShip.LoadContent(true);

					Game1.otherShips.Add(newShip);
				}
			}
		}
	}
}
