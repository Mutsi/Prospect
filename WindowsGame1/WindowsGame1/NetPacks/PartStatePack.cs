﻿using NetObj;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using WindowsGame1.ShipParts;

namespace WindowsGame1.NetPacks
{
	[ProtoContract]
	public class PartStatePack
	{
		[ProtoMember(1)]
		public Guid ShipGuid = Guid.Empty;
		[ProtoMember(2)]
		public Dictionary<ushort, byte[]> PartsUpdate;

		public byte[] Package()
		{
			MemoryStream stream = new MemoryStream();
			Serializer.Serialize<PartStatePack>(stream, this);
			return stream.ToArray();
		}

		public static PartStatePack Unpack(byte[] data)
		{
			MemoryStream stream = new MemoryStream(data);
			return Serializer.Deserialize<PartStatePack>(stream);
		}

		public static void SendShipSync(Ship ship, UDPConnection connection = null)
		{
			ShipPartPack partPack = new ShipPartPack();

			if (ship.parts.Count == 0)
				return;

			List<PartStatePack> partBatches = new List<PartStatePack>();

			List<Activatable> activatables = new List<Activatable>();
			foreach (ShipPart part in ship.parts.Where(o => o is Activatable))
			{
				activatables.Add((Activatable)part);
			}

			activatables = activatables.Where(o => o.IsDirty).ToList();

			int cur = 0;
			while (cur < activatables.Count)
			{


				//Full pack
				if (activatables.Count - cur >= 20)
				{
					PartStatePack batch = new PartStatePack();
					batch.ShipGuid = ship.guid;
					batch.PartsUpdate = activatables.GetRange(cur, 20).ToDictionary(o => o.PartId, k => k.Data);
					partBatches.Add(batch);
					cur += 20;
				}
				else if (activatables.Count - cur > 0)
				{
					int take = activatables.Count - cur;
					PartStatePack batch = new PartStatePack();
					batch.ShipGuid = ship.guid;
					batch.PartsUpdate = activatables.GetRange(cur, take).ToDictionary(o => o.PartId, k => k.Data);
					partBatches.Add(batch);
					break;
				}
			}

			foreach (PartStatePack batch in partBatches)
			{
				NetPack pack = new NetPack();
				pack.content = batch.Package();
				pack.PackID = 0x06;
				pack.isReliable = 0x0;

				if (Game1.GetIsServer)
				{
					UDPListener netListener = (UDPListener)Game1.CurrentNetSocket;
					if (connection == null)
						netListener.Broadcast(pack);
					else
					{
						pack.connection = connection;
						netListener.SendPack(pack);
					}

				}
				else
				{
					UDPSender netSender = (UDPSender)Game1.CurrentNetSocket;
					netSender.SendPack(pack);

				}
			}
		}

		public static void PartStateExec(NetPack pack)
		{
			if (Game1.GetIsServer)
			{
				UDPListener netListener = (UDPListener)Game1.CurrentNetSocket;
				netListener.Broadcast(pack);
			}

			PartStatePack syncPack = PartStatePack.Unpack(pack.content);

			//Own ship
			if (Game1.GetShips.Where(o => o.guid == syncPack.ShipGuid).Count() > 0)
			{
				return;
			}
			//execute sync
			lock (Game1.otherShips)
			{
				for (int i = 0; i < Game1.otherShips.Count; i++)
				{
					if (Game1.otherShips.ElementAt(i).guid == syncPack.ShipGuid)
					{
						List<ShipPart> activatables = Game1.otherShips.ElementAt(i).parts.Where(o => o is Activatable).ToList();
						foreach (Activatable part in activatables)
						{
							if (syncPack.PartsUpdate.ContainsKey(part.PartId))
							{
								//PartStatePack pack = syncPack.PartsUpdate[part.PartId];
								//part.Acivated = ;
								part.Data = syncPack.PartsUpdate[part.PartId];
								part.SetData();
							}
						}
						return;
					}
				}
			}
		}
	}
}
