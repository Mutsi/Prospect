﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using WindowsGame1.NetPacks;

namespace WindowsGame1
{

	[Serializable()]
	public class PersistPack
	{
		public string Name = "Prospect";
		public float Cash = Game1.Cash;
		public List<Ship> Ships;
	}

	class PersistWorld
	{
		public static bool IsDisabled = false;

		static public void StartGame()
		{
			if (IsDisabled) return;

			if (File.Exists("persist.ppus"))
			{
				Load();
			}
			else
			{
				NewGame();
			}
		}

		static public void NewGame()
		{
			PersistPack pack = new PersistPack();
			Stream TestFileStream = File.Create("persist.ppus");
			BinaryFormatter serializer = new BinaryFormatter();
			serializer.Serialize(TestFileStream, pack);
			TestFileStream.Close();

			Load();
		}

		static public void SaveGame()
		{
			if (IsDisabled) return;
			File.Delete("persist.ppus");

			PersistPack pack = new PersistPack();
			pack.Cash = Game1.Cash;

			if (Game1.GetShips.Count > 0)
				pack.Ships = Game1.GetShips;

			Stream TestFileStream = File.Create("persist.ppus");
			BinaryFormatter serializer = new BinaryFormatter();
			serializer.Serialize(TestFileStream, pack);
			TestFileStream.Close();
		}


		static public void Load()
		{
			if (IsDisabled) return;
			PersistPack pack;

			Stream TestFileStream = File.OpenRead("persist.ppus");
			BinaryFormatter deserializer = new BinaryFormatter();
			pack = (PersistPack)deserializer.Deserialize(TestFileStream);
			TestFileStream.Close();

			Game1.Cash = pack.Cash;

			if (pack.Ships != null && pack.Ships.Count > 0)
			{
				foreach (Ship ship in pack.Ships)
				{
					//ship.guid = new Guid();
					ship.LoadContent(true);
					Game1.GetShips.Add(ship);

					//ShipSyncPack.SendShipSync(ship);
				}
			}
		}
	}
}
