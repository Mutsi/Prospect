﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    class SpriteAnimation
    {
        private Texture2D sheet;
        int frame = 0;
        float time;

        public void LoadContent(string path)
        {
            sheet = ResourceManager.Load<Texture2D>(path);
        }

        public void Update()
        {
            time += 0.2f;
            if (time > 0.2f)
            {
                time = 0.0f;
                frame++;
                if (frame == 8)
                    frame = 0;
            }
        }

        public void Draw(SpriteBatch batch, Vector2 position, float rotation)
        {
            batch.Draw(sheet, position, new Rectangle(0, frame * 32, 64, 32), Color.White, rotation, new Vector2(16,16), 1.0f, SpriteEffects.None, 0.0f);
        }

    }
}
