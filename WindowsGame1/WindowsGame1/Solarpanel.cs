﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    [Serializable()]
    [ProtoContract]
    public class Solarpanel : ShipPart
    {

        public Solarpanel()
        {

            TypeID = 3;
        }
        public Solarpanel(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 3;
            mass = 5.0f;
        }

        public override void Action(Ship ship, bool isNet = false)
        {
            ship.eChargeNew += 0.15f;
        }

        public override void LoadContent()
        {
            texture = ResourceManager.Load<Texture2D>("Solarpanel");
			//if (guid == Guid.Empty)
				//guid = Guid.NewGuid();
		}
	}
}
