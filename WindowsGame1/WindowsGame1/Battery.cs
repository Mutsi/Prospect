﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    [Serializable()]
    [ProtoContract]
    public class Battery : ShipPart
    {
        int capacity = 50;

        public Battery()
        {
            TypeID = 4;

        }

        public Battery(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 4;
        }

        public override void Action(Ship ship, bool isNet = false)
        {
            ship.eChargeMax += capacity;
        }

        public override void LoadContent()
        {
            texture = ResourceManager.Load<Texture2D>("Battery");
			//if (guid == Guid.Empty)
				//guid = Guid.NewGuid();
		}
    }
}
