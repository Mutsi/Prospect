﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Newtonsoft.Json;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;

namespace WindowsGame1
{
	[ProtoContract]
	public class ShipPartPack
	{
		[ProtoMember(1)]
		public Guid ShipGuid = Guid.Empty;
		[ProtoMember(2)]
		public ShipPart ShipPart;

		public byte[] Package(Guid shipGuid, ShipPart part)
		{
			this.ShipGuid = shipGuid;
			this.ShipPart = part;

			MemoryStream stream = new MemoryStream();
			Serializer.Serialize<ShipPartPack>(stream, this);
			return stream.ToArray();
		}

		public static ShipPartPack Unpack(byte[] data)
		{
			MemoryStream stream = new MemoryStream(data);
			return Serializer.Deserialize<ShipPartPack>(stream);
		}
	}

	//[NonSerialized()]
	[ProtoContract]
	[ProtoInclude(100, typeof(Hull))]
	[ProtoInclude(101, typeof(Battery))]
	[ProtoInclude(102, typeof(Cargobay))]
	[ProtoInclude(103, typeof(FuelTank))]
	[ProtoInclude(104, typeof(Laser))]
	[ProtoInclude(105, typeof(RCSThruster))]
	[ProtoInclude(106, typeof(Seperator))]
	[ProtoInclude(107, typeof(Solarpanel))]
	[ProtoInclude(108, typeof(Thruster))]
	[ProtoInclude(109, typeof(Miner))]
	[ProtoInclude(110, typeof(TractorBeam))]
	[Serializable()]
	public class ShipPart : IPart
	{
		[NonSerialized()]
		protected Texture2D texture;



		public Hull parent;
		public bool IsNew = true;
		public Vector2 Offset = Vector2.Zero;

		public bool isRunning = true;

		public float mass = 15.0f;

		public Vector2 worldPosition;
		public Vector2 localCOMPosition;
		public float rotation;

		[ProtoMember(1)]
		public byte TypeID = 0;
		[ProtoMember(2)]
		public int localX { get; set; }
		[ProtoMember(3)]
		public int localY { get; set; }
		[ProtoMember(4)]
		public ushort guid = 0;

		public ShipPart()
		{

		}
		public ShipPart(int x, int y)
		{
			localX = x;
			localY = y;
		}

		public override bool Equals(object obj)
		{
			//       
			// See the full list of guidelines at
			//   http://go.microsoft.com/fwlink/?LinkID=85237  
			// and also the guidance for operator== at
			//   http://go.microsoft.com/fwlink/?LinkId=85238
			//

			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			ShipPart p = (ShipPart)obj;
			return guid == p.guid;
		}

		// override object.GetHashCode
		public override int GetHashCode()
		{
			return guid.GetHashCode();
		}

		public bool IsDestroyed = false;

		public virtual float Mass()
		{
			return mass;
		}

		public Vector2 WorldPosition()
		{
			return worldPosition;
		}

		public Vector2 LocalCOMPosition()
		{
			return localCOMPosition;
		}

		public void SetLocal(int x, int y)
		{
			localX = x;
			localY = y;
		}

		public bool IsValid(int x, int y, Ship ship)
		{
			return ValidatePos(x, y, ship);
		}

		public float Cost = 100;

		public virtual void Transfer(Ship ship)
		{

		}

		public virtual bool ValidatePos(int x, int y, Ship ship)
		{
			List<ShipPart> shipPartsOnCursor = ship.parts.Where(o => o.localX == x && o.localY == y).ToList();
			List<Hull> hullOnCursor = shipPartsOnCursor.OfType<Hull>().ToList();

			bool validCenter = ship.parts.Any(o => o.localX == x && o.localY == y);
			float xx = x;
			float yy = y;

			parent = hullOnCursor.FirstOrDefault();

			return validCenter && shipPartsOnCursor.Count == 1 && hullOnCursor.Count == 1;
		}

		public void NetUpdate(Ship ship)
		{
			Vector2 localCom = ship.CentreOfMass - ship.position;
			localCOMPosition = new Vector2(localX * 32, localY * 32) - localCom;

			Vector3 translation = Vector3.Transform(new Vector3(ship.CentreOfMass, 0.0f) + new Vector3(localCOMPosition, 0.0f), ship.PositionRotationMatrix);
			worldPosition.X = translation.X;
			worldPosition.Y = translation.Y;

			rotation = ship.rotation;

			if (isRunning)
				Action(ship, true);
		}

		public virtual void Update(Ship ship)
		{
			rotation = ship.rotation;
			if (parent != null)
			{
				worldPosition.X = parent.worldPosition.X;
				worldPosition.Y = parent.worldPosition.Y;
			}
			else
			{
				Vector2 localCom = ship.CentreOfMass - ship.position;
				localCOMPosition = new Vector2(localX * 32, localY * 32) - localCom;

				Vector3 translation = Vector3.Transform(new Vector3(ship.CentreOfMass, 0.0f) + new Vector3(localCOMPosition, 0.0f), ship.PositionRotationMatrix);
				worldPosition.X = translation.X;
				worldPosition.Y = translation.Y;
			}

			if (!ship.inEditor && (parent == null || parent.IsDestroyed))
			{
				parent = null;
				ship.RemovePart(this);
			}

			if (isRunning)
				Action(ship);
		}

		public virtual void Action(Ship ship, bool isNet = false)
		{
		}

		public virtual void LoadContent()
		{

		}

		public virtual void Draw(SpriteBatch batch)
		{
			batch.Draw(texture, worldPosition, null, Color.White, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}
	}
}
