﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProtoBuf;
using System;
using System.Collections.Generic;

namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	public class Falcon : Thruster
	{
		public Falcon()
		{

			TypeID = 11;
		}

		public Falcon(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 11;
			mass = 500.0f;
			Cost = 1200;

			thrust = 50.0f;
			fuelUse = 0.05f;
		}

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("Falcon");
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}
	}

}
