﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    public class TypeCompressor
    {


        public static short Encode(float value)
        {
            int cnt = 0;
            while (value != Math.Floor(value))
            {
                value *= 10.0f;
                cnt++;
            }
            return (short)((cnt << 12) + (int)value);
        }

        public static float Decode(short value)
        {
            int cnt = value >> 12;
            float result = value & 0xfff;
            while (cnt > 0)
            {
                result /= 10.0f;
                cnt--;
            }
            return result;
        }
    }
}
