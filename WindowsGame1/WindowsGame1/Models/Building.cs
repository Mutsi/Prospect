﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1.Models
{
	public class Building
	{
		public Dictionary<Resource, float> Buying { get; set; }
		public Dictionary<Resource, float> Selling { get; set; }

		public Building(Planet planet, Random random)
		{
			Buying = new Dictionary<Resource, float>();
			Selling = new Dictionary<Resource, float>();

			string[] resources = Enum.GetNames(typeof(Resource));

			int numBuy = random.Next(0, 3) + 1;
			int numSell = random.Next(0, 3) + 1;

			for (int i = 0; i < numSell; i++)
			{
				int selectResource = random.Next(1, resources.Length);
				Resource res = (Resource)selectResource;
				float price = ResourceData.ResourcePrice[res];
				float effect = 1.0f - (float)random.NextDouble() * 0.15f;

				float actual = (float)(Math.Round(price * effect * 100.0) / 100.0);

				if (!Selling.ContainsKey(res))
					Selling.Add(res, actual);
			}

			for (int i = 0; i < numBuy; i++)
			{
				int selectResource = random.Next(1, resources.Length);
				Resource res = (Resource)selectResource;
				float price = ResourceData.ResourcePrice[res];
				float effect = 1.0f + (float)random.NextDouble() * 0.15f;

				float actual = (float)(Math.Round(price * effect * 100.0) / 100.0);

				if (Selling.ContainsKey(res))
					actual = (float)(Math.Round(Selling[res] * 0.9 * 100.0) / 100.0);

				if (!Buying.ContainsKey(res))
					Buying.Add(res, actual);
			}
		}
	}
}
