﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace WindowsGame1.Models
{
	public enum Resource
	{
		Void = 0,
		Iron,
		Silver,
		Gold,
		Medical_Supplies,
		Hydrogen,
	}

	public static class ResourceData
	{
		public static Dictionary<Resource, float> ResourcePrice = new Dictionary<Resource, float> {
			{Resource.Iron, 10 },
			{Resource.Silver, 25 },
			{Resource.Gold, 100 },
			{Resource.Medical_Supplies, 2},
			{Resource.Hydrogen, 1},
		};

		public static Dictionary<Resource, float> ResourceDensity = new Dictionary<Resource, float> {
			{Resource.Iron, 7.874f },
			{Resource.Silver, 10.49f },
			{Resource.Gold, 19.32f },
			{Resource.Medical_Supplies, 1.0f },
			{Resource.Hydrogen, 0.07f },
		};

		public static List<Resource> Minerals = new List<Resource> {
			Resource.Iron, Resource.Silver, Resource.Gold
		};

		public static Dictionary<Resource, Color> ResourceColor = new Dictionary<Resource, Color>
		{
			{Resource.Iron, Color.DarkSlateGray },
			{Resource.Silver, Color.Silver },
			{Resource.Gold, Color.Gold },
			{Resource.Medical_Supplies, Color.Red },
			{Resource.Hydrogen, Color.Pink},
		};

	}
}
