﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WindowsGame1.Models
{
	public class CargoController
	{
		public Dictionary<Resource, float> resourceDictionary = new Dictionary<Resource, float>();

		public void AddResource(Resource res, float amount)
		{
			if (amount < 0)
				throw new CargoException($"Can not add negative amount of resources");

			if (!resourceDictionary.ContainsKey(res))
				resourceDictionary.Add(res, amount);
			else
				resourceDictionary[res] += amount;
		}

		public bool ContainsResource(Resource res, float amount)
		{
			if (!resourceDictionary.ContainsKey(res))
				return false;
			if (resourceDictionary[res] < amount)
				return false;
			return true;
		}

		public bool HasAny(Resource res)
		{
			if (!resourceDictionary.ContainsKey(res))
				return false;
			return resourceDictionary[res] > 0;
		}

		public float TakeMax(Resource res, float amount)
		{
			if (!resourceDictionary.ContainsKey(res))
				throw new CargoException($"Cargo does not contain {res}");

			float available = resourceDictionary[res];

			if (available >= amount)
			{
				TakeResource(res, amount);
				return amount;
			}
			else
			{
				TakeResource(res, available);
				return available;
			}
		}

		public void TakeResource(Resource res, float amount)
		{
			if (!resourceDictionary.ContainsKey(res))
				throw new CargoException($"Cargo does not contain {res}");
			if (resourceDictionary[res] < amount)
				throw new CargoException($"Cargo does not contain {amount} {res}");
			resourceDictionary[res] -= amount;

			if (resourceDictionary[res] == 0)
				resourceDictionary.Remove(res);
		}

		public float GetTotalAvailableResource(Resource res)
		{
			if (resourceDictionary.ContainsKey(res))
				return resourceDictionary[res];
			return 0;
		}
	}

	[Serializable]
	internal class CargoException : Exception
	{
		public CargoException()
		{
		}

		public CargoException(string message) : base(message)
		{
		}

		public CargoException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected CargoException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
