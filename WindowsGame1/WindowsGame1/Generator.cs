﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
{
    public class Generator
    {
        private static readonly string[] syllables = {
	        "hoicks",
	        "kane",
	        "bram",
	        "mopes",
	        "mall",
	        "shoat",
	        "tam",
	        "ephes",
	        "rna",
	        "snot",
	        "vex",
	        "metz",
	        "eysk",
	        "sert",
	        "sur",
	        "owl",
	        "wan",
	        "dens",
	        "szold",
	        "therap",
	        "terse",
	        "clod",
	        "rgs",
	        "puir",
	        "tyum",
	        "gad",
	        "recit",
	        "illust",
	        "mtb",
	        "hymn",
	        "herb",
	        "delve",
	        "pct",
	        "slade",
	        "aetat",
	        "ned",
	        "shook",
	        "lost",
	        "beer",
	        "sci",
	        "jcs",
	        "some",
	        "piss",
	        "bett",
	        "mats",
	        "guise",
	        "clipt",
	        "wreath",
	        "belt",
	        "sump",
	        "mair",
	        "gunge",
	        "cpr",
	        "soh",
	        "nedc",
	        "pug",
	        "sool",
	        "sort",
	        "rpc",
	        "sake",
	        "sup",
	        "thru",
	        "fils",
	        "perche",
	        "stage",
	        "shaw",
	        "ire",
	        "carte",
	        "grebe",
	        "punt",
	        "notch",
	        "vouch",
	        "quaich",
	        "stegh",
	        "cree",
	        "douse",
	        "p'abx",
	        "scran",
	        "pock",
	        "swede",
	        "klutz",
	        "exch",
	        "whim",
	        "gybe",
	        "sere",
	        "smooch",
	        "hip",
	        "sleep",
	        "ye'se",
	        "pearl",
	        "bleb",
	        "breech",
	        "deed",
	        "misc",
	        "lem",
	        "charr",
	        "dhole",
	        "slump",
	        "scotch",
	        "assoc"
        };

        private static readonly string[] recources = {
	        "Hydrogen",
	        "Helium",
	        "Argon",
	        "Neon",
	        "Krypton",
	        "Deuterium",
	        "Xenon",
	        "Radon",
	        "Iron",
	        "Uranium",
	        "Sulphur",
        };

        private static readonly string[] climates = {
	        "Warm",
	        "Hot",
	        "Cold",
	        "Frozen",
	        "Tropical",
	        "Seasonal",
	        "Dry",
	        "Wet",
	        "Vulcanic",
	        "Barren",
        };

        private static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        public static string GenerateName(Random random)
        {
            string name = "";

            //Get random length
            int numOfSyllables = random.Next(1, 3);

            for (int i = 0; i < numOfSyllables; ++i)
            {
                name += syllables[random.Next(1, 100)];
            }

            return FirstLetterToUpper(name);
        }

    }
}
