﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
namespace WindowsGame1
{
	[Serializable()]
	[ProtoContract]
	[ProtoInclude(201, typeof(Falcon))]
	public class Thruster : ShipPart
	{
		public float thrust = 8.0f;
		protected float fuelUse = 0.005f;
		public FuelTank fuelTank;

		[ProtoMember(1)]
		public bool isOn = false;

		public Thruster()
		{
			TypeID = 1;
		}

		public Thruster(int x, int y)
		{
			localX = x;
			localY = y;
			TypeID = 1;
			mass = 50.0f;
			Cost = 300;
			Offset = new Vector2(-32, 0);
		}

		[NonSerialized()]
		static SoundEffect thrusterSFX;
		[NonSerialized()]
		static SoundEffectInstance sfxInstance;

		[NonSerialized()]
		private float volume = 0.0f;

		public override void LoadContent()
		{
			texture = ResourceManager.Load<Texture2D>("Engine");

			if (thrusterSFX == null)
			{
				thrusterSFX = ResourceManager.Load<SoundEffect>("Audio/Thruster");
				sfxInstance = thrusterSFX.CreateInstance();
				sfxInstance.IsLooped = true;
				sfxInstance.Volume = volume;
				sfxInstance.Play();
			}
			//if (guid == Guid.Empty)
			//guid = Guid.NewGuid();
		}

		private float drainFuel(float throttle)
		{
			if (fuelTank == null)
				return 0.0f;

			float use = fuelUse * throttle;

			if (fuelTank.fuel >= use)
				fuelTank.fuel -= use;
			else
			{
				float rat = fuelTank.fuel / use;
				fuelTank.fuel = 0.0f;
				return rat;
			}

			return 1.0f;
		}

		public Vector2 GetForce(float throttle)
		{
			Vector2 forward = new Vector2();
			forward.X = (float)Math.Cos(rotation);
			forward.Y = (float)Math.Sin(rotation);

			return forward * thrust * throttle * drainFuel(throttle);
		}

		public override bool ValidatePos(int x, int y, Ship ship)
		{
			int i = ship.parts.Where(o => o.guid != guid).Count(o => o.localX == x && o.localY == y);
			Hull parentHull = ship.parts.OfType<Hull>().FirstOrDefault(o => o.localX == x + 1 && o.localY == y);
			bool validRight = parentHull != null;
			parent = parentHull;

			float xx = x;
			float yy = y;

			return validRight && i == 0;
		}

		public override void Transfer(Ship ship)
		{
			bool tankFound = false;
			if (fuelTank != null)
			{
				foreach (ShipPart part in ship.Parts)
				{
					if (part == fuelTank)
					{
						tankFound = true;
						return;
					}
				}
				if (!tankFound)
				{
					fuelTank = null;
				}
			}
		}

		public override void Update(Ship ship)
		{
			rotation = ship.rotation;
			Vector2 localCom = ship.CentreOfMass - ship.position;
			localCOMPosition = new Vector2(localX * 32, localY * 32) - localCom;

			Vector3 translation = Vector3.Transform(new Vector3(ship.CentreOfMass, 0.0f) + new Vector3(localCOMPosition, 0.0f), ship.PositionRotationMatrix);
			worldPosition.X = translation.X;
			worldPosition.Y = translation.Y;

			ShipForward = ship.Forward;

			if (!ship.inEditor && (parent == null || parent.IsDestroyed))
			{
				parent = null;
				ship.RemovePart(this);
			}

			if (isRunning)
				Action(ship);
		}

		public Vector2 ShipForward;

		public override void Action(Ship ship, bool isNet = false)
		{
			if (fuelTank != null && fuelTank.IsDestroyed)
				fuelTank = null;

			if (isOn && fuelTank != null && fuelTank.fuel > 0.0f)
			{
				Game1.particleMaster.Spawn(5, worldPosition, -ShipForward);

				volume += 0.05f;
				volume = MathHelper.Clamp(volume, 0.0f, 1.0f);
				if (sfxInstance != null)
					sfxInstance.Volume = volume;
			}
			else
			{
				volume -= 0.05f;
				volume = MathHelper.Clamp(volume, 0.0f, 1.0f);
				if (sfxInstance != null)
					sfxInstance.Volume = volume;
			}

			if (sfxInstance != null)
				sfxInstance.Pitch = sfxInstance.Volume * 0.5f - 0.25f;

			isOn = ship.thrusterOn;
		}

		public override void Draw(SpriteBatch batch)
		{
			Vector2 forward = new Vector2();
			forward.X = (float)Math.Cos(rotation);
			forward.Y = (float)Math.Sin(rotation);
			batch.Draw(texture, worldPosition, null, Color.White, rotation, new Vector2(16, 16), 1.0f, SpriteEffects.None, 0);
		}
	}
}
