float4x4 MatrixTransform;
float2 PlayerPosition;

void SpriteVertexShader(inout float4 vColor : COLOR0, inout float2 texCoord : TEXCOORD0, inout float4 position : POSITION0)
{
	position = mul(position, MatrixTransform);
}

uniform extern texture ScreenTexture;
sampler ScreenS = sampler_state
{
	Texture = < ScreenTexture > ;
};

const float blurSizeX = 2.0 / 1920.0;
const float blurSizeY = 2.0 / 1080.0;

float rand_1_05(in float2 uv)
{
	float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233)*2.0)) * 43758.5453));
		return abs(noise.x + noise.y) * 0.5;
}

float2 rand_2_10(in float2 uv) {
	float noiseX = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
	float noiseY = sqrt(1 - noiseX * noiseX);
	return float2(noiseX, noiseY);
}

float2 rand_2_0004(in float2 uv)
{
	float noiseX = (frac(sin(dot(uv, float2(12.9898, 78.233))) * 43758.5453));
	float noiseY = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
	return float2(noiseX, noiseY) * 0.004;
}

float4 PixelShaderFunction(float2 texCoord: TEXCOORD0) : COLOR0
{
	float4 src = tex2D(ScreenS, texCoord);
	float4 sum = float4(0, 0, 0, 0);

	sum += tex2D(ScreenS, float2(texCoord.x - 4.0*blurSizeX, texCoord.y)) * 0.05;
	sum += tex2D(ScreenS, float2(texCoord.x - 3.0*blurSizeX, texCoord.y)) * 0.09;
	sum += tex2D(ScreenS, float2(texCoord.x - 2.0*blurSizeX, texCoord.y)) * 0.12;
	sum += tex2D(ScreenS, float2(texCoord.x - blurSizeX, texCoord.y)) * 0.15;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y)) * 0.16;
	sum += tex2D(ScreenS, float2(texCoord.x + blurSizeX, texCoord.y)) * 0.15;
	sum += tex2D(ScreenS, float2(texCoord.x + 2.0*blurSizeX, texCoord.y)) * 0.12;
	sum += tex2D(ScreenS, float2(texCoord.x + 3.0*blurSizeX, texCoord.y)) * 0.09;
	sum += tex2D(ScreenS, float2(texCoord.x + 4.0*blurSizeX, texCoord.y)) * 0.05;

	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y - 4.0*blurSizeY)) * 0.05;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y - 3.0*blurSizeY)) * 0.09;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y - 2.0*blurSizeY)) * 0.12;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y - blurSizeY)) * 0.15;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y)) * 0.16;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y + blurSizeY)) * 0.15;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y + 2.0*blurSizeY)) * 0.12;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y + 3.0*blurSizeY)) * 0.09;
	sum += tex2D(ScreenS, float2(texCoord.x, texCoord.y + 4.0*blurSizeY)) * 0.05;

	float4 end = saturate(src * 0.8 + sum * 0.33);

	float noise = rand_1_05(PlayerPosition + texCoord);

	noise += 0.98;
	noise = clamp(noise, 0.0, 1.0);

	return  end + ((1.0-noise) * 5.0);
}

technique Technique1
{
	pass Pass1
	{
		VertexShader = compile vs_3_0 SpriteVertexShader();
		PixelShader = compile ps_3_0 PixelShaderFunction();
	}
}