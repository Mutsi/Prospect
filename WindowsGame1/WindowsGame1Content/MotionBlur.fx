texture scene;
sampler sceneSampler = sampler_state
{
	Texture = (scene);
};

float xblur = 0.0; //center of the screen
float yblur = 0.0;

float4 ps(float2 texCoord : TEXCOORD0) : COLOR0
{
	float4 color = tex2D(sceneSampler, texCoord);

	float2 coord = texCoord;
	for (int i = 0; i<12; i++)
	{
		coord.x -= (i) / 500.0f * xblur;
		coord.y -= (i) / 500.0f * yblur;
		color += tex2D(sceneSampler, coord);
	}
	color = color / 7;
	return color;
}

technique
{
	pass
	{
		PixelShader = compile ps_2_0 ps();
	}
}